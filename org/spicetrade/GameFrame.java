/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade;

import bsh.Interpreter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.*;

import org.spicetrade.collections.Actions;
import org.spicetrade.collections.Armies;
import org.spicetrade.collections.Dialogs;
import org.spicetrade.collections.Routes;
import org.spicetrade.collections.Market;
import org.spicetrade.collections.Places;
import org.spicetrade.collections.Traders;
import org.spicetrade.tools.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Spice Trade The frame for the application, essentially this runs the whole
 * show.
 * <p>
 * There are groups of functions, the "goto"-functions serve as the entry points
 * for various elements in the game, ie. gotoPlace renders a place for the
 * player, gotoCity starts moving the player towards a designated city etc.
 * <p>
 * The "goto"-functions use the "paint"-functions, which contain the UI/layout
 * logic.
 * <p>
 * Then there are the "trigger"-functions that launch a new thread that starts
 * to do something in the game world, ie. grow a plant, show the movement of the
 * player on the map while going to a new city and so on.
 * <p>
 * Then of course there are a bunch of helper functions, like getImage, loadGame,
 * saveGame.
 *
 * @author Juha Holopainen
 * @version 1.0
 */

public class GameFrame extends JFrame {

    private static final List<String> nonLoggables = Arrays.asList("Minimize", "Load", "Save", "Close", "Log");
    public static GameFrame gf;
    public static Arabesque arabesque;
    public static Interpreter interpreter;
    public Actions actions;
    public Armies armies;
    public Audio jukebox;
    public boolean idle;
    public boolean modal;
    public boolean showMapGlobe;
    public boolean showOptions;
    public Dialogs dialogs;
    public Drawable flyover;
    public Drawables hud;
    public Drawables objects;
    public Drawables panel;
    public Font font;
    public Font fontBold;
    public Graphics bg;
    public HashMap<String, Image> pictures;
    public Image offscreen;
    public int converter;
    public int jinx;
    public Interpreter bsh;
    public Market market;
    public Places places;
    public Player player;
    public Random random;
    public Routes routes;
    public String bshimport;
    public String sentences;
    public String triggerAtEntry;
    public String typeface;
    public Task task;
    public Tools tools;
    public Traders traders;
    public XStream xstream;

    public GameFrame() {
        bsh = new Interpreter();
        bshimport = "import org.spicetrade.*;\nimport org.spicetrade.tools.*;\n";
        arabesque = new Arabesque();
        gf = this;
        xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY);
        xstream.addPermission(NullPermission.NULL);
        xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
        tools = new Tools();
        objects = new Drawables();
        panel = new Drawables();
        hud = new Drawables();
        random = new Random();
        jinx = random.nextInt(100);
        pictures = new HashMap<>();
        setModal(false);
        converter = 0;

        flyover = null;
        triggerAtEntry = "";
        typeface = "Arial";
        setDialog("");
        initialize();
    }

    public void initialize() {
        try {
            // bootstrap the game world
            gf = this;
            dialogs = new Dialogs();
            places = new Places();
            market = new Market();
            jukebox = new Audio();
            jukebox.setMusicOn(false);
            actions = new Actions();
            routes = new Routes();
            player = new Player();
            armies = new Armies();
            traders = new Traders();
            player.getJournal().open("field2");
            bsh.setOut(System.out);
            bsh.set("frame", this);
            bsh.set("result", false);
            setBounds(0, 0, 1024, 768);
            // Load settings
            loadSettings();
            font = new Font(typeface, 0, 14);
            fontBold = new Font(typeface, 1, 14);
            // CHANGE 26.4.2005 support for full screen mode
            if (player.fullScreen) setFullScreen(true);
            else setFullScreen(false);
            addMouseListener(new MouseAdapter() {

                public void mouseClicked(MouseEvent e) {
                    this_mouseClicked(e);
                }

            });
            addMouseMotionListener(new MouseMotionAdapter() {

                public void mouseMoved(MouseEvent e) {
                    this_mouseMoved(e);
                }

            });
            addWindowListener(new WindowAdapter() {

                public void windowClosed(WindowEvent e) {
                    this_windowClosed(e);
                }

            });
            jukebox.musicOn = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setFullScreen(boolean fullScreen) {
        GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        if (fullScreen) {
            gf.dispose();
            gf.setUndecorated(true);
            device.setFullScreenWindow(gf);
            gf.validate();
        } else {
            device.setFullScreenWindow(null);
            gf.setUndecorated(true);
            gf.setBounds(0, 0, 1024, 768);
            gf.setVisible(true);
            gf.validate();
        }
    }

    public void gotoBattle(String who) {
        gotoBattle(who, 1, 0, armies.getHealth(who), "");
    }

    public void gotoBattle(String who, int round, int weapon, int enemyMight, String endTask) {
        // entering the battle dialog
        jukebox.loopMusic("/music/10_its_time.ogg");
        jinx = random.nextInt(100);
        setModal(true);
        player.inBattle = true;
        panel.clear();
        player.chooseFace("16");
        arabesque.paintBattle(who, round, weapon, enemyMight, endTask);
        arabesque.doLayout();
        arabesque.repaint();
    }

    public void gotoDeath(String famousLastWords, int how) {
        // game over, you died
        // how == 0: regular death, of age etc
        // how == 1: died gloriously in battle
        // how == 2: died shamefully in battle
        // how == 3: died of disease or poisoning
        player.inGameOver = true;
        setModal(true);
        player.deathType = how;
        player.lastWords = famousLastWords;
        panel.clear();
        objects.clear();
        if ("worldmap".equals(player.getPlace()))
            arabesque.paintWorldMap(true);
        else
            arabesque.paintPlace(player.getPlace());
        player.logo = "logoDeath";
        arabesque.paintStatus(Tab.DEATH, "", 0, Tab.STATUS, 0);
        arabesque.doLayout();
        arabesque.repaint();
    }

    public String getDialog() {
        return sentences;
    }

    public void setDialog(String dialog) {
        this.sentences = dialog;
    }

    public void gotoDialog(String to) {
        // entering a dialog
        player.addLog("!" + to, "frame.gotoDialog(\"" + to + "\");", player.getDay() + player.getMonth() * 30 + player.getYear() * 360);
        gf.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setDialog(to);
        setModal(true);
        flyover = null;
        panel.clear();
        objects.clear();
        if ("worldmap".equals(player.getPlace()))
            arabesque.paintWorldMap(true);
        else
            arabesque.paintPlace(player.getPlace());
        arabesque.paintDialog(getDialog());
        arabesque.doLayout();
        arabesque.repaint();
        jinx = random.nextInt(100);
    }

    public void gotoDiary() {
        gotoDiary(0);
    }

    public void gotoDiary(int page) {
        // this is the end of the game "book of life"
        setModal(true);
        panel.clear();
        objects.clear();
        arabesque.paintPlace(player.getPlace());
        arabesque.paintDiary(page);
        arabesque.doLayout();
        arabesque.repaint();
    }

    public void gotoWorldMap() {
        // entering the world map
        jukebox.loopMusic("/music/16_world_horizon.ogg");
        player.setPlace("worldmap");
        player.setNiceCity("Map of the world");
        setModal(false);
        flyover = null;
        panel.clear();
        objects.clear();
        hud.clear();
        arabesque.paintWorldMap(true);
        arabesque.paintGlass();
        arabesque.doLayout();
        arabesque.repaint();

        // again, these kinds of "game rules" should be refactored out of the game engine core
        player.getJournal().done("permit1");
        player.getJournal().open("permit5");

        // if we have agricultural produce, don't let the player get out of Baghdad
        String[] spices = {"10100","10110","10120","10130","10200","10210","10220","10230"};
        if (player.hasAnyItems(spices)&&!player.hasItem("10640"))
            gotoDialog("1680");
    }

    public void gotoPlace(String to) {
        if (!triggerAtEntry.isEmpty()) {
            try {
                // if there was something to run once entering a place, do it
                // now
                String action = triggerAtEntry;
                triggerAtEntry = "";
                arabesque.player.setPlace(to);
                bsh.eval(bshimport + action);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else
            gotoPlace(to, false);
    }

    public void gotoPlace(String to, boolean modal) {
        // entering a place
        if (!to.equals(arabesque.player.getPlace()) && !"menu".equals(to) && !"settings".equals(to) && !"intro".equals(to) && !"intro2".equals(to) && !"credits".equals(to)) {
            arabesque.player.setLastPlace(arabesque.player.getPlace());
            jinx = random.nextInt(100);
        }

        setModal(modal);
        gf.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        if ("worldmap".equals(to)) {
            player.setPlace(to);
            gotoWorldMap();
        } else {
            player.setNiceCity(places.getTitle(to));
            String old = player.getPlace();
            player.setPlace(to);
            flyover = null;
            panel.clear();
            objects.clear();
            hud.clear();
            arabesque.paintPlace(to);
            arabesque.paintGlass();
            doLayout();
            repaint();

            // the "events" in a place are triggered "before" entering the
            // place, but after
            // drawing. so it's sort of like pre-enter place event, but
            // post-draw.. if we would
            // have a true event mechanism, that is
            player.setPlace(old);
            ArrayList<Spicebean> events = places.getEvents(to);
            if (events != null && !events.isEmpty()) {
                for (Spicebean sb : events) {
                    // I love BeanShell ;-)
                    sb.ingrain();
                }
            }
            player.setPlace(to);
        }
    }

    public void gotoShop(String who) {
        gotoShop(who, Deal.LOOK, "", 0, 0);
    }

    public void gotoShop(String who, Deal deal, String id, int traderPage, int playerPage) {
        // entering a shop dialog
        jinx = random.nextInt(100);
        setModal(true);
        panel.clear();
        player.chooseFace("02");
        arabesque.paintShop(who, deal, id, traderPage, playerPage);
        doLayout();
        repaint();
    }

    public void gotoStatus(Tab tab, String id, int page, Tab from, int helpMore) {
        // entering the status dialog
        gf.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setModal(true);
        flyover = null;
        panel.clear();
        objects.clear();

        if (player.hasAllMapPieces()) {
            player.chooseFace("14");
            player.openGraveQuest();
            player.removeMapPieces();
        }

        if ("worldmap".equals(player.getPlace()))
            arabesque.paintWorldMap(true);
        else
            arabesque.paintPlace(player.getPlace());
        arabesque.paintStatus(tab, id, page, from, helpMore);
        arabesque.doLayout();
        arabesque.repaint();
    }

    public boolean isShowMapGlobe() {
        return this.showMapGlobe;
    }

    public void setShowMapGlobe(boolean show) {
        this.showMapGlobe = show;
    }

    public boolean isShowOptions() {
        return this.showOptions;
    }

    public void setShowOptions(boolean showOptions) {
        this.showOptions = showOptions;
    }

    public void loadGame() {
        try {
            jukebox.stopAll();
            FileDialog fd = new FileDialog(this, "Load game", FileDialog.LOAD);
            fd.setDirectory(".");
            fd.setFile("spicetrade.sav");
            fd.setVisible(true);
            // FIX: 12.4.2005 Changed the loading and save routine so that the directory information is used also
            String file = fd.getDirectory() + fd.getFile();
            if (fd.getFile() == null) return;
            String load = tools.readFile(file, false);
            jukebox.playSound("/music/fx_signal_bell_hitlink.ogg");
            player = (Player) xstream.fromXML(load);
            setShowOptions(true);
            //tools.showMessage(this, "Game loaded.");
            // check if the player has a travel permit
            if (player.hasAnyItems("10620", "10630"))
                setShowMapGlobe(true);
            player.chooseFace();
            refresh();
            Thread.sleep(50);
            gotoPlace(player.getPlace());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveGame() {
        try {
            // XStream.. gotta love it! I mean, how much less code can you write?
            String save = xstream.toXML(player);
            FileDialog fd = new FileDialog(this, "Save game", FileDialog.SAVE);
            fd.setDirectory(".");
            fd.setFile("spicetrade.sav");
            fd.setVisible(true);
            String file = fd.getDirectory() + fd.getFile();
            tools.writeFile(file, save);
            jukebox.playSound("/music/fx_signal_bell_hitlink.ogg");
            tools.showMessage(this, "Game saved!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveSettings() {
        try {
            Settings settings = new Settings();
            settings.setDifficulty(player.getDifficulty());
            settings.setMusicOn(jukebox.isMusicOn());
            settings.setFullScreen(player.isFullScreen());
            String save = xstream.toXML(settings);
            tools.writeFile("spicetrade.properties", save);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loadSettings() {
        try {
            String load = tools.readFile("spicetrade.properties", false);
            Settings settings = new Settings();
            if (load != null && !load.isEmpty())
                settings = (Settings) xstream.fromXML(load);
            player.setDifficulty(settings.getDifficulty());
            jukebox.setMusicOn(settings.isMusicOn());
            player.setFullScreen(settings.isFullScreen());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public Image getImage(String picture) {
        return getImage("", picture, false);
    }

    public Image getImage(String name, boolean cache) {
        return getImage(name, "", cache);
    }

    public Image getImage(String name, String picture, boolean cache) {
        return getImage(name, picture, cache, false);
    }

    public Image getImage(String name, String picture, boolean cache, boolean removeCache) {
        // I'm not all together happy with this implementation, but will have to do for a while
        Image image;
        try {
            if (name == null || name.isEmpty())
                name = picture;
            if (pictures.containsKey(name) && !removeCache)
                return pictures.get(name);
            else {
                image = tools.loadImage(this, picture);
                if (image == null) {
                    image = tools.loadImage(this, "/pics/notavailable.gif");
                    System.out.println("Image " + picture + " not available.");
                }
                pictures.remove(name);

                if (cache)
                    pictures.put(name, image);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            image = pictures.get("notavailable");
        }
        return image;
    }

    public boolean isModal() {
        return modal;
    }

    public void setModal(boolean modal) {
        this.modal = modal;
    }

    public void exit() {
        System.exit(0);
    }

    public void this_windowClosed(WindowEvent e) {
        System.exit(0);
    }

    public void this_mouseClicked(MouseEvent e) {
        try {
            // to prevent the user from double clicking on icons that generate
            // an animated action
            if (shouldDelayClick())
                Thread.sleep(1000);

            Drawables drawables = isModal() ? panel : objects;

            for (Drawable drawable : drawables)
                if (processDrawableClick(drawable, e)) {
                    return; // Found a clickable object, no need to continue
                }
    
            if (!isModal()) {
                drawables = hud;
                for (Drawable drawable : drawables)
                    if (processDrawableClick(drawable, e)) {
                        return; // Found a clickable object, no need to continue
                    }
                }
    
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void this_mouseMoved(MouseEvent e) {
        try {
            // this is for the flyover
            Drawables drawables = isModal() ? panel : objects;
            boolean repaintp = false;
    
            flyover = null;
    
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

            for (Drawable drawable : drawables)
                if (processDrawableMove(drawable, e)) {
                    repaintp = true;
                    break;
                }
    
            if (!repaintp && !isModal()) {
                drawables = hud;
                for (Drawable drawable : drawables) {
                    if (processDrawableMove(drawable, e)) {
                        repaintp = true;
                        break;
                    }
                }
            }
    
            if (repaintp) {
                if (flyover != null) {
                    flyover.x = (flyover.labelWidth * 2 > getWidth() - e.getX()) ? flyover.x - (flyover.labelWidth + 30) : flyover.x;
                    flyover.y = (40 > getHeight() - e.getY()) ? flyover.y - 40 : flyover.y;
                }
                repaint();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean shouldDelayClick() {
        return ("fields".equals(player.getPlace()) || "witchmountain".equals(player.getPlace()) || "hermitcave".equals(player.getPlace()));
    }
    
    private boolean processDrawableClick(Drawable drawable, MouseEvent e) {
        if (isClickable(drawable, e)) {
            String label = getDrawableLabel(drawable);
            logIfNecessary(label, drawable.action);
            evalAction(drawable.action);
            return true; // Click processed
        }
        return false; // Click not processed
    }
    
    private boolean processDrawableMove(Drawable drawable, MouseEvent e) {
        if (isClickable(drawable, e)) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            String label = getDrawableLabel(drawable);
            flyover = new Drawable(e.getX() + 10, e.getY() + 10, 300, 20, UIType.FLYOVER, label);
            return true;
        }
        return false;
    }
    
    private boolean isClickable(Drawable drawable, MouseEvent e) {
        return drawable.contains(e.getPoint()) && drawable.action != null;
    }
    
    private void logIfNecessary(String label, String action) {
        if (!nonLoggables.contains(label)) {
            player.addLog(label, action, (player.getDay() + player.getMonth() * 30 + player.getYear() * 360));
        }
    }
    
    private void evalAction(String action) {
        try {
            bsh.eval(bshimport + action);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private String getDrawableLabel(Drawable drawable) {
        if ("worldmap".equals(player.getPlace())) {
            return drawable.label != null ? drawable.label : drawable.action;
        } else {
            return (drawable.tooltip != null) ? drawable.tooltip : (drawable.label != null) ? drawable.label : drawable.action;
        }
    }

    public void refresh() {
        // FIX 10.04.2005 - the journal.refresh() is required when loading game 
        player.getJournal().refresh();
    }
}
