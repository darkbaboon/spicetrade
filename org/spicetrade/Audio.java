/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade;

import org.spicetrade.tools.Sound;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

public class Audio {

    public boolean musicOn = true;
    public String lastMusic = "";
    public String currentMusic = "";
    ConcurrentHashMap<String, Sound> jukebox = new ConcurrentHashMap<>();

    public void playSound(String file, boolean loop) {
        if (!musicOn) return;
        try {
            if (isPlaying(file)) return;
            else if (has(file)) jukebox.remove(file);
            Sound sound = new Sound();
            sound.start(file, loop);
            jukebox.put(file, sound);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void playSound(String file) {
        playSound(file, false);
    }

    public void loopSound(String file) {
        playSound(file, true);
    }

    public void playMusic(String file) {
        if (!musicOn) return;
        try {
            if (isPlaying(file)) return;
            stopAll();
            playSound(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loopMusic(String file) {
        if (!musicOn) return;
        try {
            if (isPlaying(file)) return;
            lastMusic = currentMusic;
            currentMusic = file;
            stopAll();
            loopSound(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void stopSound(String file) {
        try {
            if (!isPlaying(file)) return;
            Sound sound = jukebox.get(file);
            sound.stop();
            jukebox.remove(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void stopAll() {
        Sound sound;
        for (Iterator<Sound> iterator = jukebox.values().iterator(); iterator.hasNext(); ) {
            sound = iterator.next();
            sound.stop();
        }
        jukebox.clear();
    }

    public boolean has(String file) {
        return jukebox.containsKey(file);
    }

    public boolean isPlaying(String file) {
        if (has(file)) {
            Sound sound = jukebox.get(file);
            return sound.playing;
        } else return false;
    }

    public void setMusicOn(boolean musicOn) {
        this.musicOn = musicOn;
        if (!musicOn) stopAll();
    }

    public boolean isMusicOn() {
        return musicOn;
    }
}