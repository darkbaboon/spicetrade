/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade;

import org.spicetrade.collections.Journal;
import org.spicetrade.tools.Inventory;
import org.spicetrade.tools.Item;
import org.spicetrade.tools.LogItem;
import org.spicetrade.tools.Transport;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.ArrayList;

public class Player {

    public static final String[] cities = {"baghdad", "anjoudan", "najaf", "latakia", "konya", "baku", "constantinople", "venice", "madrid", "lisboa",
            "budapest", "vienna", "moscow", "hamburg", "amsterdam", "london", "paris"};
    public static final String[] europeanCities = {"venice", "madrid", "lisboa", "budapest", "vienna", "moscow", "hamburg", "amsterdam", "london", "paris"};
    public static final String[] nearEastCities = {"baghdad", "anjoudan", "najaf", "latakia", "konya", "baku", "constantinople"};
    public static final String[] niceCities = {"Baghdad", "Anjoudan", "Najaf", "al-Ladiqiyah", "Konya", "Baku", "Constantinople", "Venice", "Madrid",
            "Lisbon", "Budapest", "Vienna", "Moscow", "Hamburg", "Amsterdam", "London", "Paris"};
    public static final String[] niceNearEastCities = {"Baghdad", "Anjoudan", "Najaf", "al-Ladiqiyah", "Konya", "Baku", "Constantinople"};
    final Mainframe frame;
    public HashMap<String, String> attr;
    public Inventory soldItems;
    public Inventory items;
    public ArrayList<LogItem> log;
    public boolean fullScreen;
    public int x;
    public int y;
    public int xHomeCountry;
    public int yHomeCountry;
    public int year;
    public int month;
    public int day;
    public int age;
    public String ageType;
    public double health;
    public int money;
    public int happiness;
    public int moral;
    public int force;
    public int baseForce;
    public int culture;
    public int economy;
    public int wives;
    public int children;
    public String place;
    public String city;
    public String nicecity;
    public String lastPlace;
    public String logo;
    public String lastWords;
    public boolean inGame;
    public boolean inBattle;
    public boolean inGameOver;
    public Transport transport;
    public Journal journal;
    public boolean addictedHashish;
    public boolean addictedOpium;
    public boolean sickPlague;
    public boolean sickGeneral;
    public boolean sickPoisoned;
    public int deathType;
    public int difficulty;
    public String statusFace = "/pics/abu/face/young01.png";
    public Random random;

    public Player() {
        this(1);
    }

    public Player(int difficulty) {
        frame = Mainframe.me;
        this.difficulty = difficulty;
        attr = new HashMap<>();
        soldItems = new Inventory();
        items = new Inventory();
        journal = new Journal();
        log = new ArrayList<>();
        x = 0;
        y = 0;
        xHomeCountry = 0;
        yHomeCountry = 0;
        moral = 0;
        inGameOver = false;
        addictedHashish = false;
        addictedOpium = false;
        sickPlague = false;
        inBattle = false;
        lastWords = "";
        ageType = "young";
        attr.put("Name", "Abu Mansur 'abd ar-Rahman al-Qazzaz");
        attr.put("dateAndPlace", "C. 500 AH, Baghdad");
        attr.put("wives", "");
        attr.put("children", "");
        random = new Random();

        for (String city : cities) {
            int c = random.nextInt(100);
            while (c < getThreshold(city)) {
                c = random.nextInt(100);
            }
            attr.put("culture" + city, String.valueOf(getMultiplier(city) * c));
        }

        attr.put("Moneyunit", "Dirham");
        attr.put("Timeunit", "AH");
        attr.put("Special", "Navigation");
        year = 500;
        month = 4;
        day = 7;
        age = 17;
        health = (difficulty == 1) ? 50 : 80;
        happiness = 30;
        money = (difficulty == 1) ? 25 : 9999;
        baseForce = (difficulty == 1) ? 2 : 5;
        force = baseForce;
        economy = 0;
        culture = 0;
        wives = 0;
        children = 0;
        place = "";
        lastPlace = "";
        logo = "logoIntro";
        to(141, 421);
        toHomeCountry(249, 135, "baghdad", "Baghdad");
        buyItem("12050", 0); // home
        buyItem("14000", 0); // fields
        buyItem("10000", 0); // basic food
        buyItem("10300", 0); // ability to walk

        frame.setShowMapGlobe(false);
        inGame = false;
        transport = Transport.WALK;
    }

    // define the threshold and multiplier for each city
    private int getThreshold(String city) {
        switch (city) {
            case "baghdad":
            case "venice":
            case "paris":
            case "moscow":
            case "london":
                return 60; // 60 : baghdad, venice, paris, moscow, london
            case "madrid":
            case "hamburg":
                return 50; // 50 : madrid, hamburg
            case "anjoudan":
            case "najaf":
            case "latakia":
            case "lisboa":
            case "amsterdam":
                return 40; // 40 : anjoudan, najaf, latakia, lisboa, amsterdam
            case "konya":
            case "baku":
            case "vienna":
            case "budapest":
                return 30; // 30 : konya, baku, vienna, budapest
            default:
                return 20; // 20 : constantinople
        }
    }

    public int getAge() {
        return age;
    }

    public void inJail(int sentence) {
        this.age += sentence;
        this.year += sentence;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCulture() {
        return culture;
    }

    public void setCulture(int culture) {
        this.culture = culture;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getStrengh() {
        return force;
    }

    public void setStrengh(int force) {
        this.force = force;
    }

    public boolean isFullScreen() {
        return fullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        this.fullScreen = fullScreen;
    }

    public double getHealth() {
        return health;
    }

    public void revitalize(double amount) {
        health += amount;
        if (health > 300) {
            health = 300;
        }
    }

    public void deplete(double amount) {
        health -= amount;
        if (health < 0) {
            health = 0;
        }
    }

    public boolean getInBattle() {
        return inBattle;
    }

    public void setInBattle(boolean inBattle) {
        this.inBattle = inBattle;
    }

    public boolean isAddictedHashish() {
        return addictedHashish;
    }

    public boolean isAddictedOpium() {
        return addictedOpium;
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public String getLastWords() {
        return lastWords;
    }

    public void setLastWords(String lastWords) {
        this.lastWords = lastWords;
    }


    public void addMoney(int amount) {
        money += amount;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void subMoney(int amount) {
        money -= amount;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMoral() {
        return moral;
    }

    public void setMoral(int moral) {
        this.moral = moral;
    }

    public String getNiceCity() {
        return nicecity;
    }

    public void setNiceCity(String nicecity) {
        this.nicecity = nicecity;
    }

    public String getStatusFace() {
        return statusFace;
    }

    public void setStatusFace(String statusFace) {
        this.statusFace = statusFace;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    private int getMultiplier(String city) {
        return Arrays.asList(europeanCities).contains(city) ? -1 : 1;
    }

    public String get(String key) {
        return attr.getOrDefault(key, null);
    }

    public boolean has(String key) {
        return attr.containsKey(key);
    }

    public void add(String key) {
        attr.put(key, "");
    }

    public void add(String key, String value) {
        attr.put(key, value);
    }

    public void remove(String key) {
        attr.remove(key);
    }

    public boolean contains(String key, String value) {
        return (get(key).contains(value));
    }

    public void addWife(String name) {
        if (hasWife(name))
            return;
        String all = attr.get("wives");
        if (all == null || all.isEmpty())
            all = name;
        else
            all += ", " + name;
        attr.put("wives", all);
        wives++;
    }

    public boolean hasWife(String name) {
        return contains("wives", name);
    }

    public void removeWife(String name) {
        if (!hasWife(name)) return;
        String all = attr.get("wives");
        if (all.indexOf(name) == 0)
            if (!all.contains(","))
                all = "";
            else
                all = all.substring(all.indexOf(","));
        else
            all = all.substring(all.indexOf(", " + name) + name.length() + 2);
        attr.put("wives", all);
        wives--;
    }

    public void addChild(String name) {
        if (hasChild(name)) return;
        String all = attr.get("children");
        if (all == null || all.isEmpty())
            all = name;
        else
            all += ", " + name;
        attr.put("children", all);
        children++;
    }

    public boolean hasChild(String name) {
        return contains("children", name);
    }

    public void removeChild(String name) {
        if (!hasChild(name)) return;
        String all = attr.get("children");
        if (all.indexOf(name) == 0)
            if (!all.contains(","))
                all = "";
            else
                all = all.substring(all.indexOf(","));
        else
            all = all.substring(all.indexOf(", " + name) + name.length() + 2);
        attr.put("children", all);
        children--;
    }

    public void addLog(String id, String action, int when) {
        if (inGame)
            log.add(new LogItem(id, action, when));
        if (log.size() > 100)
            log.remove(0);
    }

    public Inventory getInventory() {
        Inventory inventory = new Inventory();
        for (Item item : items) {
            if (item.inventory) inventory.add(item);
        }
        return inventory;
    }

    public void buyItem(String id) {
        try {
            Item item = frame.market.getItem(id);
            buyItem(id, item.getPrice());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buyItem(String id, int price, boolean only) {
        try {
            if (only && !hasItem(id))
                buyItem(id, price);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buyItem(String id, int price, String who) {
        try {
            for (Item item : soldItems) {
                if (item.who.equals(who) && item.getId().equals(id)) {
                    buyItem(id, price);
                    soldItems.remove(item);
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buyItem(String id, int price) {
        try {
            Item item = frame.market.getItem(id);
            money -= price;
            if ("16000".equals(id) || "16010".equals(id) || "16020".equals(id) || "16030".equals(id))
                item.addName(get("worker"));
            frame.jukebox.playSound("/music/fx_hit.ogg");
            items.add(item);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean hasSold(String id) {
        return hasSold(id, null);
    }

    public boolean hasSold(String id, String who) {
        return soldItems.stream()
                .map(item -> (Item) item)
                .anyMatch(item -> item.getId().equals(id) && (who == null || who.equals(item.who)));
    }    

    public void sellItem(String id) {
        try {
            Item item = frame.market.getItem(id);
            sellItem(id, item.getPrice());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sellItem(String id, int price) {
        try {
            if (journal.has("field2"))
                journal.done("field2", "§- I sold some spices in " + nicecity);
            money += price;
            frame.jukebox.playSound("/music/fx_hit.ogg");
            removeItem(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Inventory getSoldItems(String who) {
        Inventory items = new Inventory();

        try {
            for (Item item : soldItems) {
                if (who.equals(item.who))
                    items.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return items;
    }

    public void sellItem(String id, int price, String who) {
        if (hasItem(id)) {
            Item item = frame.market.getItem(id);
            removeItem(id);
            item.who = who;
            money += price;
            soldItems.add(item);
        }
    }

    public boolean hasItem(String id) {
        return hasItem(id, 1);
    }

    public boolean hasItem(String id, int amount) {
        return hasItem(id, amount, true);
    }

    public boolean hasItem(String id, int amount, boolean givenItems) {
        int count = 0;
        boolean bool = false;
        for (Item item : items) {
            if (item.getId().equals(id))
                count++;
        }

        for (Item item : soldItems) {
            if (item.getId().equals(id))
                count++;
        }
        if (attr.containsKey(id) && givenItems)
            count++;
        if (count >= amount)
            bool = true;
        return bool;
    }


    public int amountItem(String id) {
        int amount = 0;
        for (Item item : items) {
            if (item.getId().equals(id))
                amount++;
        }
        return amount;
    }
    
    public boolean hasAnyItems(String id1, String id2) {
        String[] ids = new String[]{id1, id2};
        return hasAnyItems(ids);
    }

    public boolean hasAnyItems(String[] ids) {
        for (Item item : items) {
            for (String id : ids)
                if (item.getId().equals(id))
                    return true;
        }
        return false;
    }

    public boolean canBuy(String id) {
        try {
            return (money >= frame.market.getPrice(id));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean hasMoney(int compare) {
        return money >= compare;
    }

    public boolean hasMoreMoney(int compare) {
        return money > compare;
    }

    public void removeItem(String id) {
        for (Item item : items) {
            if (!item.getId().equals(id))
                continue;
            items.remove(item);
            break;
        }
    }

    public void giveItem(String id) {
        removeItem(id);
        attr.put(id, "");
    }

    public String getLastPlace() {
        return lastPlace;
    }

    public void setLastPlace(String lastPlace) {
        this.lastPlace = lastPlace;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean inPlace(String where) {
        return inPlace(where, false);
    }

    public boolean inPlace(String where, boolean exact) {
        return (exact) ? (where.equals(place)) : (place.contains(where));
    }

    public void to(int _x, int _y) {
        x = _x;
        y = _y;
    }

    public void toHomeCountry(int _x, int _y, String _city, String _nicecity) {
        xHomeCountry = _x;
        yHomeCountry = _y;
        city = _city;
        nicecity = _nicecity;
    }

    public void nextDay() {
        nextDay(false);
    }

    public void nextDay(int amount) {
        nextDay(amount, false);
    }

    public void nextDay(int amount, boolean travelling) {
        for (int i = 0; i < amount; i++)
            nextDay(travelling);
    }

    public void nextDay(boolean travelling) {
        if (day == 30)
            nextMonth(travelling);
        day++;

        if (health > 300) health = 300; // have to have a max value

        if (travelling)
            switch (transport) {
                case WALK:
                    health -= (difficulty == 1) ? 0.5 : 0.3;
                    break;
                case HORSE:
                    health -= (difficulty == 1) ? 0.4 : 0.2;
                    break;
                case CARAVAN:
                    health -= (difficulty == 1) ? 0.4 : 0.2;
                    break;
                case SULEIMAN:
                    health -= (difficulty == 1) ? 0.2 : 0.1;
                    break;
                case BOAT:
                    health -= (difficulty == 1) ? 0.4 : 0.2;
                    break;
                case BORAK:
                    health -= (difficulty == 1) ? 0.2 : 0.1;
                    break;
                case DOG:
                    health= (difficulty == 1) ? 0.2 : 0.1;
                    break;
            }

        if (sickGeneral) {
            health -= 0.01;

        } else if (sickPlague) {
            health -= 0.1;
        } else if (sickPoisoned) {
            health -= 1;
        }

        if (sickPoisoned) {
            int value = Integer.parseInt(attr.get("poisoned"));
            value++;
            attr.put("poisoned", String.valueOf(value));
            if (value >= 100)
                frame.triggerAtEntry = "frame.gotoDeath(\"Abu died of poisoning.\", 3);";
        }

        if (health < 1) {
            // died, game over
            frame.triggerAtEntry = "frame.gotoDialog(\"9110\");";
        }
    }

    public void nextMonth() {
        nextMonth(false);
    }

    public void nextMonth(boolean travelling) {
        day = 1;
        month++;

        if (month > 12) {
            month = 1;
            nextYear();
        }

        int curse = random.nextInt(100);
        ageType = "young";

        int randomEffect = 0;
        force = baseForce;
        for (Item item : items) {
            if (item.getId().length() == 5) {
                health += item.health;
                money += item.monthlyCost;
                culture += item.getCulture();
                economy += item.getEconomy();
                force += item.getForce();
                happiness += item.happiness;
                randomEffect += item.random;
            }
        }

        if ((hasItem("11000") && hasItem("11001") && hasItem("11002") && hasItem("11003") && hasItem("11004") && hasItem("11005"))
                || (hasItem("11010") && hasItem("11011") && hasItem("11012") && hasItem("11013") && hasItem("11014") && hasItem("11015"))) {
            logo = "logoAmulet";
            happiness++;
            health += 5;
            baseForce++;
            randomEffect += 5;
        } else {
            // Yima
            if (journal.has("yima1") && !journal.contains("yima1", "cursed by Yima")) {
                int value = Integer.parseInt(journal.get("yima1").substring(1));
                value--;
                journal.put("yima1", "!" + value);

                if (value < 1) {
                    if (curse >= 0 && curse < 25 - randomEffect) {
                        curseByYima("random2");
                    } else if (curse >= 25 && curse <  50 - randomEffect) {
                        curseByYima("random3");
                    } else if (curse >= 50 && curse <  65 - randomEffect) {
                        curseByYima("random1");
                    } else if (curse >= 65 && curse <  70 - randomEffect) {
                        curseByYimaWithAddiction("plague", 10);
                    }
                }
            }
        }

        // Iblis
        if (journal.has("iblis1") && journal.get("iblis1").charAt(0) == '!') {
            int value = Integer.parseInt(journal.get("iblis1").substring(1));
            value--;
            journal.put("iblis1", "!" + value);
            if (value <= 0 && curse >= 0 && curse <  75 - randomEffect) {
                frame.triggerAtEntry = "frame.gotoPlace(\"supernaturalbad2\");frame.gotoDialog(\"1571\");";
                logo = "logoEvil";
            }
        }

        // permits {travel, museum, mosque, export, library, shrine, shrine}
        String[] permits = {"permit1", "permit2", "permit3", "permit4", "permit10", "permit11", "permit12"};

        for (String permit : permits) {
            if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
                int value = Integer.parseInt(journal.get(permit).substring(1));
                value--;
                journal.put(permit, "!" + value);
                if (value <= 0) {
                    if ("permit12".equals(permit)) {
                        journal.put(permit, "§- The permit can be retrieved from the king's official");
                    } else {
                        journal.put(permit, "§- The permit can be retrieved from the sultan's official");
                    }
                }
            }
        }

        // Umm
        if (journal.has("abdullah2") && journal.get("abdullah2").charAt(0) == '!') {
            String parse = journal.get("abdullah2").substring(2, (journal.get("abdullah2")).length());
            journal.put("abdullah2", parse + "§- I waited one month to see Umm again");
        }

        // culture
        for (String city : cities) {
            Inventory museumItems = itemsList(city + "museum");
            if (has("museum" + city)) {
                for (Item item : museumItems) {
                    int cultureChange = ("baghdad".equals(city)) ? item.getCulture() / 2 : item.getCulture() / 4;
                    for (String otherCity : cities) {
                        addCulture(otherCity, cultureChange);
                    }
                    if (!"baghdad".equals(city))
                        addCulture(city, item.getCulture());
                }
            }

            if (getCulture(city) < 0) {
                boolean destroy = false;
                // they don't appreciate you
                if (has("museum" + city) && !journal.contains("permit" + city, "destroyed") && frame.triggerAtEntry.isEmpty()) {
                    switch (city) {
                        case "amsterdam":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3340\");";
                            break;
                        case "venice":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3341\");";
                            break;
                        case "budapest":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3342\");";
                            break;
                        case "vienna":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3343\");";
                            break;
                        case "moscow":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3344\");";
                            break;
                        case "madrid":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3345\");";
                            break;
                        case "lisboa":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3346\");";
                            break;
                        case "paris":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3347\");";
                            break;
                        case "london":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3348\");";
                            break;
                        case "hamburg":
                            frame.triggerAtEntry = "frame.gotoDialog(\"3349\");";
                            break;
                    }
                    // destroy museum and collections in there
                    destroy = true;
                    museumItems = itemsList(city + "museum");
                    for (Item item : museumItems) {
                        giveItem(item.getId());
                    }
                    journal.add("permit" + city, "§- They destroyed my museum and my collections");
                    add("removemuseum" + city);
                }

                if (has("mosque" + city)) {
                    if ((destroy || frame.triggerAtEntry.isEmpty()) && !journal.contains("church" + city, "mosque is gone")) {
                        switch (city) {
                            case "amsterdam":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3330\");" : "frame.gotoDialog(\"3320\");";
                                break;
                            case "venice":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3331\");" : "frame.gotoDialog(\"3321\");";
                                break;
                            case "budapest":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3332\");" : "frame.gotoDialog(\"3322\");";
                                break;
                            case "vienna":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3333\");" : "frame.gotoDialog(\"3323\");";
                                break;
                            case "moscow":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3334\");" : "frame.gotoDialog(\"3324\");";
                                break;
                            case "madrid":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3335\");" : "frame.gotoDialog(\"3325\");";
                                break;
                            case "lisboa":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3336\");" : "frame.gotoDialog(\"3326\");";
                                break;
                            case "paris":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3337\");" : "frame.gotoDialog(\"3327\");";
                                break;
                            case "london":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3338\");" : "frame.gotoDialog(\"3328\");";
                                break;
                            case "hamburg":
                                frame.triggerAtEntry = destroy ? "frame.gotoDialog(\"3339\");" : "frame.gotoDialog(\"3329\");";
                                break;
                        }
                        // destroy mosque
                        journal.add("church" + city, "§- The mosque is gone and a church stands in it's place");
                        add("removemosque" + city);
                    }
                }

            } else if (getCulture(city) > 49 && !has("museum" + city) && !journal.has("permit" + city)
                        && !journal.isDone("permit" + city) && frame.triggerAtEntry.isEmpty()) {
                // they really like you and want you to build a museum
                switch (city) {
                    default:
                        break;
                    case "amsterdam":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3300\");";
                        break;
                    case "venice":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3301\");";
                        break;
                    case "budapest":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3302\");";
                        break;
                    case "vienna":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3303\");";
                        break;
                    case "moscow":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3304\");";
                        break;
                    case "madrid":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3305\");";
                        break;
                    case "lisboa":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3306\");";
                        break;
                    case "paris":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3307\");";
                        break;
                    case "london":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3308\");";
                        break;
                    case "hamburg":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3309\");";
                        break;
                }
            } else if ((getCulture(city) > 79) && !has("mosque" + city) && !journal.has("church" + city)
                        && !journal.isDone("church" + city) && frame.triggerAtEntry.isEmpty()) {
                // they worship you and will change their churches to mosques
                switch (city) {
                    default:
                        break;
                    case "amsterdam":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3310\");";
                        break;
                    case "venice":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3311\");";
                        break;
                    case "budapest":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3312\");";
                        break;
                    case "vienna":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3313\");";
                        break;
                    case "moscow":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3314\");";
                        break;
                    case "madrid":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3315\");";
                        break;
                    case "lisboa":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3316\");";
                        break;
                    case "paris":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3317\");";
                        break;
                    case "london":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3318\");";
                        break;
                    case "hamburg":
                        frame.triggerAtEntry = "frame.gotoDialog(\"3319\");";
                        break;
                }
            }

            if (getCulture(city) < 0 && !has("church" + city) && frame.triggerAtEntry.isEmpty()) {
                add("church" + city);
                switch (city) {
                    default:
                        break;
                    case "baghdad":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5100\");";
                        break;
                    case "najaf":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5101\");";
                        break;
                    case "anjoudan":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5102\");";
                        break;
                    case "latakia":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5103\");";
                        break;
                    case "konya":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5104\");";
                        break;
                    case "baku":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5105\");";
                        break;
                    case "constantinople":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5106\");";
                        break;
                }
            }

            if (getCulture(city) > 9 && has("church" + city) && frame.triggerAtEntry.isEmpty()) {
                add("mosque" + city);
                remove("church" + city);
                switch (city) {
                    default:
                        break;
                    case "baghdad":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5110\");";
                        break;
                    case "najaf":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5111\");";
                        break;
                    case "anjoudan":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5112\");";
                        break;
                    case "latakia":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5113\");";
                        break;
                    case "konya":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5114\");";
                        break;
                    case "baku":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5115\");";
                        break;
                    case "constantinople":
                        frame.triggerAtEntry = "frame.gotoDialog(\"5116\");";
                        break;
                }
            }
        }

        boolean isMahdiOrYimaDone = journal.isDone("mahdi1") || journal.isDone("yima1") || journal.isDone("grave3");
        boolean isMoneyConditionMet = (money > (difficulty == 0 ? 10000 : 20000));
        
        if (age > (difficulty == 0 ? 19 : 25) && isMoneyConditionMet || isMahdiOrYimaDone && !journal.has("permit2")
            && !journal.isDone("permit2") && frame.triggerAtEntry.isEmpty()) {
            journal.open("permit2");
            add("buildingpermit");
            frame.triggerAtEntry = "frame.gotoDialog(\"3110\");";
        } else if (age > (difficulty == 0 ? 24 : 29) && isMoneyConditionMet || isMahdiOrYimaDone && !journal.has("permit10")
            && !journal.isDone("permit10") && frame.triggerAtEntry.isEmpty() && journal.isDone("permit2")) {
            journal.open("permit10");
            add("buildingpermit");
            frame.triggerAtEntry = "frame.gotoDialog(\"3080\");";
        }

        // addictions
        boolean addicted = false;
        String drug = "";
        String itemId = "";
        if (addictedHashish) {
            addicted = true;
            drug = "hashish";
            itemId = "10220";
        } else if (addictedOpium) {
            addicted = true;
            itemId = "10230";
            drug = "opium";
        }
        
        if (addicted) {
            if (hasItem(itemId)) {
                removeItem(itemId);
                addLog("You smoked " + drug, "result=false;", day + month * 30 + year * 360);
            } else {
                if ("hashish".equals(drug))
                    health -= 5;
                else if ("opium".equals(drug))
                    health -= 20;
                addLog("You had no " + drug + ", even though you are addicted to it. You feel weaker.", "result=false;", day + month * 30 + year * 360);
            }
        }

        String[] armies = {"venice", "lisboa", "paris", "hamburg", "moscow", "budapest", "vienna", "madrid", "london", "amsterdam"};
        int[] ageThresholds = {18, 20, 23, 24, 27, 29, 34, 35, 39, 44};
        String[] quests = {"random1", "random4", "random5", "random6", "random7", "random8", "random9", "random10", "random11", "random12"};
        for (int i = 0; i < armies.length; i++) {
            if (!journal.isDone(quests[i]) && hasItem("16020") && age > ageThresholds[i] && (i == 0 || journal.isDone(quests[i - 1]))) {
                add("foreignarmy", armies[i]);
                journal.open(quests[i], "§- The army of " + frame.armies.getName(armies[i]) + " is attacking your country!");
                break;
            }
        }

        int allCulture = 0;
        for (String city : cities) allCulture += getCulture(city);
        culture = allCulture / cities.length;

        String[] shopCities = {"amsterdam", "budapest", "constantinople", "hamburg", "lisboa", "london", "madrid", "moscow", "paris", "venice", "vienna"};
        String[] shops = {"12080", "12081", "12082", "12083", "12084", "12085", "12086", "12087", "12088", "12089", "12090"};
        for (int i = 0; i < shops.length; i++)
            if (hasItem(shops[i]))
                putShopMoney(shopCities[i], 150);

        chooseFace(true);
    }

    private void curseByYima(String randomKey) {
        journal.put("yima1", "§- I was cursed by Yima");
        journal.open(randomKey);
        logo = "logoEvil";
    }
    
    private void curseByYimaWithAddiction(String addiction, int amount) {
        journal.put("yima1", "§- I was cursed by Yima");
        addAddiction(addiction, amount);
        logo = "logoEvil";
    }

    public void nextYear() {
        age++;
        year++;
        // adjust baseForce based on difficulty level
        baseForce += (difficulty == 0) ? 5 : 3;
        // reset map piece counters
        for (String city : cities) attr.remove(city + "mappiece");

        // update cultures for european cities
        int[] cultures = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
        int whole = 0;

        for (int i = 0; i < europeanCities.length; i++) {
            whole += cultures[i];
            int randomCultureChanges = random.nextInt((int) cultures[i]);
            if (random.nextInt(10) > 7)
                removeCulture(europeanCities[i], randomCultureChanges);
            else
                addCulture(europeanCities[i], randomCultureChanges);
        }

        // update cultures for near east cities
        int[] cultureChanges = {whole / 10, whole / 8, whole / 8, whole / 6, whole / 6, whole / 6, whole / 3};
        for (int i = 0; i < nearEastCities.length; i++) {
            if (random.nextInt(10) > 7) {
                addCulture(nearEastCities[i], cultureChanges[i]);
            } else {
                removeCulture(nearEastCities[i], cultureChanges[i]);
            }
        }

        // died of age
        if ((age > 50 && health < 60 && random.nextInt(100) < age) || (age > 70 && random.nextBoolean())) {
            frame.triggerAtEntry = "frame.gotoDialog(\"9120\");";
        }
    }

    public void chooseFace() {
        chooseFace(false, "");
    }

    public void chooseFace(String number) {
        chooseFace(false, number);
    }

    public void chooseFace(boolean nextMonth) {
        chooseFace(nextMonth, "");
    }

    public void chooseFace(boolean nextMonth, String number) {
        String path = "/pics/abu/face/";
        
        if (age > 49)
            ageType = "old";
        else if (age > 29)
            ageType = "middleaged";
        else
            ageType = "young";
    
        if (number.isEmpty()) {
            statusFace = getStatusFace(path);
        } else {
            statusFace = path + ageType + number + ".png";
        }
    }
    
    private String getStatusFace(String path) {
        if (health < 10) {
            return path + ageType + "08.png";
        } else if (health < 20) {
            return path + ageType + "12.png";
        } else if (health < 35) {
            return getStatusFaceBasedOnMoral(path, "04.png");
        } else if (health < 60) {
            return getStatusFaceBasedOnMoralOrMoney(path, "01.png", "15.png");
        } else if (health < 75) {
            return getStatusFaceBasedOnMoral(path, "06.png");
        } else if (health < 90) {
            return getStatusFaceBasedOnMoral(path, "09.png");
        } else {
            return getStatusFaceBasedOnMoral(path, "11.png");
        }
    }
    
    private String getStatusFaceBasedOnMoral(String path, String defaultFace) {
        if (moral < -75) {
            return path + ageType + "07.png";
        } else if (moral > 75) {
            return path + ageType + "17.png";
        } else {
            return path + ageType + defaultFace;
        }
    }
    
    private String getStatusFaceBasedOnMoralOrMoney(String path, String defaultFace, String altFace) {
        if (moral < -75) {
            return path + ageType + "07.png";
        } else if (moral > 75) {
            return path + ageType + "17.png";
        } else if (money < 200 && frame.jinx < 2) {
            return path + ageType + altFace;
        } else {
            return path + ageType + defaultFace;
        }
    }

    public void addGood(int amount) {
        addBad(-amount);
    }

    public void addBad(int amount) {
        if (moral >= (-100 - amount) && moral <= (100 + amount)) {
            moral -= amount;
        }

        chooseFace();
    }

    public void addAddiction(String addiction, int amount) {
        int value = 0;
        if (attr.containsKey(addiction))
            value = Integer.parseInt(attr.get(addiction));
        value += amount;
        attr.put(addiction, String.valueOf(value));

        switch (addiction) {
            case "hashish":
                if (value > 20 && frame.jinx < 60)
                    addictedHashish = true;
                else if (value < 20)
                    addictedHashish = false;
                break;
            case "opium":
                if (value > 20 && frame.jinx < 80)
                    addictedOpium = true;
                else if (value < 20)
                    addictedOpium = false;
                break;
            case "sick":
                if (value > 0)
                    sickGeneral = true;
                else if (value <= 0)
                    sickGeneral = false;
                break;
            case "poisoned":
                if (value > 0)
                    sickPoisoned = true;
                else if (value <= 0)
                    sickPoisoned = false;
                break;
            case "plague":
                if (value > 0)
                    sickPlague = true;
                else if (value <= 0)
                    sickPlague = false;
                break;
        }

        logo = "logoIntro";

        if (addictedHashish)
            logo = "logoHashish";
        if (addictedOpium)
            logo = "logoOpium";
        if (sickPlague || sickGeneral || sickPoisoned)
            logo = "logoPlague";

        chooseFace();
    }

    public void removeAddiction(String addiction) {
        switch (addiction) {
            case "hashish":
                attr.remove(addiction);
                addictedHashish = false;
                break;
            case "opium":
                attr.remove(addiction);
                addictedOpium = false;
                break;
            case "sick":
                attr.remove(addiction);
                sickGeneral = false;
                break;
            case "poisoned":
                attr.remove(addiction);
                sickPoisoned = false;
                break;
            case "plague":
                attr.remove(addiction);
                sickPlague = false;
                break;
        }
        logo = "logoIntro";
        chooseFace();
    }

    public void removeAddictions() {
        removeAddiction("hashish");
        removeAddiction("opium");
        removeAddiction("sick");
        removeAddiction("poisoned");
        removeAddiction("plague");
    }

    public void switchItem(String id, String id2) {
        switchItem(id, id2, false);
    }

    public void switchItem(String id, String id2, boolean out) {
        try {
            if (!hasItem(id))
                return;
            removeItem(id);
            Item item = frame.market.getItem(id2);
            if (!out)
                item.where = place;
            items.add(item);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean itemHere(String id) {
        for (Item item : items) {
            if (item.getId().equals(id) && item.here())
                return true;
        }
        return false;
    }

    public boolean noItemsHere(String id1, String id2, String id3, String id4, String id5, String id6) {
        String[] ids = {id1, id2, id3, id4, id5, id6};
        return noItemsHere(ids);
    }

    public boolean noItemsHere(String id1, String id2, String id3, String id4, String id5) {
        String[] ids = {id1, id2, id3, id4, id5};
        return noItemsHere(ids);
    }

    public boolean noItemsHere(String id1, String id2, String id3, String id4) {
        String[] ids = {id1, id2, id3, id4};
        return noItemsHere(ids);
    }

    public boolean noItemsHere(String id1, String id2, String id3) {
        String[] ids = {id1, id2, id3};
        return noItemsHere(ids);
    }

    public boolean noItemsHere(String id1, String id2) {
        String[] ids = {id1, id2};
        return noItemsHere(ids);
    }

    public boolean noItemsHere(String[] ids) {
        for (Item item : items) {
            for (String id : ids)
                if (item.getId().equals(id) && item.here())
                    return false;
        }
        return true;
    }

    public int itemsHere() {
        return itemsHere(place);
    }

    public int itemsHere(String place) {
        int counter = 0;
        for (Item item : items) {
            if (item.here(place)) counter++;
        }
        return counter;
    }

    public Inventory itemsList(String place) {
        Inventory items = new Inventory();

        for (Item item : items) {
            if (item.here(place)) items.add(item);
        }
        return items;
    }

    public void addCulture(String where, int amount) {
        removeCulture(where, -amount);
    }

    public void removeCulture(String where, int amount) {
        try {
            String parse = attr.get("culture" + where);
            int value = Integer.parseInt(parse);
            amount = -amount;
            value += amount;
            attr.put("culture" + where, String.valueOf(value));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getCulture(String where) {
        try {
            String parse = attr.get("culture" + where);
            int value = Integer.parseInt(parse);
            return value;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public void getShopMoney(String where) {
        try {
            String[] shops = {"amsterdam", "budapest", "constantinople", "hamburg", "lisboa", "london", "madrid", "moscow", "paris", "venice", "vienna"};

            if ("all".equals(where)) {
                for (String shop : shops) {
                    processShop(shop);
                }
            } else if (has(where + "shop")) {
                processShop(where);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void processShop(String shop) {
        if (has(shop + "shop")) {
            String parse = attr.get(shop + "shop");
            int value = Integer.parseInt(parse);
            money += value;
            attr.put(shop + "shop", "0");
        }
    }

    public void putShopMoney(String where, int amount) {
        try {
            if (has(where + "shop")) {
                String parse = attr.get(where + "shop");
                int value = Integer.parseInt(parse);
                value += amount;
                attr.put(where + "shop", String.valueOf(value));
            } else
                attr.put(where + "shop", "0");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean hasAllMapPieces() {
        return hasItem("11500") && hasItem("11510") && hasItem("11520") && hasItem("11530");
    }
    
    public void removeMapPieces() {
        removeItem("11500");
        removeItem("11510");
        removeItem("11520");
        removeItem("11530");
    }

    public void openGraveQuest() {
        String[] graveKeys = {"grave1", "grave2", "grave3", "grave4", "grave5", "grave6", "grave7"};
        int which = getRandomCityIndex();
        for (String graveKey : graveKeys) {
            if (!journal.isDone(graveKey)) {
                journal.open(graveKey, "!" + niceNearEastCities[which]);
                break;
            }
        }
    }

    private int getRandomCityIndex() {
        return random.nextInt(6);
    }
}
