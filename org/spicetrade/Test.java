/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade;

import javax.swing.*;
import java.awt.*;

/**
 * Spice Trade
 * This is the main class that starts the whole application.
 *
 * @author Juha Holopainen
 * @version 1.0
 */

public class Test {

    public Test() {
        JFrame frame = new GameFrame();
        // I heard that macs have problems with this undecorated setting, will look into that
        frame.setUndecorated(true);
        frame.setLayout(null);
        frame.setPreferredSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width / 2, Toolkit.getDefaultToolkit().getScreenSize().height / 2));
        frame.setLocationRelativeTo(null);
        frame.setName("mainframe");
        frame.setResizable(true);
        frame.setTitle("Spice Trade");
        frame.setBackground(Color.lightGray);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new Test();
        // start at the intro page
        GameFrame.gf.gotoPlace("intro");
    }
}
