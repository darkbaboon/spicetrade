/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade;

import org.spicetrade.collections.Actions;
import org.spicetrade.collections.Armies;
import org.spicetrade.collections.Dialogs;
import org.spicetrade.collections.Routes;
import org.spicetrade.collections.Market;
import org.spicetrade.collections.Places;
import org.spicetrade.collections.Traders;
import org.spicetrade.graphics.Battle;
import org.spicetrade.graphics.Calendar;
import org.spicetrade.graphics.Dialog;
import org.spicetrade.graphics.Diary;
import org.spicetrade.graphics.Glass;
import org.spicetrade.graphics.Place;
import org.spicetrade.graphics.Shop;
import org.spicetrade.graphics.Status;
import org.spicetrade.graphics.WorldMap;
import org.spicetrade.tools.Deal;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.Pictures;
import org.spicetrade.tools.Tab;
import org.spicetrade.tools.Task;
import org.spicetrade.tools.Tools;

import bsh.Interpreter;
import com.thoughtworks.xstream.XStream;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.HashMap;

import javax.swing.JComponent;

public class Arabesque extends JComponent {
    public Actions actions;
    public Armies armies;
    public Audio jukebox;
    public Battle battle;
    public Calendar calendar;
    public Cursor cursor;
    public Diary diary;
    public Glass glass;
    public Interpreter interpreter;
    public WorldMap map;
    public Shop shop;
    public Status status;
    public HashMap<String, Task> tasks;
    public boolean idle;
    public boolean modal;
    public Dialog dialog;
    public Dialogs dialogs;
    public Drawable flyover;
    public Drawables hud;
    public Drawables objects;
    public Drawables panel;
    public Font font;
    public Font fontBold;
    public Graphics bg;
    public Image offscreen;
    public int converter;
    public Interpreter bsh;
    public Market market;
    public Picture picture;
    public Pictures pictures;
    public Place place;
    public Places places;
    public Player player;
    public Routes routes;
    public String bshimport;
    public String sentences;
    public String triggerAtEntry;
    public String typeface;
    public Task task;
    public Tools tools;
    public Traders traders;
    public WorldMap worldMap;
    public XStream xstream;

    public Arabesque() {
        bsh = new Interpreter();
        bshimport = "import org.spicetrade.*;\nimport org.spicetrade.tools.*;\n";
        tools = new Tools();
        pictures = new Pictures();
        converter = 0;
        flyover = null;
        sentences = "";
        triggerAtEntry = "";
        typeface = "Arial";
        initialize();
    }

    public void initialize() {
        try {
            actions = new Actions();
            armies = new Armies();
            battle = new Battle();
            cursor = new Cursor(Cursor.DEFAULT_CURSOR);
            diary = new Diary();
            glass = new Glass();
            dialog = new Dialog();
            dialogs = new Dialogs();
            font = new Font(typeface, Font.PLAIN, 12);
            fontBold = new Font(typeface, Font.BOLD, 12);
            worldMap = new WorldMap();
            market = new Market();
            place = new Place();
            places = new Places();
            player = new Player();
            picture = new Picture();
            routes = new Routes();
            shop = new Shop();
            status = new Status();
            traders = new Traders();
            xstream = new XStream();
            player.getJournal().open("field2");
            bsh.setOut(System.out);
            bsh.set("frame", this);
            bsh.set("result", false);
            // Define path for images
            final String ICONS_PATH = "/pics/navigation/icons/";
            final String LOGOS_PATH = ICONS_PATH + "logos/";
            // Load frequently used images
            pictures.put("notavailable", tools.loadImage(null, "/pics/notavailable.gif"));
            picture.getPicture("controls", ICONS_PATH + "controls.png", true);
            picture.getPicture("questionMark", ICONS_PATH + "question_mark.png", true);
            picture.getPicture("localMap", ICONS_PATH + "icon_map.png", true);
            picture.getPicture("globalMap", ICONS_PATH + "icon_globe.png", true);
            // Load status-related images
            picture.getPicture("statusFace", player.getStatusFace(), true);
            picture.getPicture("statusBar", ICONS_PATH + "statusbar.jpg", true);
            picture.getPicture("statusHashish", ICONS_PATH + "hashish_addiction.png", true);
            picture.getPicture("statusOpium", ICONS_PATH + "opium_addiction.png", true);
            picture.getPicture("statusPlague", ICONS_PATH + "health.png", true);
            // Load logos and icons
            picture.getPicture("playerIcon", ICONS_PATH + "abusmall.png", true);
            picture.getPicture("logoIntro", LOGOS_PATH + "logo_intro.png", true);
            picture.getPicture("logoEvil", LOGOS_PATH + "logo_bad.png", true);
            picture.getPicture("logoAmulet", LOGOS_PATH + "logo_fatima.png", true);
            picture.getPicture("logoDeath", LOGOS_PATH + "logo_dead.png", true);
            picture.getPicture("logoDeath2", LOGOS_PATH + "logo_invert.png", true);
            picture.getPicture("logoFatima", LOGOS_PATH + "logo_fatima.png", true);
            picture.getPicture("logoHashish", LOGOS_PATH + "logo_hashish.png", true);
            picture.getPicture("logoOpium", LOGOS_PATH + "logo_opium.png", true);
            picture.getPicture("logoPlague", LOGOS_PATH + "logo_red_crescent.png", true);
            // Load character and object images
            picture.getPicture("abu", "/pics/navigation/characters/abu.png", true);
            picture.getPicture("abuSoldier", "/pics/objects/abu_fighting_150.png", true);
            // Load miscellaneous images
            picture.getPicture("pixel", "/pics/pixel.gif", true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // This method is called for travelling and others routines
    public void createTask(String action, int duration, int delay) {
        createTask(action, duration, "", delay);
    }

    // This method is called for animation routines
    public void createTask(String action, int duration, String doAfter, int delay) {
        task = new Task(action, duration, doAfter, delay);
        task.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // the paint routine is pretty simple, we just loop through the drawable items
        // and draw them in the buffer. once we are done, the buffer is drawn to the screen

        try {
            GameFrame frame = GameFrame.gf;
            if (offscreen == null) {
                // initialize buffer
                g.setFont(font);
                offscreen = createImage(getWidth(), getHeight());
                bg = offscreen.getGraphics();
            }

            // clear buffer
            bg.setColor(Color.darkGray);
            bg.fillRect(0, 0, 2000, 2000);

            // draw base objects
            objects.draw(bg);

            // draw modal "dialog"
            if (frame.isModal())
                panel.draw(bg);

            // draw status etc..
            hud.draw(bg);

            // draw flyover
            if (flyover != null)
                flyover.draw(bg);

            // draw buffer to screen
            g.drawImage(offscreen, 0, 0, this);
        } catch (Exception ex) {
            // once in a while the drawable lists are out of sync and we don't
            // want to
            // show that error message to the player, since it does not matter,
            // everything
            // just gets drawn again, if there was a problem
            ex.printStackTrace();
        }
    }

    public void paintBattle(String who, int turn, int weapon, int enemyMight, String endTask) {
        battle.paintBattle(who, turn, weapon, enemyMight, endTask);
    }

    public void paintCalendar(String back) {
        calendar.paintCalendar(back, -1, 0, 0);
    }

    public void paintCalendar(String back, int day, int month, int year) {
        calendar.paintCalendar(back, day, month, year);
    }

    public void paintDialog(String text) {
        dialog.paintDialog(text);
    }

    public void paintDiary(int page) {
        diary.paintDiary(page);
    }

    public void paintGlass() {
        glass.paintGlass();
    }

    public void paintPlace(String placeName) {
        place.paintPlace(placeName);
    }

    public void paintShop(String who, Deal deal, String id, int traderPage, int playerPage) {
        shop.paintShop(who, deal, id, traderPage, playerPage);
    }

    public void paintStatus(Tab tab, String id, int page, Tab from, int more) {
        status.paintStatus(tab, id, page, from, more);
    }

    public void paintWorldMap(boolean showIcon) {
        worldMap.paintWorldMap(showIcon);
    }
}
