/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.collections.Dialogs;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.Spicebean;
import org.spicetrade.tools.UIType;
import org.spicetrade.Mainframe;
import org.spicetrade.Player;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

public class Dialog implements Paintable {
    private Drawables panel;
    private Dialogs dialogs;
    public String sentences;
    public Drawable flyover;
    public Place place;
    public Player player;
    public Mainframe mainframe;
    public Spicebean spicebean;
    public Picture picture;
    public Integer screenWidth;
    public WorldMap worldmap;

    public Dialog() {
        dialogs = new Dialogs();
    }

    public void paintDialog(String sentences) {
        screenWidth = 1024;

        try {
            ArrayList<Spicebean> actions = dialogs.getChoices(sentences);
            String path = "/pics/navigation/characters/" + dialogs.getTitle(sentences).toLowerCase().replaceAll(" ", "_") + ".png";
            Image image = picture.getPicture(path);

            // draw the panel and the label
            Drawable drawable = new Drawable(200, 56, screenWidth / 2 - 160, 150, UIType.LABEL, dialogs.getText(sentences));
            if (drawable.plainText.size() > 6)
                panel.add(new Drawable(20, 18, screenWidth - 70, 60 + (drawable.plainText.size()) * 18, UIType.PANEL, image, dialogs.getTitle(sentences)));
            else if (actions.size() > 3)
                panel.add(new Drawable(20, 18, screenWidth - 70, 100 + (actions.size() + 1) * 20, UIType.PANEL, image, dialogs.getTitle(sentences)));
            else
                panel.add(new Drawable(20, 18, screenWidth - 70, 155, UIType.PANEL, picture.getPicture(path), dialogs.getTitle(sentences)));
            panel.add(drawable);

            // draw all the lines of dialog choices
            int _y = 56;
            for (Spicebean spicebean : actions) {
                drawable = new Drawable(screenWidth / 2 + 70, _y, 340, 30, UIType.LABEL, spicebean.name, spicebean.action);
                panel.add(drawable);
                _y += 16 * drawable.plainText.size() - 1;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g) {
        panel.draw(g);
    }
}
