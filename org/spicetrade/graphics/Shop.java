/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.Player;
import org.spicetrade.collections.Traders;
import org.spicetrade.tools.Deal;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Inventory;
import org.spicetrade.tools.Item;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.UIType;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Arrays;

public class Shop implements Paintable {
    public Drawables panel = new Drawables();
    public Player player;
    public Picture picture;
    public Traders traders;
    public Font font;
    public Font fontBold;
    public String typeface;

    public void paintShop(String who, Deal deal, String id, int traderPage, int playerPage) {
        int x = 150;
        int y = 100;
        int width = 760;
        int height = 20;
        int page = 0;
        int per = 9;
        String name = "";

        Font statusFontBold = new Font(typeface, 1, 12);
        Font statusFont = new Font(typeface, 0, 12);
        Color silver = new Color(132, 132, 132);

        panel.add(new Drawable(x, y, width, height + 480, UIType.BPANEL, picture.getPicture("logoIntro"), "Trading with " + traders.getDisplay(who)));

        panel.add(new Drawable(x + 10, y + 10, 0, 0, UIType.IMAGE, picture.getPicture(traders.getString(who, "Picture"))));
        panel.add(new Drawable(x + 650, y + 10, 0, 0, UIType.IMAGE, picture.getPicture("abu")));

        panel.add(new Drawable(720, 541, 120, height, UIType.LABEL, "Silver dirhams:", statusFontBold, silver));
        panel.add(new Drawable(830, 541, 200, height, UIType.LABEL, " " + player.getMoney(), statusFont, Color.black));

        // first we need to know what items we can buy and sell
        Inventory traderItems = traders.getItemsToBuy(who);
        Inventory playerItems = traders.getItemsForSale(who);

        // for checking the availability of more of the items we just bought or sold
        boolean inStockToBuy = false;
        boolean inStockForSale = false;

        x = 260;
        y = 219;
        Image image;
        String header = "frame.gotoShop(\"" + who + "\", Deal." + deal + ", \"" + id + "\", ";

        // first we paint the items we can buy..
        if (traderItems.size() > per) {
            int pages = (traderItems.size() - 1) / per;
            if (traderPage > 0)
                panel.add(new Drawable(x - 100, y, 80, height, UIType.LABEL, "<< Previous", "Goto previous page",
                    header + (traderPage - 1) + ", " + playerPage + ");"));
            for (page = 0; page <= pages; page++) {
                String strPage = page + 1 + (page == pages ? "" : ",");
                Color labelColor = (page == traderPage) ? Color.red : null;
                Font labelFont = (page == traderPage) ? fontBold : null;
                panel.add(new Drawable(x + (page * 20), y, 18, height, UIType.LABEL, strPage, "Goto page " + (page + 1),
                    header + page + ", " + playerPage + ");", labelFont, labelColor));
            }
            if (traderPage < ((traderItems.size() - 1) / per))
                panel.add(new Drawable(x + (page * 20), y, 80, height, UIType.LABEL, "Next >>", "Goto next page",
                    header + (traderPage + 1) + ", " + playerPage + ");"));
        }
        x = 200;
        y = 250;
        page = 0;
        for (Item buyItem : traderItems) {
            page++;

            // Check if buy item is still available
            if (buyItem.getId().equals(id)) {
                inStockToBuy = true;
                name = buyItem.getName();
            }

            if (page > (traderPage * per) && (page <= (traderPage * per + per) && (page < traderItems.size() + 1))) {
                int purchasePrice = traders.getPurchasePrice(who, buyItem.getId());
                image = picture.getPicture(buyItem.getPicture());
                panel.add(new Drawable(x + (47 - (image.getWidth(null) / 2)), y + (50 - (image.getHeight(null) / 2)), 70, 70, UIType.IMAGE, image,
                    "Buy " + buyItem.getName() + "§" + buyItem.getDescription() + "§" + "Price: " + purchasePrice + " silver dr",
                    "frame.gotoShop(\"" + who + "\", Deal." + Deal.BUY + ", \"" + buyItem.getId() + "\", " + traderPage + ", " + playerPage + ");"));
                y += 100;

                if (y > 510) {
                    x += 94;
                    y = 250;
                }
            }
        }

        x = 610;
        y = 219;
        page = 0;
        // .. and then the items we can sell
        if (playerItems.size() > per) {
            int pages = (playerItems.size() - 1) / per;
            for (page = 0; page <= pages; page++) {
                String strPage = page + (page == playerItems.size() / per ? "" : ",");
                Color labelColor = (playerPage == page) ? Color.red : null;
                Font labelFont = (playerPage == page) ? fontBold : null;
                panel.add(new Drawable(x - (pages * 20 + 80) + (page * 20), y, 18, height, UIType.LABEL, strPage, "Goto page " + (page + 1),
                    header + traderPage + ", " + page + ");", labelFont, labelColor));
            }
            if (playerPage > 0)
                panel.add(new Drawable(x - (pages * 20 + 180), y, 80, height, UIType.LABEL, "<< Previous", "Goto previous page",
                    header + traderPage + ", " + (playerPage - 1) + ");"));
            if (playerPage < ((playerItems.size() - 1) / per))
                panel.add(new Drawable(x - (pages * 20 + 80) + (page * 20), y, 80, height, UIType.LABEL, "Next >>", "Goto next page",
                    header + traderPage + ", " + (playerPage + 1) + ");"));
        }
        x = 550;
        y = 250;
        page = 0;
        for (Item sellItem : playerItems) {
            page++;

            // Check if sell item is still available
            if (sellItem.getId().equals(id)) {
                inStockForSale = true;
                name = sellItem.getName();
            }

            if (page > (playerPage * per) && (page <= (playerPage * per + per) && page < playerItems.size() + 1)) {
                int sellingPrice = traders.getSellingPrice(who, sellItem.getId());
                image = picture.getPicture(sellItem.getPicture());
                panel.add(new Drawable(x + (47 - (image.getWidth(null) / 2)), y + (50 - (image.getHeight(null) / 2)), 70, 70, UIType.IMAGE, image,
                    "Sell " + sellItem.getName() + "§" + sellItem.getDescription() + "§" + "Price: " + sellingPrice + " silver dr" + "§" + "Pieces:" + " "+ player.amountItem(sellItem.getId()),
                    "frame.gotoShop(\"" + who + "\", Deal." + Deal.SELL + ", \"" + sellItem.getId() + "\", " + traderPage + ", " + playerPage + ");"));
                y += 100;

                if (y > 510) {
                    x += 94;
                    y = 250;
                }
            }
        }

        // Buy, sell, close labels
        String[] vehicules = {"10310", "10320", "10330", "10340", "10350", "10360"};
        String[] diets = {"10000", "10050", "10720"};
        String[] spices = {"10100", "10110", "10120", "10130", "10200", "10210", "10220", "10230"};

        boolean purchasable = (deal == Deal.BUY && inStockToBuy);
        int purchasePrice = (purchasable) ? traders.getPurchasePrice(who, id) : 0;
        boolean saleable = (deal == Deal.SELL && inStockForSale);
        int sellingPrice = (saleable) ? traders.getSellingPrice(who, id) : 0;
        String action = "";

        if (purchasable) {
            // food items
            if (Arrays.asList(diets).contains(id))
                action += "frame.player.removeItem(\"10000\");frame.player.removeItem(\"10050\");frame.player.removeItem(\"10720\");";
            // movement items
            if (Arrays.asList(vehicules).contains(id))
                action += "frame.player.buyItem(\"" + id.substring(0, 4) + "5\", 0);";
            action += "frame.player.buyItem(\"" + id + "\", " + purchasePrice;
            action += (player.hasSold(id)) ? ", \"" + who + "\");" : ");";
        } else if (saleable) {
            // food items
            if ("10050".equals(id) || "10720".equals(id))
                action = "frame.player.buyItem(\"10000\");";
            // movement items
            if (Arrays.asList(vehicules).contains(id))
                action += "frame.player.removeItem(\"" + id.substring(0, 4) + "5\");";
            action += "frame.player.sellItem(\"" + id + "\", " + sellingPrice;
            // agricultural produce and vehicules
            action += (Arrays.asList(spices).contains(id) || Arrays.asList(vehicules).contains(id)) ? ");" : ", \"" + who + "\");";
        }
        action += header + traderPage + ", " + playerPage + ");";

        String label = "";
        boolean unaffordable = (purchasable && player.getMoney() < purchasePrice);

        if (unaffordable) {
            label = "You cannot afford " + name;
            action = "result=false;";
        } else if (purchasable) label = "Buy " + name + " (Price: " + purchasePrice + ")";
        if (saleable) label = "Sell " + name + " (Price: " + sellingPrice + ")";

        Drawable drawable = new Drawable(520, 563, 300, 20, UIType.LABEL, label, action);
        drawable.x -= (drawable.labelWidth / 2);
        panel.add(drawable);
        panel.add(new Drawable(850, 570, 80, 20, UIType.LABEL, "Close", "frame.player.chooseFace();frame.gotoPlace(frame.player.getPlace());"));
    }


    public void paint(Graphics g) {

        panel.draw(g);
    }
}
