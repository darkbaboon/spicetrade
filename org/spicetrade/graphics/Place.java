/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.Mainframe;
import org.spicetrade.Player;
import org.spicetrade.collections.Places;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Inventory;
import org.spicetrade.tools.Item;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.Spicebean;
import org.spicetrade.tools.UIType;

import java.awt.Graphics;
import java.util.ArrayList;

public class Place implements Paintable {
    final Mainframe frame = Mainframe.me;
    private Drawables objects = new Drawables();
    private Places places = new Places();
    private Picture picture = new Picture();
    private Player player = new Player();
    private int screenWidth = 0;
    private int screenHeight = 0;
   
    public void paintPlace() {
        paintPlace(player.getPlace());
    }

    public void paintPlace(String to) {

        try {
            objects.add(new Drawable(0, 0, screenHeight, screenWidth, UIType.IMAGE, picture.getPicture(places.getBackground(to))));
            ArrayList<Spicebean> pictures = places.getPictures(to);
            for (Spicebean sb : pictures) {
                if (sb.isDoable())
                    objects.add(new Drawable(sb.x, sb.y, sb.width, sb.height, UIType.IMAGE, picture.getPicture(sb.picture), sb.name, sb.action));
                else
                    objects.add(new Drawable(sb.x, sb.y, 0, 0, UIType.IMAGE, picture.getPicture(sb.picture)));
            }

            int x = 25;
            int y = 25;
            int width;
            int height;
            int counter = 0;
            String path;

            // if there are any items that want to be shown on the screen, draw them
            Inventory items = player.items;
            for (Item item : items) {
                if (item.isActive()) {
                    if (item.getPictureBig() == null || item.getPictureBig().isEmpty()) {
                        path = item.getPicture();
                        width = 55;
                        height = 55;
                    } else {
                        path = item.getPictureBig();
                        width = 100;
                        height = 100;
                    }

                    objects.add(new Drawable(x, y, width, height, UIType.IMAGE, picture.getPicture(path), item.getName() + "§" + item.getDescription(), item.getAction()));
                    x += 75;
                    switch (counter) {
                        default:
                            break;
                        case 11:
                        case 23:
                        case 35:
                        case 47:
                            y += 90;
                            x = 25;
                            break;
                    }
                    counter++;
                }
            }

            x = 210;

            // random drawables that get thrown on the screen
            Drawables drawables = places.getDrawables(to);
            objects.addAll(drawables);

            if (frame.isShowOptions()) frame.paintOptions(objects);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g) {

        objects.draw(g);
    }
}

