/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.Player;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.UIType;

import java.awt.Image;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

public class Diary implements Paintable {
    private Drawables panel = new Drawables();
    private Picture picture = new Picture();
    private Player player = new Player();
    private String typeface = "Arial";

    public void paintDiary(int page) {

        try {
            int y;
            int x;
            panel.add(new Drawable(150, 100, 760, 500, UIType.PANEL, picture.getPicture("/pics/abu/face/dead2.png"), picture.getPicture(player.logo, true),
                    "Your diary", Color.black));

            Font statusFontBold = new Font(typeface, 1, 14);
            Font statusFont = new Font(typeface, 0, 14);
            Color color = Color.white;

            // ADD 10.04.2005 -- Want to see the book of life events in the order they happen in
            ArrayList<String> quests = player.getJournal().quests;
            String quest;

            if (page == 0) {
                Image image;

                x = 327;
                y = 360;
                switch (player.deathType) {
                    case 1:
                        image = picture.getPicture("/pics/abu/death/dead001.png");
                        x = 420;
                        y = 280;
                        break;
                    case 2:
                        image = picture.getPicture("/pics/abu/death/dead003a.png");
                        x = 528;
                        y = 177;
                        break;
                    case 3:
                        image = picture.getPicture("/pics/abu/death/dead004.png");
                        x = 420;
                        y = 280;
                        break;
                    case 4:
                        image = picture.getPicture("/pics/abu/death/dead005.png");
                        x = 400;
                        y = 280;
                        break;
                    default:
                        image = picture.getPicture("/pics/abu/death/dead002.png");
                        x = 400;
                        y = 280;
                        break;
                }

                panel.add(new Drawable(x, y, 0, 0, UIType.IMAGE, image));
                Drawable d1 = new Drawable((410 + 175), 495, 460, 20, UIType.LABEL, player.lastWords, statusFontBold, color);
                d1.x -= (d1.labelWidth / 2);
                panel.add(d1);
            } else {
                quest = quests.get(page - 1);
                int when = Integer.parseInt(player.getJournal().timestamp.get(quest));
                int year = (when / 360) + 17 - 500;
                String time = ", age of " + year;
                Image image = picture.getPicture(player.getJournal().getPicture(quest));
                String questText = player.getJournal().getString(quest, "Achievement");
                panel.add(new Drawable((507 + 75) - (image.getWidth(null) / 2), (350) - (image.getHeight(null) / 2), 0, 0, UIType.IMAGE, image));
                Drawable d1 = new Drawable((410 + 175), 515, 460, 20, UIType.LABEL, player.getJournal().getString(quest, "Display") + time, statusFontBold, color);
                d1.x -= (d1.labelWidth / 2);
                panel.add(d1);
                Drawable d2 = new Drawable((410 + 175), 540, 460, 380, UIType.LABEL, questText, statusFont, color);
                d2.x -= (d2.labelWidth / 2);
                panel.add(d2);
            }

            if (page > 0)
                panel.add(new Drawable(170, 570, 120, 20, UIType.LABEL, "<< PREVIOUS", "frame.gotoDiary(" + (page - 1) + ");", statusFontBold, color));
            // FIX 10.5.2005 Clicking END GAME caused music to be turned back on
            panel.add(new Drawable(540, 570, 120, 20, UIType.LABEL, "END GAME",
                    "boolean p = frame.jukebox.musicOn;frame.jukebox.musicOn=false;frame.player = new Player();frame.jukebox.musicOn=p;frame.jukebox.loopMusic(\"/music/16_world_horizon.ogg\");frame.gotoPlace(\"menu\");", statusFontBold, color));
            if (page < player.getJournal().questsClosed.size())
                panel.add(new Drawable(840, 570, 120, 20, UIType.LABEL, "NEXT >>", "frame.gotoDiary(" + (page + 1) + ");", statusFontBold, color));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g) {
        panel.draw(g);
    }
}
