/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.UIType;

import java.awt.Color;
import java.awt.Font;

public class Calendar {
    public Drawables panel;
    public Font font;
    public Font fontBold;
    public Picture picture;
    public String typeface;

    public void paintCalendar(String back) {
        paintCalendar(back, -1, 0, 0);
    }

    public void paintCalendar(String back, int number, int action, int total) {
        // this is the Hijra <-> Gregorian calendar calculator :-)
        // it was pretty fun to make this, even though we were pressed for time
        int x = 165;
        int y = 280;
        int width = 220;
        int height = 290;
        int newTotal = total;
        Font statusFontBold = new Font(typeface, 1, 11);
        Font statusFont = new Font(typeface, 0, 10);
        if (number > -1)
            newTotal = Integer.parseInt(String.valueOf(total) + number);
        if (action == 1)
            newTotal = -newTotal;
        panel.add(new Drawable(x, y, width, height, UIType.PANEL, null, null, "Hijra converter", null, null, null, Color.white, 1));
        panel.add(new Drawable(x + width - 20, y + 10, 20, 20, UIType.LABEL, "X", "frame.converter=0;" + back, "Close converter", statusFontBold, Color.black));
        panel.add(new Drawable(x + 40, y + 18, 140, 20, UIType.LABEL, "CALENDAR CONVERTER", statusFontBold, Color.black));
        panel.add(new Drawable(x + 43, y + 45, 120, 108, UIType.IMAGE, picture.getPicture("/pics/objects/converter.png")));

        panel.add(new Drawable(x + width - 70, y + 190, 40, 20, UIType.LABEL, "CLEAR", "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"")
                + "\", -1, 0, 0);", "Clear converter", statusFont, Color.black));

        panel.add(new Drawable(x + 26, y + 183, 50, 20, UIType.LABEL, "+/-", "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"") + "\", -1, 1, "
                + newTotal + ");", "Change year to positive/negative", statusFontBold, Color.black));

        int _x = x - 25 + 85;
        int _y = y + 138;
        Color color = Color.black;
        int counter = 0;
        for (int i = 1; i < 11; i++) {
            if (i == number)
                color = Color.red;
            else
                color = Color.black;

            if (i < 10) {
                _x += 20;
                panel.add(new Drawable(_x, _y, 13, 15, UIType.LABEL, String.valueOf(i), "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"") + "\", "
                        + i + ", 0, " + newTotal + ");", String.valueOf(i), statusFontBold, color));
            } else {
                _x += 40;
                panel.add(new Drawable(_x, _y, 13, 15, UIType.LABEL, "0", "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"") + "\", 0, 0, "
                        + newTotal + ");", "0", statusFontBold, color));
            }

            if (counter > 1) {
                _x = x - 25 + 85;
                _y += 15;
                counter = 0;
            }
            counter++;
        }
        int ah = newTotal - Math.round((newTotal / 33.0F)) + 622;
        int gre = (newTotal - 622) + Math.round((newTotal - 622) / 32.0F);
        String greStr = "AC";
        String ahStr = "AH";
        String greStr2 = "AC";
        String ahStr2 = "AH";
        if (gre < 0)
            ahStr = "BH";
        if (ah < 0)
            greStr = "BC";
        if (newTotal < 0)
            ahStr2 = "BH";
        if (newTotal < 0)
            greStr2 = "BC";
        panel.add(new Drawable(x + 40, y + 220, 300, 100, UIType.LABEL, "Hijra years to gregorian years", statusFont, Color.black));
        panel.add(new Drawable(x + 70, y + 235, 300, 100, UIType.LABEL, newTotal + " " + ahStr2 + " = " + ah + " " + greStr, statusFont, Color.black));
        panel.add(new Drawable(x + 40, y + 255, 300, 100, UIType.LABEL, "Gregorian years to Hijra years", statusFont, Color.black));
        panel.add(new Drawable(x + 70, y + 270, 300, 100, UIType.LABEL, newTotal + " " + greStr2 + " = " + gre + " " + ahStr, statusFont, Color.black));
    }

}
