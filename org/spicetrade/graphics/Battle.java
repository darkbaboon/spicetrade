/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.Player;
import org.spicetrade.collections.Armies;
import org.spicetrade.collections.Market;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Item;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.UIType;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

public class Battle implements Paintable {
    private Drawables panel = new Drawables();
    private Armies armies = new Armies();
    private Market market = new Market();
    private Picture picture = new Picture();
    private Player player = new Player();
    private static Random random = new Random();
    private Integer jinx = random.nextInt(10);
    private Integer x = 150;
    private Integer y = 100;
    private Integer width = 760;
    private Integer height = 500;
    private boolean end = false;
    private String typeface;

    public void paintBattle(String who, int turn, int weapon, int enemyMight, String endTask) {

        try {
            String endDo = "frame.gotoPlace(frame.player.getPlace());";

            String abuLog = "Abu's actions:§";
            String enemyLog = "Actions for " + armies.getName(who) + ":§";

            panel.add(new Drawable(x, y, width, height, UIType.BPANEL, picture.getPicture("logoIntro"), "Fighting against " + armies.getName(who) + " starting turn: " + turn));
            panel.add(new Drawable(x + 10, y + 10, 0, 0, UIType.IMAGE, picture.getPicture(armies.getString(who, "Picture"))));
            panel.add(new Drawable(x + width - 110, y + 10, 0, 0, UIType.IMAGE, picture.getPicture("abu")));
            panel.add(new Drawable(x + 50, y + 150, 0, 0, UIType.IMAGE, picture.getPicture(armies.getString(who, "Soldier"))));
            panel.add(new Drawable(x + width - 150, y + 150, 0, 0, UIType.IMAGE, picture.getPicture("abuSoldier")));

            int abu = (int) player.getHealth();
            int enemy = enemyMight;
            int fight = random.nextInt(100);

            Item item;

            if (player.hasItem("13000")) {
                item = market.getItem("13000");
                enemy -= item.getForce() - (jinx);
                abuLog += "§- Your sword swings into action";
            } else if (player.hasItem("13010")) {
                item = market.getItem("13010");
                enemy -= item.getForce() - (jinx);
                abuLog += "§- You struck with the dagger";
            }

            switch (weapon) {
                case 1: // dagger
                    item = market.getItem("13010");
                    enemy -= item.getForce() - (jinx);
                    abuLog += "§- You struck with the dagger";
                    break;
                case 2: // sword
                    item = market.getItem("13000");
                    enemy -= item.getForce() - (jinx);
                    abuLog += "§- Your sword swings into action";
                    break;
                case 3: // djinnies
                    enemy -= 50 - (jinx);
                    abuLog += "§- The djinnies appear and attack the enemy";
                    break;
            }

            if (fight >= 99 && fight < 100) {
                enemy -= 20 - (jinx);
                abuLog += "§- You get a critical strike!";
            } else if (fight >=1 && fight < 2) {
                abu -= 20 - (jinx);
                enemyLog += "§- The enemy forces get strike a mighty blow!";

            }

            if (player.addictedHashish && fight >= 0 && fight < 25) {
                abu -= 1;
                abuLog += "§- You are hampered by your addiction to hashish";
            } else if (player.addictedOpium && fight >= 0 && fight < 50) {
                abu -= 3;
                abuLog += "§- Your opium addiction causes you to lose your will to fight";
            } else if (player.sickPlague && fight >= 0 && fight < 75) {
                abu -= 6;
                abuLog += "§- The plague you are carrying hinders your battle greatly";
            }

            if (player.moral < -75 && fight >= 0 && fight < 75) {
                abu += 10 - (jinx);
                enemyLog += "§- Your vile deeds on the battlefield cause your foes to tremble";
            } else if (player.moral > 75 && fight >= 0 && fight < 75) {
                abu += 10 - (jinx);
                enemyLog += "§- Your good reputation precedes you, some of the enemies soldiers abandon their foul cause";
            }

            int enemyHit = (armies.getStrengh(who) + random.nextInt(10));
            int abuHit = (player.getStrengh() + random.nextInt(10));
            abu -= enemyHit;
            enemy -= abuHit;

            abuLog += hitLog("You", "the enemy", abuHit);
            enemyLog += hitLog("The enemy", "you", enemyHit);

            if (enemy < 1) {
                enemyLog += "§- The enemy forces have been annihilated";
                abuLog += "§- You won!";
                end = true;
                player.add(who + "won");
            } else if (abu < 1) {
                abuLog += "§- You were defeated!";
                if (enemy > 0)
                    endDo = "frame.gotoDeath(\"Abu was defeated in a battle that occurred the surroundings of " + player.getNiceCity()
                            + " against the forces of " + armies.getName(who) + ".\", 1);";
                else
                    endDo = "frame.gotoDeath(\"Abu died in a glorious battle on the surroundings of " + player.getNiceCity()
                            + " while destroying the remnants of the forces of " + armies.getName(who) + ".\", 2);";
                end = true;
            }

            player.health = abu;

            Font statusFont = new Font(typeface, 0, 10);

            panel.add(new Drawable(x + 200, y + 150, 150, 200, UIType.LABEL, enemyLog, statusFont, Color.black));
            panel.add(new Drawable(x + width - 350, y + 150, 150, 200, UIType.LABEL, abuLog, statusFont, Color.black));

            // FIX 26.4.2005 Included endTask in the next turn script
            String nextTurn = "frame.player.nextDay();frame.gotoBattle(\"" + who + "\", " + (turn + 1) + ", !weapon, " + enemy + ", \"" + endTask.replaceAll("\"", "\\\\\"") + "\");";

            if (!end) {
                player.inBattle = false;
                //panel.add(new Drawable(170, 570, 100, 20, UIType.LABEL, "Invoke djinnis", nextTurn.replaceAll("!weapon", "3")));
                //if (player.hasItem("13000"))
                //    panel.add(new Drawable(290, 570, 85, 20, UIType.LABEL, "Use sword", nextTurn.replaceAll("!weapon", "2")));
                //else if (player.hasItem("13010"))
                //    panel.add(new Drawable(290, 570, 85, 20, UIType.LABEL, "Use dagger", nextTurn.replaceAll("!weapon", "1")));
                panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "Next round", nextTurn.replaceAll("!weapon", "0")));
            } else {
                player.inBattle = false;

                if (!endTask.isEmpty())
                    panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "End battle", "frame.jukebox.loopMusic(frame.jukebox.lastMusic);frame.player.chooseFace();" + endTask));
                else if (!endDo.isEmpty())
                    panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "End battle", "frame.jukebox.loopMusic(frame.jukebox.lastMusic);frame.player.chooseFace();" + endDo));
                else
                    panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "End battle", "frame.jukebox.loopMusic(frame.jukebox.lastMusic);frame.player.chooseFace();frame.gotoPlace(frame.player.getPlace());"));

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String hitLog(String entity, String foe, int hit) {
        String conjugaison = (entity == "The enemy") ? "s " : " ";
        if (hit < 5)
            return "§- " + entity + " slaps" + conjugaison + foe;
        else if (hit < 15)
            return "§- " + entity + " feebly hit" + conjugaison + foe;
        else if (hit < 25)
            return "§- " + entity + " hit" + conjugaison + foe;
        else if (hit < 50)
            return "§- " + entity + " strike" + conjugaison + foe + " hard";
        else
            return "§- " + entity + " score" + conjugaison + "a devastating hit";
    }

    public void paint(Graphics g) {
        for (Drawable element : panel) {
            element.draw(g);
        }
    }

}
