/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.Mainframe;
import org.spicetrade.Player;
import org.spicetrade.collections.Routes;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Inventory;
import org.spicetrade.tools.Item;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.Transport;
import org.spicetrade.tools.Travel;
import org.spicetrade.tools.UIType;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

public class WorldMap implements Paintable {
    final Mainframe frame = Mainframe.me;
    private Drawables objects = new Drawables();
    private Routes routes = new Routes();
    private Picture picture = new Picture();
    private Player player = new Player();

    public void paintWorldMap(boolean showIcon) {

           try {
            // first we get the active map entries, which depend on the
            // transport type and also if we have a travel
            // permit to europe
            objects.add(new Drawable(0, 0, frame.getHeight(), frame.getWidth(), UIType.IMAGE, picture.getPicture(routes.getBackground())));
            ArrayList<Travel> entries = routes.getEntries(player.getCity(), player.getTransport(), (player.hasItem("10630") && player.getTransport() != Transport.CARAVAN));
            for (Travel entry : entries) {
                objects.add(new Drawable(entry.x - 10, entry.y - 10, entry.width, entry.height, UIType.IMAGE, picture.getPicture("pixel", true), null,
                    "Go to " + entry.nice, entry.getDescription(player.getTransport()), entry.getAction(player.getTransport()), null, null, 0));
            }

            // right side of the window
            //int y = getHeight() - 465;
            //int x = 980;
            // left side of the window
            int y = frame.getHeight() - 565;
            int x = 50;
            Inventory items = player.items;
            for (Item item : items) {
                if (item.isActive()) {
                    Image image = picture.getPicture(item.getPicture());
                    if (item.getName().toLowerCase().equals(player.getTransport().getName()))
                        image = picture.getPicture(item.getPicture().replaceAll(".png", "_active.png"));
                    objects.add(new Drawable(x - (image.getWidth(null) / 2), y + (65 - (image.getHeight(null) / 2)), 75, 75, UIType.IMAGE, image,
                        item.getName() + "§" + item.getDescription(), item.getAction()));
                    y += 65;
                }
            }

            if (showIcon)
                objects.add(new Drawable(player.xHomeCountry - 14, player.yHomeCountry - 14, 0, 0, UIType.IMAGE, picture.getPicture("playerIcon", true)));

            if (frame.isShowOptions()) frame.paintOptions(objects);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g) {

        objects.draw(g);
    }
}
