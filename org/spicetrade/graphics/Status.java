/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.Player;
import org.spicetrade.collections.Market;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Inventory;
import org.spicetrade.tools.Item;
import org.spicetrade.tools.LogItem;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.Tab;
import org.spicetrade.tools.UIType;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Iterator;

public class Status implements Paintable {
    private Drawables panel = new Drawables();
    public Font font;
    public Font fontBold;
    public String typeface;
    public Market market = new Market();
    private Picture picture = new Picture();
    private Player player = new Player();

    public void paintStatus(Tab tab, String id, int page, Tab from, int more) {

        try {
            // this is a terrible mess, because there is so much of it and this takes
            // the longest to calculate and draw, so if any optimization is needed,
            // this is one place to start.
            // STATUS: the main status screen shows an overview of the player status
            // INVENTORY: all the items that the player has (well, not _all_ of them, just the ones we want to show
            // DIARY: open and done quests
            // HELP: help screen, index or individual help
            // LOG: the log of "all" the things that have happened, dialogs, etc
            // CULTURE: statistics for the culture points of the player
            // DEATH: the death screen, when the game ends

            int x;
            int y;
            int _y;
            int _x;
            int width;
            int height;
            panel.add(new Drawable(150, 100, 760, 500, UIType.PANEL, picture.getPicture("statusFace", true), picture.getPicture(player.logo, true), tab.getName()));

            Font statusFontBold = new Font(typeface, 1, 12);
            Font statusFont = new Font(typeface, 0, 10);

            if (tab != Tab.HELP) {
                x = 410;
                y = 240;
                _x = 594;
                _y = 255;
                width = 300;
                height = 20;
                panel.add(new Drawable(x, y - 30, width, height, UIType.LABEL, player.get("Name"), statusFontBold, Color.black));
                panel.add(new Drawable(x, y, width, height, UIType.LABEL, "date and place of born", statusFont, Color.black));
                panel.add(new Drawable(_x, y, width, height, UIType.LABEL, player.get("dateAndPlace"), statusFont, Color.black));

                Drawable wd = new Drawable(_x, _y, width, height, UIType.LABEL, player.get("wives"), statusFont, Color.black);
                int wives = wd.plainText.size();
                String label = (wives > 1) ? "wives" : "wife";
                panel.add(new Drawable(x, _y, width, height, UIType.LABEL, label, statusFont, Color.black));
                if (!player.get("wives").isEmpty()) panel.add(wd);
                else panel.add(new Drawable(_x, _y, width, height, UIType.LABEL, "none", statusFont, Color.black));

                Drawable cd = new Drawable(_x, _y + 2 + (wives * 9), width, height, UIType.LABEL, player.get("children"), statusFont, Color.black);
                int children = cd.plainText.size();
                label = (children > 1) ? "children" : "child";
                panel.add(new Drawable(x, _y + 2 + (wives * 9), width, height, UIType.LABEL, label, statusFont, Color.black));
                if (!player.get("children").isEmpty()) panel.add(cd);
                else panel.add(new Drawable(_x, 257 + (wives * 9), width, height, UIType.LABEL, "none", statusFont, Color.black));

                _x = 177;
                _y = 280;
                width = 200;
                panel.add(new Drawable(_x, _y, width, height, UIType.LABEL, "MONEY", statusFontBold, Color.black));
                panel.add(new Drawable(_x, _y + 20, width, height, UIType.LABEL, player.getMoney() + " silver dirhams", statusFont, Color.black));
            }

            switch (tab) {
                default:
                    break;
                case STATUS:
                    int increment = 20;
                    y = 370;
                    _x = 177;
                    width = 300;
                    height = 20;
                    boolean found = false;
                    boolean foundsuper = false;
                    // Property
                    panel.add(new Drawable(_x, y - 30, width, height, UIType.LABEL, "PROPERTY", statusFontBold, Color.black));
                    panel.add(new Drawable(_x, y, width, height, UIType.LABEL, "- lands", statusFont, Color.black));
                    x = 253;
                    width = 20;
                    height = 25;
                    String[] lands = {"12050", "14000", "14005", "14010", "14015", "14020", "14025", "14030", "14035", "14040", "14045"};
                    found = createImageDrawables(panel, lands, x, y - 5, width, height, increment, tab, page, found);
                    found = false;

                    String[] spices = {"10110", "10100", "10120", "10130", "10210", "10200", "10220", "10230"};
                    found = createImageDrawables(panel, spices, x, y + 22, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 30, "- spices", statusFont, Color.black);
                    found = false;

                    String[] art1 = {"10800", "10801", "10802", "10803", "10804", "10805"};
                    String[] art2 = {"10810", "10811", "10812"};
                    String[] art3 = {"10820"};
                    String[] art4 = {"10830", "10831"};
                    found = createImageDrawables(panel, art1, x, y + 53, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, art2, x, y + 53, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, art3, x, y + 53, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, art4, x, y + 53, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 60, "- works of art", statusFont, Color.black);
                    found = false;

                    String[] treasures1 = {"11600", "11601", "11602", "11603", "11604"};
                    String[] treasures2 = {"11610", "11611"};
                    String[] treasures3 = {"11620"};
                    String[] treasures4 = {"11630"};
                    String[] treasures5 = {"11640"};
                    String[] treasures6 = {"11650", "11651", "11652", "11660", "11661"};
                    String[] treasures7 = {"11670", "11671", "11672", "11673"};
                    String[] treasures8 = {"11680", "11680"};
                    found = createImageDrawables(panel, treasures1, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures2, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures3, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures4, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures5, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures6, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures7, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures8, x, y + 82, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 90, "- treasures", statusFont, Color.black);
                    found = false;

                    String[] luxury1 = {"10720"};
                    String[] luxury2 = {"10750", "10751"};
                    String[] luxury3 = {"10730", "10731", "10732", "10733"};
                    String[] luxury4 = {"10760"};
                    String[] luxury5 = {"10770"};
                    String[] luxury6 = {"10710"};
                    String[] luxury7 = {"10740", "10741", "10742", "10743", "10744", "10745"};
                    found = createImageDrawables(panel, luxury1, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury2, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury3, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury4, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury5, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury6, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury7, x, y + 112, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 120, "- luxury", statusFont, Color.black);
                    found = false;

                    String[] buildings1 = {"12000", "12001", "12002", "12003", "12004"};
                    String[] buildings2 = {"12060", "12061", "12062", "12063", "12064", "12065", "12066", "12067", "12068", "12069", "12070"};
                    String[] buildings3 = {"12080", "12081", "12082", "12083", "12084", "12085", "12086", "12087", "12088", "12089", "12090"};
                    String[] buildings4 = {"12030"};
                    String[] buildings5 = {"12010"};
                    String[] buildings6 = {"12020"};
                    String[] buildings7 = {"12040"};
                    found = createImageDrawables(panel, buildings1, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings2, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings3, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings4, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings5, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings6, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings7, x, y + 143, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 150, "- buildings", statusFont, Color.black);
                    found = false;

                    // Licenses
                    String[] licenses1 = {"10600", "10610"};
                    String[] licenses2 = {"10620"};
                    String[] licenses3 = {"10630"};
                    String[] licenses4 = {"10640"};
                    String[] licenses5 = {"10650", "10651", "10652", "10653", "10654", "10655", "10656", "10657", "10658", "10659", "10660"};
                    increment = 40;
                    x = 450;
                    _x = 460;
                    width = 40;
                    height = 40;
                    found = createImageDrawables(panel, licenses1, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses2, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses3, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses4, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses5, x, y - 10, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y - 30, "LICENSES", statusFontBold, Color.black);
                    found = false;

                    // Maps
                    String[] maps = {"11500", "11510", "11520", "11530"};
                    found = createImageDrawables(panel, maps, x, y + 60, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 40, "MAPS", statusFontBold, Color.black);
                    found = false;

                    // Workers
                    String[] workers = {"16000", "16010", "16020", "16030"};
                    found = createImageDrawables(panel, workers, x, y + 130, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 110, "WORKERS", statusFontBold, Color.black);
                    found = false;

                    // Supernatural
                    String[] amulets = {"11000", "11001", "11002", "11003", "11004", "11005", "11010", "11011", "11012", "11013", "11014", "11015"};
                    x = 723;
                    _x = 652;
                    width = 10;
                    height = 20;
                    increment = 13;
                    found = createImageDrawables(panel, amulets, x, y - 6, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y, "- amulets", statusFont, Color.black);
                    found = false;

                    String[] drinks = {"11030", "11031", "11040"};
                    found = createImageDrawables(panel, drinks, x, y + 24, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 30, "- drinks", statusFont, Color.black);
                    found = false;

                    String[] buckles = {"11050", "11051", "11052", "11053", "11054", "11055", "11056"};
                    width = 13;
                    height = 22;
                    increment = 20;
                    found = createImageDrawables(panel, buckles, x, y + 54, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 60, "- buckles", statusFont, Color.black);        
                    found = false;

                    String[] rosary = {"11020", "11025"};
                    width = 60;
                    height = 50;
                    increment = 50;
                    found = createImageDrawables(panel, rosary, x, y + 87, width, height, increment, tab, page, found);
                    String label = (player.hasItem("11020")) ? "- rosary" : "- talisman";
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 90, label, statusFont, Color.black);
                    found = false;

                    String[] flying = {"10335", "10345", "10355"};
                    found = createImageDrawables(panel, flying, x, y + 105, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 120, "- flying", statusFont, Color.black);
                    found = false;

                    String[] others = {"11700", "13000"};
                    increment = 40;
                    found = createImageDrawables(panel, others, x, y + 150, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 150, "- others", statusFont, Color.black);
                    found = false;
                    createLabelDrawable(foundsuper, foundsuper, _x, y - 30, "SUPERNATURAL", statusFontBold, Color.black);
                    break;
                case Tab.INVENTORY:
                    _x = 150;
                    int per = 16;
                    Item item;
                    Inventory inventory = player.getInventory();
                    int i = page * per;
                    int j = Math.min((page + 1) * per, inventory.size() - 1);
                    if (inventory.size() > per) {
                        if (page > 0) panel.add(new Drawable(190, 340, 80, 20, UIType.LABEL, "<< Previous", "Goto previous page", "frame.gotoStatus(Tab.INVENTORY, \"\", " + (page - 1) + ", Tab." + tab + ", 0);"));
                        int k = 0;
                        _y = 312;
                        for (int l = (inventory.size() - 1) / per; k <= l; k++) {
                            String strPage = (k + 1) + (k == l ? "" : ",");
                            Color labelColor = (page == k) ? Color.red : null;
                            Font labelFont = (page == k) ? fontBold : null;
                            panel.add(new Drawable(290 + (k * 20), 340, 18, 20, UIType.LABEL, strPage, "Goto page " + (k + 1), "frame.gotoStatus(Tab.INVENTORY, \"\", " + k + ", Tab." + tab + ", 0);", labelFont, labelColor));
                        }
                        if (page < ((inventory.size() - 1) / per))
                            panel.add(new Drawable(290 + (k * 20), 340, 80, 20, UIType.LABEL, "Next >>", "Goto next page", "frame.gotoStatus(Tab.INVENTORY, \"\", " + (page + 1) + ", Tab." + tab + ", 0);"));

                    }

                    _y = 360;
                    if ((inventory.size() >= per && page != (inventory.size() / per)) || (page == 0 && inventory.size() != per))
                        j++;

                    for (int l = i; l < j; l++) {
                        if (_y > 500) {
                            _x += 94;
                            _y = 360;
                        }
                        item = inventory.get(l);
                        Image image = picture.getPicture(item.getPicture());
                        i++;
                        panel.add(new Drawable(_x + (47 - (image.getWidth(null) / 2)), _y + (50 - (image.getHeight(null) / 2)), 70, 70, UIType.IMAGE, picture.getPicture(item.getPicture()),
                            item.getName() + "§" + item.getDescription(), "frame.gotoStatus(Tab.HELP,\"" + item.getHelpId() + "\", " + page + ", " + tab + ", 0);"));
                        _y += 100;
                    }
                    break;
                case DIARY:
                    increment = 20;
                    x = 177;
                    y = 370;
                    width = 300;
                    height = 20;
                    panel.add(new Drawable(x, y - 30, width, height, UIType.LABEL, "OPEN QUESTS", statusFontBold, Color.black));

                    Iterator<String> objects = player.getJournal().questsOpen.keySet().iterator();
                    String quest;
                    String additional;
                    int counter = 0;
                    while (objects.hasNext()) {
                        quest = objects.next();
                        // FIX 29.4.2005 Do not display null values
                        if (quest == null) continue;
                        additional = player.getJournal().get(quest);
                        if (!additional.isEmpty() && additional.charAt(0) == '!')
                            if (!additional.contains("§")) additional = "";
                            else additional = additional.substring(additional.indexOf("§"));
                        // FIX 29.4.2005 Do not display null values
                        if (player.getJournal().getString(quest, "Display") != null && player.getJournal().getString(quest, "Text") != null)
                            panel.add(new Drawable(x, y, 180, height, UIType.LABEL, player.getJournal().getString(quest, "Display"), player.getJournal().getString(quest, "Text") + additional, "result=false;", statusFont, Color.black));
                        y += increment;
                        counter++;
                        if (counter == 9) {
                            y = 370;
                            x += 180;
                        }
                    }

                    increment = 20;
                    y = 370;
                    counter = 0;
                    x = 450;
                    panel.add(new Drawable(450, y - 30, width, height, UIType.LABEL, "QUESTS DONE", statusFontBold, Color.black));

                    if (player.getJournal().questsClosed.size() > (18 + more)) {
                        panel.add(new Drawable(750, y - 30, width, height, UIType.LABEL, "MORE >>", "frame.gotoStatus(Tab." + tab + ", \"\", " + page + ", Tab." + from + ", " + (more + 18) + ");", statusFontBold, Color.black));
                    }
                    if (more > 0) {
                        panel.add(new Drawable(650, y - 30, width, height, UIType.LABEL, "<< LESS", "frame.gotoStatus(Tab." + tab + ", \"\", " + page + ", Tab." + from + ", " + (more - 18) + ");", statusFontBold, Color.black));
                    }

                    Iterator<String> quests = player.getJournal().questsClosed.keySet().iterator();
                    quest = "";
                    int looper = 0;
                    while (quests.hasNext()) {

                        quest = quests.next();
                        if (looper >= more) {
                            panel.add(new Drawable(x, y, 180, height, UIType.LABEL, player.getJournal().getString(quest, "Display"), player.getJournal().getString(quest, "Text") + player.getJournal().get(quest), "result=false;", statusFont, Color.black));
                            y += increment;

                            counter++;
                            if (counter == 9) {
                                y = 370;
                                x += 180;
                            } else if (counter == 18)
                                break;
                        }
                        looper++;
                    }
                    break;
                case HELP:
                    if (id.isEmpty()) {
                        String[] alphabet = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
                        Inventory helps = market.getHelp(alphabet[more]);

                        for (i = 0; i < alphabet.length; i++) {
                            Color hcolor = (i == more) ? Color.red : Color.black;
                            panel.add(new Drawable(190 + (i * 27), 310, 15, 15, UIType.LABEL, alphabet[i], "Show help for letter " + alphabet[i], "frame.gotoStatus(Tab." + tab + ", \"\", " + page + ", Tab." + from + ", " + i + ");", statusFontBold, hcolor));
                        }

                        _y = 340;
                        _x = 177;
                        for (Item help : helps) {
                            panel.add(new Drawable(_x, _y, 230, 20, UIType.LABEL, help.name, "frame.gotoStatus(Tab." + tab + ", \"" + help.id + "\", " + page + ", Tab." + tab + ", 0);", statusFontBold, Color.black));
                            _y += 30;

                            if (_y > 530) {
                                _y = 340;
                                _x += 190;
                            }
                            if (_x > 750)
                                break;
                        }
                    } else {
                        item = market.getItem(id);
                        Image image = picture.getPicture(item.getPictureBig());
                        panel.add(new Drawable((200 + 75) - (image.getWidth(null) / 2), (300 + 119) - (image.getHeight(null) / 2), 0, 0, UIType.IMAGE, image));
                        panel.add(new Drawable(410, 210, 460, 20, UIType.LABEL, item.getName(), fontBold, Color.black));
                        Drawable drawable = new Drawable(410, 240, 460, 380, UIType.LABEL, item.getDescription());
                        panel.add(new Drawable(750, 529, 120, 20, UIType.LABEL, "Calendar converter",
                         "frame.converter=frame.panel.size();frame.paintCalendar(\"frame.gotoStatus(Tab." + tab + ", \\\"" + id + "\\\", " + page + ", " + "Tab." + from + ", " + more + ");\");"));
                        x = 570;
                        y = 529;
                        width = 40;
                        height = 20;
                        int pagination = 15;
                        int endIndex = more * pagination;

                        if (more > 0) drawable.plainText.subList(0, endIndex).clear(); 
                        boolean beyond = drawable.plainText.size() > pagination;
                        if (beyond) drawable.plainText.subList(pagination + 1, drawable.plainText.size()).clear();

                        int xLess = (beyond && more < 3) ? x - 20 : x;
                        int xMore = (beyond && more > 0) ? x + 20 : x;
                        Drawable drawableLess = new Drawable(xLess, y, width, height, UIType.LABEL, "Less", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (more - 1) + ");");
                        Drawable drawableMore = new Drawable(xMore, y, width, height, UIType.LABEL, "More", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (more + 1) + ");");

                        switch (more) {
                            case 0:
                                if(beyond) panel.add(drawableMore);
                                break;
                            case 1:
                            case 2:
                                panel.add(drawableLess);
                                if(beyond) panel.add(drawableMore);
                                break;
                            case 3:
                                panel.add(drawableLess);
                                break;
                            default:
                                break;
                        }
                        panel.add(drawable);
                        panel.add(new Drawable(780, 570, 65, height, UIType.LABEL, "<< Back", "frame.gotoStatus(" + "Tab." + from + ", \"\", " + page + ", Tab.STATUS, 0);"));
                    }
                    break;
                case LOG:
                    x = 550;
                    y = 545;
                    _y = 370;
                    _x = 177;
                    width = 40;
                    height = 20;
                    counter = player.log.size() - 1;

                    panel.add(new Drawable(_x, _y - 30, 100, 200, UIType.LABEL, "LOG", statusFontBold, Color.black));

                    Drawable drawable;
                    while (_y < 500 && counter > -1) {
                        if (counter <= more) {
                            LogItem logItem = player.log.get(counter);
                            drawable = new Drawable(_x, _y, 700, 200, UIType.LABEL, logItem.toString(), statusFont, Color.black);
                            panel.add(drawable);
                            _y += drawable.plainText.size() * 11 + 3;
                        }
                        counter--;
                    }
                    if (more > 0) {
                        int left = more - 2;
                        left = (left > 0) ? left : 0;
                        panel.add(new Drawable(x + 40, y, width, height, UIType.LABEL, "More", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (left) + ");"));
                    }
                    if (more < player.log.size() - 1)
                        panel.add(new Drawable(x, y, width, height, UIType.LABEL, "Less", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (more + 2) + ");"));
                    break;
                case CULTURE:
                    int middleX = 510;
                    int startY = 315;
                    int labelWidth = 100;
                    int lineHeight = 10;
                    Color pink = new Color(220, 0, 255);

                    panel.add(new Drawable(middleX, startY, middleX, startY + 260, UIType.LINE, Color.blue, 1));

                    startY += 5;
                    Font cfont = null;
                    Color leftColor = Color.red;
                    Color rightColor = Color.blue;
                    for (String city : Player.cities) {
                        int culture = player.getCulture(city);
                        int cultureWidth = culture * 2;
                        cfont = (city.equals(player.getCity())) ? new Font(typeface, Font.BOLD, 10) : statusFont;
                        Drawable cityLabel = new Drawable(middleX, startY - 3, labelWidth, lineHeight, UIType.LABEL, city.toUpperCase(), cfont, Color.black);

                        if (culture < 0) {
                            leftColor = (culture > -16) ? pink : Color.red;
                            panel.add(new Drawable(middleX + cultureWidth, startY, -(cultureWidth), lineHeight, UIType.RECTANGLE, "", null, leftColor));
                            cityLabel.x = middleX + 5;
                        } else {
                            rightColor = (culture < 16) ? pink : Color.blue;
                            panel.add(new Drawable(middleX, startY, cultureWidth, lineHeight, UIType.RECTANGLE, "", null, rightColor));
                            cityLabel.x = middleX - 5 - cityLabel.labelWidth;
                        }
                        panel.add(cityLabel);
                        startY += 15;
                    }
                    // meaning of red
                    panel.add(new Drawable(760, 475, 10, lineHeight, UIType.RECTANGLE, "", null, Color.red));
                    panel.add(new Drawable(773, 472, 140, lineHeight, UIType.LABEL, "European influence strong", cfont, Color.black));
                    // ..blue
                    panel.add(new Drawable(760, 500, 10, lineHeight, UIType.RECTANGLE, "", null, Color.blue));
                    panel.add(new Drawable(773, 497, 140, lineHeight, UIType.LABEL, "My influence strong", cfont, Color.black));
                    // ..and the meaning of pink :-)
                    panel.add(new Drawable(760, 525, 10, lineHeight, UIType.RECTANGLE, "", null, pink));
                    panel.add(new Drawable(773, 522, 140, lineHeight, UIType.LABEL, "Close to neutral", cfont, Color.black));

                    break;
                case DEATH:
                    x = 420;
                    y = 350;
                    width = 270;
                    height = 200;

                    String action = "frame.setSowStatus(false);frame.player.logo=\"logoDeath2\";frame.gotoPlace(\"gameover" + (player.moral > 0 ? "paradise" : "hell") + "\");";

                    switch (player.deathType) {
                        case 1:
                            panel.add(new Drawable(x, y, width, height, UIType.IMAGE, picture.getPicture("/pics/abu/death/dead001.png"), "Game over", action));
                            break;
                        case 2:
                            panel.add(new Drawable(528, 177, width, 400, UIType.IMAGE, picture.getPicture("/pics/abu/death/dead003.png"), "Game over", action));
                            break;
                        case 3:
                            panel.add(new Drawable(x, y, width, height, UIType.IMAGE, picture.getPicture("/pics/abu/death/dead004.png"), "Game over", action));
                            break;
                        case 4:
                            panel.add(new Drawable(400, 280, width, height, UIType.IMAGE, picture.getPicture("/pics/abu/death/dead005.png"), "Game over", action));
                            break;
                        default:
                            panel.add(new Drawable(400, y, width, height, UIType.IMAGE, picture.getPicture("/pics/abu/death/dead002.png"), "Game over", action));
                            break;
                    }
                    break;
            }
            // Navigation bar
            String[] actions = {
                "frame.gotoStatus(Tab.STATUS, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.INVENTORY, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.DIARY, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.HELP, \"\", " + page + ", Tab.STATUS, 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.LOG, \"\", " + page + ", Tab." + tab + ", (frame.player.log.size()-1));",
                "frame.player.chooseFace();frame.gotoStatus(Tab.CULTURE, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.setShowOptions(false);frame.player.logo=\"logoDeath2\";frame.gotoPlace(\"gameover" + (player.moral > 0 ? "paradise" : "hell") + "\");",
                "frame.player.chooseFace();frame.gotoPlace(frame.player.getPlace());"
            };
            x = 170;
            y = 570;
            _x = 850;
            width = 80;
            height = 20;
            for (Tab tabToDraw : Tab.values()) {
                int i = tabToDraw.ordinal();
                String label = tabToDraw.getName();
                Color color = (tab == tabToDraw) ? Color.red : Color.black;
                if (tabToDraw != Tab.DEATH) {
                    panel.add(new Drawable(x, y, width, height, UIType.LABEL, label, actions[i], fontBold, color));
                    x += 100;
                } else {
                    if (player.inGameOver) panel.add(new Drawable(_x, y, width - 20, height, UIType.LABEL, label, actions[i], fontBold, color));
                    else panel.add(new Drawable(_x, y, width - 20, height, UIType.LABEL, "Close", actions[7], fontBold, color));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean createImageDrawables(Drawables panel, String[] objects, int x, int y, int width, int height, int increment, Tab tab, int page, boolean found) {
        for (String object : objects) {
            if (player.hasItem(object, 1, false)) {
                Item item = market.getItem(object);
                Image image = picture.getPicture(item.getPictureStatus());
                panel.add(createImageDrawable(x, y, width, height, tab, page, image, item));
                x += increment;
                found = true;
                break;
            }
        }
        return found;
    }

    private Drawable createImageDrawable(int x, int y, int width, int height, Tab tab, int page, Image image, Item item) {
        String action = "frame.gotoStatus(Tab.HELP,\"" + item.getHelpId() + "\", " + page + ", Tab." + tab + ", 0);";
        String description = item.getName() + "§" + item.getDescription();
        return new Drawable(x, y, width, height, UIType.IMAGE, image, description, action);
    }

    private boolean createLabelDrawable(boolean found, boolean foundsuper, int x, int y, String label, Font statusFont, Color color) {
        if (found) {
            panel.add(new Drawable(x, y, 300, 20, UIType.LABEL, label, statusFont, color));
            foundsuper = true;
        }
        return foundsuper;
    }

    public void paint(Graphics g) {

        panel.draw(g);
    }
}
