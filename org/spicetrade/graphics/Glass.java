/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.graphics;

import org.spicetrade.GameFrame;
import org.spicetrade.Player;
import org.spicetrade.collections.Places;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Picture;
import org.spicetrade.tools.UIType;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Glass implements Paintable {
    private Drawables hud = new Drawables();
    private Places places = new Places();
    private Picture picture = new Picture();
    private Player player = new Player();
    private String typeface = "Arial";

    public void paintGlass() {

        try {
            GameFrame frame = GameFrame.gf;
            // status bar and the city/world icons

            if (frame.isShowOptions()) {
                picture.getPicture("statusFace", player.getStatusFace(), true, true);
                hud.add(new Drawable(20, frame.getHeight() - 169, 155, 155, UIType.STATUS, picture.getPicture("statusFace", true), picture.getPicture("statusBar", true),
                        "Open status", "Open status", "frame.gotoStatus(Tab.STATUS, \"\", 0, Tab.STATUS, 0);", null, null, 0));

                hud.add(new Drawable(386, frame.getHeight() - 38, 100, 19, UIType.LABEL, player.getAge() + " years",
                        "Abu's age is " + player.getDay() + " days " + player.getMonth() + " months " + player.getAge() + " years",
                        "result=false;", new Font(typeface, 1, 12), Color.magenta));

                
                // FIX 19.04.2006 by Markus Piotrowski, show health
                hud.add(new Drawable(460, frame.getHeight() - 38, 100, 19, UIType.LABEL, "  ", "Abu's health is " + (int)player.getHealth(),
                        "result=false;", new Font(typeface, 1, 12), Color.magenta));
                
                // FIX 19.04.2006 by Markus Piotrowski, show money
                hud.add(new Drawable(550, frame.getHeight() - 38, 100, 19, UIType.LABEL, "  ", "Abu's money is " + player.getMoney() +" silver dirhams ",
                        "result=false;", new Font(typeface, 1, 12), Color.magenta));
                
                // FIX 04.05.2006 by Markus Piotrowski, show culture
                hud.add(new Drawable(680, frame.getHeight() - 38, 90, 19, UIType.LABEL, "  ", "Abu's culture is " + player.getCulture(),
                        "result=false;", new Font(typeface, 1, 12), Color.magenta));
                
                // FIX 01.05.2006 by Markus Piotrowski, show moral
                hud.add(new Drawable(780, frame.getHeight() - 38, 70, 19, UIType.LABEL, "MORAL", "Abu's moral is " + player.getMoral(),
                        "result=false;", new Font(typeface, 1, 12), Color.black));
        
                if (!"highwaymen".equals(player.getPlace()) && !"pirates".equals(player.getPlace())) {
                    hud.add(new Drawable(195, frame.getHeight() - 100, 65, 60, UIType.IMAGE, picture.getPicture("localMap", true),
                            "Go to map of " + places.getTitle(player.getCity()), "frame.gotoPlace(\"" + player.getCity() + "\");"));
                    if (frame.isShowMapGlobe())
                        hud.add(new Drawable(265, frame.getHeight() - 100, 60, 60, UIType.IMAGE, picture.getPicture("globalMap", true), null,
                                "Go to map of the world",  "frame.gotoWorldMap();"));
                }
                int y = 698;
                if (player.sickGeneral || player.sickPoisoned || player.sickPlague) {
                    if (player.sickGeneral)
                        y += 10;
                    
                    if (player.sickPoisoned || player.sickGeneral) {
                        hud.add(new Drawable(480, y, 20, 20, UIType.IMAGE, picture.getPicture("statusPlague", true),
                            "You are poisoned",
                            "frame.gotoStatus(Tab.HELP, \"5430\", 0, Tab.STATUS, 0);"));
                        if (!player.sickPlague)
                            y -= 40;
                    }
                
                    if (player.sickPlague) {
                        hud.add(new Drawable(480, y, 20, 20, UIType.IMAGE, picture.getPicture("statusPlague", true),
                            "You have the plague",
                            "frame.gotoStatus(Tab.HELP, \"5430\", 0, Tab.STATUS, 0);"));
                        y -= 40;
                    }
                }
                if (player.isAddictedHashish()) {
                    hud.add(new Drawable(473, y, 27, 27, UIType.IMAGE, picture.getPicture("statusHashish", true),
                        "You are addicted to hashish",
                        "frame.gotoStatus(Tab.HELP, \"3360\", 0, Tab.STATUS, 0);"));
                    y -= 43;
                }
                if (player.isAddictedOpium())
                    hud.add(new Drawable(479, y, 27, 27, UIType.IMAGE, picture.getPicture("statusOpium", true),
                        "You are addicted to opium",
                        "frame.gotoStatus(Tab.HELP, \"3360\", 0, Tab.STATUS, 0);"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g) {
        hud.draw(g);
    }
}
