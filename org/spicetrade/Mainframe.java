/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade;

import bsh.Interpreter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.*;

import org.spicetrade.collections.Actions;
import org.spicetrade.collections.Armies;
import org.spicetrade.collections.Dialogs;
import org.spicetrade.collections.Routes;
import org.spicetrade.collections.Market;
import org.spicetrade.collections.Places;
import org.spicetrade.collections.Traders;
import org.spicetrade.tools.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Spice Trade The frame for the application, essentially this runs the whole
 * show.
 * <p>
 * There are groups of functions, the "goto"-functions serve as the entry points
 * for various elements in the game, ie. gotoPlace renders a place for the
 * player, gotoCity starts moving the player towards a designated city etc.
 * <p>
 * The "goto"-functions use the "paint"-functions, which contain the UI/layout
 * logic.
 * <p>
 * Then there are the "trigger"-functions that launch a new thread that starts
 * to do something in the game world, ie. grow a plant, show the movement of the
 * player on the map while going to a new city and so on.
 * <p>
 * Then of course there are a bunch of helper functions, like getImage, loadGame,
 * saveGame.
 *
 * @author Juha Holopainen
 * @version 1.0
 */

public class Mainframe extends JFrame {

    private static final List<String> nonLoggables = Arrays.asList("Minimize", "Load", "Save", "Close", "Log");
    public static Mainframe me;
    public Actions actions;
    public Armies armies;
    public Audio jukebox;
    public boolean idle;
    public boolean modal;
    public boolean showMapGlobe;
    public boolean showOptions;
    public Dialog dialog;
    public Dialogs dialogs;
    public Drawable flyover;
    public Drawables hud;
    public Drawables objects;
    public Drawables panel;
    public Font font;
    public Font fontBold;
    public Graphics bg;
    public HashMap<String, Image> pictures;
    public Image offscreen;
    public int converter;
    public int jinx;
    public Interpreter bsh;
    public Market market;
    public Places places;
    public Player player;
    public Random random;
    public Routes routes;
    public String bshimport;
    public String sentences;
    public String triggerAtEntry;
    public String typeface;
    public Task task;
    public Tools tools;
    public Traders traders;
    public XStream xstream;

    public Mainframe() {
        bsh = new Interpreter();
        bshimport = "import org.spicetrade.*;\nimport org.spicetrade.tools.*;\n";
        xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY);
        xstream.addPermission(NullPermission.NULL);
        xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
        tools = new Tools();
        objects = new Drawables();
        panel = new Drawables();
        hud = new Drawables();
        random = new Random();
        jinx = random.nextInt(100);
        pictures = new HashMap<>();
        setModal(false);
        converter = 0;
        setShowOptions(false);
        setShowMapGlobe(false);
        setIdle(false);
        flyover = null;
        triggerAtEntry = "";
        typeface = "Arial";
        setDialog("");
        initialize();
    }

    public void initialize() {
        try {
            // bootstrap the game world
            me = this;
            dialogs = new Dialogs();
            places = new Places();
            market = new Market();
            jukebox = new Audio();
            jukebox.musicOn = false;
            actions = new Actions();
            routes = new Routes();
            player = new Player();
            armies = new Armies();
            traders = new Traders();
            player.getJournal().open("field2");
            bsh.setOut(System.out);
            bsh.set("frame", this);
            bsh.set("result", false);
            setBounds(0, 0, 1024, 768);
            // Define path for images
            final String ICONS_PATH = "/pics/navigation/icons/";
            final String LOGOS_PATH = ICONS_PATH + "logos/";
            // Load frequently used images
            pictures.put("notavailable", tools.loadImage(this, "/pics/notavailable.gif"));
            setIconImage(getImage(ICONS_PATH + "icon.gif"));
            getImage("controls", ICONS_PATH + "controls.png", true);
            getImage("questionMark", ICONS_PATH + "question_mark.png", true);
            getImage("localMap", ICONS_PATH + "icon_map.png", true);
            getImage("globalMap", ICONS_PATH + "icon_globe.png", true);
            // Load status-related images
            getImage("statusFace", player.getStatusFace(), true);
            getImage("statusBar", ICONS_PATH + "statusbar.jpg", true);
            getImage("statusHashish", ICONS_PATH + "hashish_addiction.png", true);
            getImage("statusOpium", ICONS_PATH + "opium_addiction.png", true);
            getImage("statusPlague", ICONS_PATH + "health.png", true);
            // Load logos and icons
            getImage("playerIcon", ICONS_PATH + "abusmall.png", true);
            getImage("logoIntro", LOGOS_PATH + "logo_intro.png", true);
            getImage("logoEvil", LOGOS_PATH + "logo_bad.png", true);
            getImage("logoAmulet", LOGOS_PATH + "logo_fatima.png", true);
            getImage("logoDeath", LOGOS_PATH + "logo_dead.png", true);
            getImage("logoDeath2", LOGOS_PATH + "logo_invert.png", true);
            getImage("logoFatima", LOGOS_PATH + "logo_fatima.png", true);
            getImage("logoHashish", LOGOS_PATH + "logo_hashish.png", true);
            getImage("logoOpium", LOGOS_PATH + "logo_opium.png", true);
            getImage("logoPlague", LOGOS_PATH + "logo_red_crescent.png", true);
            // Load character and object images
            getImage("abu", "/pics/navigation/characters/abu.png", true);
            getImage("abuSoldier", "/pics/objects/abu_fighting_150.png", true);
            // Load miscellaneous images
            getImage("pixel", "/pics/pixel.gif", true);
            // Load settings
            loadSettings();
            font = new Font(typeface, 0, 14);
            fontBold = new Font(typeface, 1, 14);
            // CHANGE 26.4.2005 support for full screen mode
            if (player.isFullScreen() && GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getFullScreenWindow() == null)
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(null);
            if (!player.isFullScreen() && GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getFullScreenWindow() != null)
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(null);
            addMouseListener(new MouseAdapter() {

                public void mouseClicked(MouseEvent e) {
                    this_mouseClicked(e);
                }

            });
            addMouseMotionListener(new MouseMotionAdapter() {

                public void mouseMoved(MouseEvent e) {
                    this_mouseMoved(e);
                }

            });
            addWindowListener(new WindowAdapter() {

                public void windowClosed(WindowEvent e) {
                    this_windowClosed(e);
                }

            });
            jukebox.musicOn = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g) {
        // the paint routine is pretty simple, we just loop through the drawable items
        // and draw them in the buffer. once we are done, the buffer is drawn to the screen

        try {
            if (offscreen == null) {
                // initialize buffer
                g.setFont(font);
                offscreen = createImage(getWidth(), getHeight());
                bg = offscreen.getGraphics();
            }

            // clear buffer
            bg.setColor(Color.darkGray);
            bg.fillRect(0, 0, 2000, 2000);

            // draw base objects
            objects.draw(bg);

            // draw modal "dialog"
            if (isModal())
                panel.draw(bg);

            // draw status etc..
            hud.draw(bg);

            // draw flyover
            if (flyover != null)
                flyover.draw(bg);

            // draw buffer to screen
            g.drawImage(offscreen, 0, 0, this);
        } catch (Exception ex) {
            // once in a while the drawable lists are out of sync and we don't
            // want to
            // show that error message to the player, since it does not matter,
            // everything
            // just gets drawn again, if there was a problem
            ex.printStackTrace();
        }
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void trigger(String action) {
        trigger(action, getWidth() - 300, "");
    }

    public void trigger(String action, String doAfter) {
        trigger(action, getWidth() - 300, doAfter);
    }

    public void trigger(String action, int x, String doAfter) {
        trigger(action, x, 20, 20, 750, 20, doAfter);
    }

    public void trigger(String action, int x, int y, int duration, int delay, String doAfter) {
        trigger(action, x, y, duration, delay, 0, doAfter);
    }

    public void trigger(String action, int x, int y, int duration, int delay, int after) {
        trigger(action, x, y, duration, delay, after, "");
    }

    public void trigger(String action, int x, int y, int duration, int delay, int after, String doAfter) {
        trigger(action, x, y, duration, delay, after, doAfter, false);
    }

    public void trigger(String action, int x, int y, int duration, int delay, int after, String doAfter, boolean fade) {
        // this ia a monstrosity that should be cleaned up, but too much script already depends on it..
        setModal(true);
        String act = "frame.actions.process(\"" + action + "\", !counter, " + x + ", " + y + ", !donep, " + after + ", " + fade + ");";
        createTask(act, duration, doAfter, delay);
    }

    public void gotoWorldMap() {
        // entering the world map
        jukebox.loopMusic("/music/16_world_horizon.ogg");
        player.setPlace("worldmap");
        player.setNiceCity("Map of the world");
        setModal(false);
        flyover = null;
        panel.clear();
        objects.clear();
        hud.clear();
        paintWorldMap(true);
        paintGlass();
        doLayout();
        repaint();

        // again, these kinds of "game rules" should be refactored out of the game engine core
        player.getJournal().done("permit1");
        player.getJournal().open("permit5");

        // if we have agricultural produce, don't let the player get out of Baghdad
        String[] spices = {"10100", "10110", "10120", "10130", "10200", "10210", "10220", "10230"};
        if (player.hasAnyItems(spices)&&!player.hasItem("10640"))
            gotoDialog("1680");
    }

    public void gotoPlace(String to) {
        if (!triggerAtEntry.isEmpty()) {
            try {
                // if there was something to run once entering a place, do it
                // now
                String action = triggerAtEntry;
                triggerAtEntry = "";
                player.setPlace(to);
                bsh.eval(bshimport + action);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else
            gotoPlace(to, false);
    }

    public void gotoPlace(String to, boolean modal) {
        // entering a place
        if (!to.equals(player.getPlace()) && !"menu".equals(to) && !"settings".equals(to) && !"intro".equals(to) && !"intro2".equals(to) && !"credits".equals(to)) {
            player.setLastPlace(player.getPlace());
            jinx = random.nextInt(100);
        }

        // CHANGE 26.4.2005 support for full screen mode
        if (player.fullScreen && GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getFullScreenWindow() == null)
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(this);
        if (!player.fullScreen && GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getFullScreenWindow() != null)
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(null);

        setModal(modal);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        if ("worldmap".equals(to)) {
            player.setPlace(to);
            gotoWorldMap();
        } else {
            player.setNiceCity(places.getTitle(to));
            String old = player.getPlace();
            player.setPlace(to);
            flyover = null;
            panel.clear();
            objects.clear();
            hud.clear();
            paintPlace(to);
            paintGlass();
            doLayout();
            repaint();

            // the "events" in a place are triggered "before" entering the
            // place, but after
            // drawing. so it's sort of like pre-enter place event, but
            // post-draw.. if we would
            // have a true event mechanism, that is
            player.setPlace(old);
            ArrayList<Spicebean> events = places.getEvents(to);
            if (events != null && !events.isEmpty()) {
                for (Spicebean sb : events) {
                    // I love BeanShell ;-)
                    sb.ingrain();
                }
            }
            player.setPlace(to);
        }
    }

    public void gotoCity(String to, String action, int xcity, int ycity, int duration) {
        // travelling to a new city
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        if (to.equals(player.getCity())) {
            player.setNiceCity(to);
            gotoPlace(to);
        } else {
            objects.clear();
            paintWorldMap(false);
            player.to(xcity, ycity);
            createTask(action, duration, 200);
        }
    }

    public String getDialog() {
        return sentences;
    }

    public void setDialog(String dialog) {
        this.sentences = dialog;
    }

    public void gotoDialog(String to) {
        // entering a dialog
        player.addLog("!" + to, "frame.gotoDialog(\"" + to + "\");", player.getDay() + player.getMonth() * 30 + player.getYear() * 360);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setDialog(to);
        setModal(true);
        flyover = null;
        panel.clear();
        objects.clear();
        if ("worldmap".equals(player.getPlace()))
            paintWorldMap(true);
        else
            paintPlace(player.getPlace());
        paintDialog(getDialog());
        doLayout();
        repaint();
        jinx = random.nextInt(100);
    }

    public void gotoStatus(Tab tab, String id, int page, Tab from, int helpMore) {
        // entering the status dialog
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setModal(true);
        flyover = null;
        panel.clear();
        objects.clear();

        if (player.hasAllMapPieces()) {
            player.chooseFace("14");
            player.openGraveQuest();
            player.removeMapPieces();
        }

        if ("worldmap".equals(player.getPlace()))
            paintWorldMap(true);
        else
            paintPlace(player.getPlace());
        paintStatus(tab, id, page, from, helpMore);
        doLayout();
        repaint();
    }

    public void gotoBattle(String who) {
        gotoBattle(who, 1, 0, armies.getHealth(who), "");
    }

    public void gotoBattle(String who, int round, int weapon, int enemyMight, String endTask) {
        // entering the battle dialog
        jukebox.loopMusic("/music/10_its_time.ogg");
        jinx = random.nextInt(100);
        setModal(true);
        player.setInBattle(true);
        panel.clear();
        player.chooseFace("16");
        paintBattle(who, round, weapon, enemyMight, endTask);
        doLayout();
        repaint();
    }

    public void gotoDiary() {
        gotoDiary(0);
    }

    public void gotoDiary(int page) {
        // this is the end of the game "book of life"
        setModal(true);
        panel.clear();
        objects.clear();
        paintPlace(player.getPlace());
        paintDiary(page);
        doLayout();
        repaint();
    }

    public void gotoShop(String who) {
        gotoShop(who, Deal.LOOK, "", 0, 0);
    }

    public void gotoShop(String who, Deal deal, String id, int traderPage, int playerPage) {
        // entering a shop dialog
        jinx = random.nextInt(100);
        setModal(true);
        panel.clear();
        player.chooseFace("02");
        paintShop(who, deal, id, traderPage, playerPage);
        doLayout();
        repaint();
    }

    public void gotoDeath(String famousLastWords, int how) {
        // game over, you died
        // how == 0: regular death, of age etc
        // how == 1: died gloriously in battle
        // how == 2: died shamefully in battle
        // how == 3: died of disease or poisoning
        player.inGameOver = true;
        setModal(true);
        player.deathType = how;
        player.lastWords = famousLastWords;
        panel.clear();
        objects.clear();
        if ("worldmap".equals(player.getPlace()))
            paintWorldMap(true);
        else
            paintPlace(player.getPlace());
        player.logo = "logoDeath";
        paintStatus(Tab.DEATH, "", 0, Tab.STATUS, 0);
        doLayout();
        repaint();
    }

    public void paintShop(String who, Deal deal, String id, int traderPage, int playerPage) {
        int x = 150;
        int y = 100;
        int width = 760;
        int height = 20;
        int page = 0;
        int per = 9;
        String name = "";

        Font statusFontBold = new Font(typeface, 1, 12);
        Font statusFont = new Font(typeface, 0, 12);
        Color silver = new Color(132, 132, 132);

        panel.add(new Drawable(x, y, width, height + 480, UIType.BPANEL, getImage("logoIntro"), "Trading with " + traders.getDisplay(who)));

        panel.add(new Drawable(x + 10, y + 10, 0, 0, UIType.IMAGE, getImage(traders.getString(who, "Picture"))));
        panel.add(new Drawable(x + 650, y + 10, 0, 0, UIType.IMAGE, getImage("abu")));

        panel.add(new Drawable(720, 541, 120, height, UIType.LABEL, "Silver dirhams:", statusFontBold, silver));
        panel.add(new Drawable(830, 541, 200, height, UIType.LABEL, " " + player.getMoney(), statusFont, Color.black));

        // first we need to know what items we can buy and sell
        Inventory traderItems = traders.getItemsToBuy(who);
        Inventory playerItems = traders.getItemsForSale(who);

        // for checking the availability of more of the items we just bought or sold
        boolean inStockToBuy = false;
        boolean inStockForSale = false;

        x = 260;
        y = 219;
        Image image;
        String header = "frame.gotoShop(\"" + who + "\", Deal." + deal + ", \"" + id + "\", ";

        // first we paint the items we can buy..
        if (traderItems.size() > per) {
            int pages = (traderItems.size() - 1) / per;
            if (traderPage > 0)
                panel.add(new Drawable(x - 100, y, 80, height, UIType.LABEL, "<< Previous", "Goto previous page",
                    header + (traderPage - 1) + ", " + playerPage + ");"));
            for (page = 0; page <= pages; page++) {
                String strPage = page + 1 + (page == pages ? "" : ",");
                Color labelColor = (page == traderPage) ? Color.red : null;
                Font labelFont = (page == traderPage) ? fontBold : null;
                panel.add(new Drawable(x + (page * 20), y, 18, height, UIType.LABEL, strPage, "Goto page " + (page + 1),
                    header + page + ", " + playerPage + ");", labelFont, labelColor));
            }
            if (traderPage < ((traderItems.size() - 1) / per))
                panel.add(new Drawable(x + (page * 20), y, 80, height, UIType.LABEL, "Next >>", "Goto next page",
                    header + (traderPage + 1) + ", " + playerPage + ");"));
        }
        x = 200;
        y = 250;
        page = 0;
        for (Item buyItem : traderItems) {
            page++;

            // Check if buy item is still available
            if (buyItem.getId().equals(id)) {
                inStockToBuy = true;
                name = buyItem.getName();
            }

            if (page > (traderPage * per) && (page <= (traderPage * per + per) && (page < traderItems.size() + 1))) {
                int purchasePrice = traders.getPurchasePrice(who, buyItem.getId());
                image = getImage(buyItem.getPicture());
                panel.add(new Drawable(x + (47 - (image.getWidth(this) / 2)), y + (50 - (image.getHeight(this) / 2)), 70, 70, UIType.IMAGE, image,
                    "Buy " + buyItem.getName() + "§" + buyItem.getDescription() + "§" + "Price: " + purchasePrice + " silver dr",
                    "frame.gotoShop(\"" + who + "\", Deal." + Deal.BUY + ", \"" + buyItem.getId() + "\", " + traderPage + ", " + playerPage + ");"));
                y += 100;

                if (y > 510) {
                    x += 94;
                    y = 250;
                }
            }
        }

        x = 610;
        y = 219;
        page = 0;
        // .. and then the items we can sell
        if (playerItems.size() > per) {
            int pages = (playerItems.size() - 1) / per;
            for (page = 0; page <= pages; page++) {
                String strPage = page + (page == playerItems.size() / per ? "" : ",");
                Color labelColor = (playerPage == page) ? Color.red : null;
                Font labelFont = (playerPage == page) ? fontBold : null;
                panel.add(new Drawable(x - (pages * 20 + 80) + (page * 20), y, 18, height, UIType.LABEL, strPage, "Goto page " + (page + 1),
                    header + traderPage + ", " + page + ");", labelFont, labelColor));
            }
            if (playerPage > 0)
                panel.add(new Drawable(x - (pages * 20 + 180), y, 80, height, UIType.LABEL, "<< Previous", "Goto previous page",
                    header + traderPage + ", " + (playerPage - 1) + ");"));
            if (playerPage < ((playerItems.size() - 1) / per))
                panel.add(new Drawable(x - (pages * 20 + 80) + (page * 20), y, 80, height, UIType.LABEL, "Next >>", "Goto next page",
                    header + traderPage + ", " + (playerPage + 1) + ");"));
        }
        x = 550;
        y = 250;
        page = 0;
        for (Item sellItem : playerItems) {
            page++;

            // Check if sell item is still available
            if (sellItem.getId().equals(id)) {
                inStockForSale = true;
                name = sellItem.getName();
            }

            if (page > (playerPage * per) && (page <= (playerPage * per + per) && page < playerItems.size() + 1)) {
                int sellingPrice = traders.getSellingPrice(who, sellItem.getId());
                image = getImage(sellItem.getPicture());
                panel.add(new Drawable(x + (47 - (image.getWidth(this) / 2)), y + (50 - (image.getHeight(this) / 2)), 70, 70, UIType.IMAGE, image,
                    "Sell " + sellItem.getName() + "§" + sellItem.getDescription() + "§" + "Price: " + sellingPrice + " silver dr" + "§" + "Pieces:" + " "+ player.amountItem(sellItem.getId()),
                    "frame.gotoShop(\"" + who + "\", Deal." + Deal.SELL + ", \"" + sellItem.getId() + "\", " + traderPage + ", " + playerPage + ");"));
                y += 100;

                if (y > 510) {
                    x += 94;
                    y = 250;
                }
            }
        }

        // Buy, sell, close labels
        String[] vehicules = {"10310", "10320", "10330", "10340", "10350", "10360"};
        String[] diets = {"10000", "10050", "10720"};
        String[] spices = {"10100", "10110", "10120", "10130", "10200", "10210", "10220", "10230"};

        boolean purchasable = (deal == Deal.BUY && inStockToBuy);
        int purchasePrice = (purchasable) ? traders.getPurchasePrice(who, id) : 0;
        boolean saleable = (deal == Deal.SELL && inStockForSale);
        int sellingPrice = (saleable) ? traders.getSellingPrice(who, id) : 0;
        String action = "";

        if (purchasable) {
            // food items
            if (Arrays.asList(diets).contains(id))
                action += "frame.player.removeItem(\"10000\");frame.player.removeItem(\"10050\");frame.player.removeItem(\"10720\");";
            // movement items
            if (Arrays.asList(vehicules).contains(id))
                action += "frame.player.buyItem(\"" + id.substring(0, 4) + "5\", 0);";
            action += "frame.player.buyItem(\"" + id + "\", " + purchasePrice;
            action += (player.hasSold(id)) ? ", \"" + who + "\");" : ");";
        } else if (saleable) {
            // food items
            if ("10050".equals(id) || "10720".equals(id))
                action = "frame.player.buyItem(\"10000\");";
            // movement items
            if (Arrays.asList(vehicules).contains(id))
                action += "frame.player.removeItem(\"" + id.substring(0, 4) + "5\");";
            action += "frame.player.sellItem(\"" + id + "\", " + sellingPrice;
            // agricultural produce and vehicules
            action += (Arrays.asList(spices).contains(id) || Arrays.asList(vehicules).contains(id)) ? ");" : ", \"" + who + "\");";
        }
        action += header + traderPage + ", " + playerPage + ");";

        String label = "";
        boolean unaffordable = (purchasable && player.getMoney() < purchasePrice);

        if (unaffordable) {
            label = "You cannot afford " + name;
            action = "result=false;";
        } else if (purchasable) label = "Buy " + name + " (Price: " + purchasePrice + ")";
        if (saleable) label = "Sell " + name + " (Price: " + sellingPrice + ")";

        Drawable drawable = new Drawable(520, 563, 300, 20, UIType.LABEL, label, action);
        drawable.x -= (drawable.labelWidth / 2);
        panel.add(drawable);
        panel.add(new Drawable(850, 570, 80, 20, UIType.LABEL, "Close", "frame.player.chooseFace();frame.gotoPlace(frame.player.getPlace());"));
    }

    public void paintBattle(String who, int turn, int weapon, int enemyMight, String endTask) {
        try {
            int x = 150;
            int y = 100;
            int width = 760;
            int height = 500;
            boolean end = false;

            String endDo = "frame.gotoPlace(frame.player.getPlace());";

            String abuLog = "Abu's actions:§";
            String enemyLog = "Actions for " + armies.getName(who) + ":§";

            panel.add(new Drawable(x, y, width, height, UIType.BPANEL, getImage("logoIntro"), "Fighting against " + armies.getName(who) + " starting turn: " + turn));
            panel.add(new Drawable(x + 10, y + 10, 0, 0, UIType.IMAGE, getImage(armies.getString(who, "Picture"))));
            panel.add(new Drawable(x + width - 110, y + 10, 0, 0, UIType.IMAGE, getImage("abu")));
            panel.add(new Drawable(x + 50, y + 150, 0, 0, UIType.IMAGE, getImage(armies.getString(who, "Soldier"))));
            panel.add(new Drawable(x + width - 150, y + 150, 0, 0, UIType.IMAGE, getImage("abuSoldier")));

            int abu = (int) player.getHealth();
            int enemy = enemyMight;
            int fight = random.nextInt(100);
            int jinx = random.nextInt(10);

            Item item;

            if (player.hasItem("13000")) {
                item = market.getItem("13000");
                enemy -= item.getForce() - (jinx);
                abuLog += "§- Your sword swings into action";
            } else if (player.hasItem("13010")) {
                item = market.getItem("13010");
                enemy -= item.getForce() - (jinx);
                abuLog += "§- You struck with the dagger";
            }

            switch (weapon) {
                case 1: // dagger
                    item = market.getItem("13010");
                    enemy -= item.getForce() - (jinx);
                    abuLog += "§- You struck with the dagger";
                    break;
                case 2: // sword
                    item = market.getItem("13000");
                    enemy -= item.getForce() - (jinx);
                    abuLog += "§- Your sword swings into action";
                    break;
                case 3: // djinnies
                    enemy -= 50 - (jinx);
                    abuLog += "§- The djinnies appear and attack the enemy";
                    break;
            }

            if (fight >= 99 && fight < 100) {
                enemy -= 20 - (jinx);
                abuLog += "§- You get a critical strike!";
            } else if (fight >=1 && fight < 2) {
                abu -= 20 - (jinx);
                enemyLog += "§- The enemy forces get strike a mighty blow!";

            }

            if (player.addictedHashish && fight >= 0 && fight < 25) {
                abu -= 1;
                abuLog += "§- You are hampered by your addiction to hashish";
            } else if (player.addictedOpium && fight >= 0 && fight < 50) {
                abu -= 3;
                abuLog += "§- Your opium addiction causes you to lose your will to fight";
            } else if (player.sickPlague && fight >= 0 && fight < 75) {
                abu -= 6;
                abuLog += "§- The plague you are carrying hinders your battle greatly";
            }

            if (player.moral < -75 && fight >= 0 && fight < 75) {
                abu += 10 - (jinx);
                enemyLog += "§- Your vile deeds on the battlefield cause your foes to tremble";
            } else if (player.moral > 75 && fight >= 0 && fight < 75) {
                abu += 10 - (jinx);
                enemyLog += "§- Your good reputation precedes you, some of the enemies soldiers abandon their foul cause";
            }

            int enemyHit = (armies.getStrengh(who) + random.nextInt(10));
            int abuHit = (player.getStrengh() + random.nextInt(10));
            abu -= enemyHit;
            enemy -= abuHit;

            abuLog += hitLog("You", "the enemy", abuHit);
            enemyLog += hitLog("The enemy", "you", enemyHit);

            if (enemy < 1) {
                enemyLog += "§- The enemy forces have been annihilated";
                abuLog += "§- You won!";
                end = true;
                player.add(who + "won");
            } else if (abu < 1) {
                abuLog += "§- You were defeated!";
                if (enemy > 0)
                    endDo = "frame.gotoDeath(\"Abu was defeated in a battle that occurred the surroundings of " + player.getNiceCity()
                            + " against the forces of " + armies.getName(who) + ".\", 1);";
                else
                    endDo = "frame.gotoDeath(\"Abu died in a glorious battle on the surroundings of " + player.getNiceCity()
                            + " while destroying the remnants of the forces of " + armies.getName(who) + ".\", 2);";
                end = true;
            }

            player.health = abu;

            Font statusFont = new Font(typeface, 0, 10);

            panel.add(new Drawable(x + 200, y + 150, 150, 200, UIType.LABEL, enemyLog, statusFont, Color.black));
            panel.add(new Drawable(x + width - 350, y + 150, 150, 200, UIType.LABEL, abuLog, statusFont, Color.black));

            // FIX 26.4.2005 Included endTask in the next turn script
            String nextTurn = "frame.player.nextDay();frame.gotoBattle(\"" + who + "\", " + (turn + 1) + ", !weapon, " + enemy + ", \"" + endTask.replaceAll("\"", "\\\\\"") + "\");";

            if (!end) {
                player.inBattle = false;
                //panel.add(new Drawable(170, 570, 100, 20, UIType.LABEL, "Invoke djinnis", nextTurn.replaceAll("!weapon", "3")));
                //if (player.hasItem("13000"))
                //    panel.add(new Drawable(290, 570, 85, 20, UIType.LABEL, "Use sword", nextTurn.replaceAll("!weapon", "2")));
                //else if (player.hasItem("13010"))
                //    panel.add(new Drawable(290, 570, 85, 20, UIType.LABEL, "Use dagger", nextTurn.replaceAll("!weapon", "1")));
                panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "Next round", nextTurn.replaceAll("!weapon", "0")));
            } else {
                player.inBattle = false;

                if (!endTask.isEmpty())
                    panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "End battle", "frame.jukebox.loopMusic(frame.jukebox.lastMusic);frame.player.chooseFace();" + endTask));
                else if (!endDo.isEmpty())
                    panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "End battle", "frame.jukebox.loopMusic(frame.jukebox.lastMusic);frame.player.chooseFace();" + endDo));
                else
                    panel.add(new Drawable(810, 570, 80, 20, UIType.LABEL, "End battle", "frame.jukebox.loopMusic(frame.jukebox.lastMusic);frame.player.chooseFace();frame.gotoPlace(frame.player.getPlace());"));

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String hitLog(String entity, String foe, int hit) {
        String conjugaison = (entity == "The enemy") ? "s " : " ";
        if (hit < 5)
            return "§- " + entity + " slaps" + conjugaison + foe;
        else if (hit < 15)
            return "§- " + entity + " feebly hit" + conjugaison + foe;
        else if (hit < 25)
            return "§- " + entity + " hit" + conjugaison + foe;
        else if (hit < 50)
            return "§- " + entity + " strike" + conjugaison + foe + " hard";
        else
            return "§- " + entity + " score" + conjugaison + "a devastating hit";
    }

    public void paintWorldMap(boolean showIcon) {
        try {
            // first we get the active map entries, which depend on the
            // transport type and also if we have a travel
            // permit to europe
            objects.add(new Drawable(0, 0, getHeight(), getWidth(), UIType.IMAGE, getImage(routes.getBackground())));
            ArrayList<Travel> entries = routes.getEntries(player.getCity(), player.getTransport(), (player.hasItem("10630") && player.getTransport() != Transport.CARAVAN));
            for (Travel entry : entries) {
                objects.add(new Drawable(entry.x - 10, entry.y - 10, entry.width, entry.height, UIType.IMAGE, getImage("pixel", true), null,
                    "Go to " + entry.nice, entry.getDescription(player.getTransport()), entry.getAction(player.getTransport()), null, null, 0));
            }

            // right side of the window
            //int y = getHeight() - 465;
            //int x = 980;
            // left side of the window
            int y = getHeight() - 565;
            int x = 50;
            Inventory items = player.items;
            for (Item item : items) {
                if (item.isActive()) {
                    Image image = getImage(item.getPicture());
                    if (item.getName().toLowerCase().equals(player.getTransport().getName()))
                        image = getImage(item.getPicture().replaceAll(".png", "_active.png"));
                    objects.add(new Drawable(x - (image.getWidth(this) / 2), y + (65 - (image.getHeight(this) / 2)), 75, 75, UIType.IMAGE, image,
                        item.getName() + "§" + item.getDescription(), item.getAction()));
                    y += 65;
                }
            }

            if (showIcon)
                objects.add(new Drawable(player.xHomeCountry - 14, player.yHomeCountry - 14, 0, 0, UIType.IMAGE, getImage("playerIcon", true)));

            if (isShowOptions()) paintOptions(objects);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paintGlass() {
        try {
            // status bar and the city/world icons

            if (isShowOptions()) {
                getImage("statusFace", player.getStatusFace(), true, true);
                hud.add(new Drawable(20, getHeight() - 169, 155, 155, UIType.STATUS, getImage("statusFace", true), getImage("statusBar", true),
                        "Open status", "Open status", "frame.gotoStatus(Tab.STATUS, \"\", 0, Tab.STATUS, 0);", null, null, 0));

                hud.add(new Drawable(386, getHeight() - 38, 100, 19, UIType.LABEL, player.getAge() + " years",
                        "Abu's age is " + player.getDay() + " days " + player.getMonth() + " months " + player.getAge() + " years", "result=false;", new Font(typeface, 1, 12), Color.magenta));

                
                // FIX 19.04.2006 by Markus Piotrowski, show health
                hud.add(new Drawable(460, getHeight() - 38, 100, 19, UIType.LABEL, "  ", "Abu's health is " + (int)player.getHealth(),
                        "result=false;", new Font(typeface, 1, 12), Color.magenta));
                
                // FIX 19.04.2006 by Markus Piotrowski, show money
                hud.add(new Drawable(550, getHeight() - 38, 100, 19, UIType.LABEL, "  ", "Abu's money is " + player.getMoney() +" silver dirhams ",
                        "result=false;", new Font(typeface, 1, 12), Color.magenta));
                
                // FIX 04.05.2006 by Markus Piotrowski, show culture
                hud.add(new Drawable(680, getHeight() - 38, 90, 19, UIType.LABEL, "  ", "Abu's culture is " + player.getCulture(),
                        "result=false;", new Font(typeface, 1, 12), Color.magenta));
                
                // FIX 01.05.2006 by Markus Piotrowski, show moral
                hud.add(new Drawable(780, getHeight() - 38, 70, 19, UIType.LABEL, "MORAL", "Abu's moral is " + player.getMoral(),
                        "result=false;", new Font(typeface, 1, 12), Color.black));
        
                if (!"highwaymen".equals(player.getPlace()) && !"pirates".equals(player.getPlace())) {
                    hud.add(new Drawable(195, getHeight() - 100, 65, 60, UIType.IMAGE, getImage("localMap", true),
                            "Go to map of " + places.getTitle(player.getCity()), "frame.gotoPlace(\"" + player.getCity() + "\");"));
                    if (isShowMapGlobe())
                        hud.add(new Drawable(265, getHeight() - 100, 60, 60, UIType.IMAGE, getImage("globalMap", true), null,
                                "Go to map of the world",  "frame.gotoWorldMap();"));
                }
                int y = 698;
                if (player.sickGeneral || player.sickPoisoned || player.sickPlague) {
                    if (player.sickGeneral)
                        y += 10;
                    
                    if (player.sickPoisoned || player.sickGeneral) {
                        hud.add(new Drawable(480, y, 20, 20, UIType.IMAGE, getImage("statusPlague", true),
                            "You are poisoned",
                            "frame.gotoStatus(Tab.HELP, \"5430\", 0, Tab.STATUS, 0);"));
                        if (!player.sickPlague)
                            y -= 40;
                    }
                
                    if (player.sickPlague) {
                        hud.add(new Drawable(480, y, 20, 20, UIType.IMAGE, getImage("statusPlague", true),
                            "You have the plague",
                            "frame.gotoStatus(Tab.HELP, \"5430\", 0, Tab.STATUS, 0);"));
                        y -= 40;
                    }
                }
                if (player.isAddictedHashish()) {
                    hud.add(new Drawable(473, y, 27, 27, UIType.IMAGE, getImage("statusHashish", true),
                        "You are addicted to hashish",
                        "frame.gotoStatus(Tab.HELP, \"3360\", 0, Tab.STATUS, 0);"));
                    y -= 43;
                }
                if (player.isAddictedOpium())
                    hud.add(new Drawable(479, y, 27, 27, UIType.IMAGE, getImage("statusOpium", true),
                        "You are addicted to opium",
                        "frame.gotoStatus(Tab.HELP, \"3360\", 0, Tab.STATUS, 0);"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paintPlace(String to) {
        try {
            objects.add(new Drawable(0, 0, getHeight(), getWidth(), UIType.IMAGE, getImage(places.getBackground(to))));
            ArrayList<Spicebean> pictures = places.getPictures(to);
            for (Spicebean sb : pictures) {
                if (sb.isDoable())
                    objects.add(new Drawable(sb.x, sb.y, sb.width, sb.height, UIType.IMAGE, getImage(sb.picture), sb.name, sb.action));
                else
                    objects.add(new Drawable(sb.x, sb.y, 0, 0, UIType.IMAGE, getImage(sb.picture)));
            }

            int x = 25;
            int y = 25;
            int width;
            int height;
            int counter = 0;
            String picture;

            // if there are any items that want to be shown on the screen, draw them
            Inventory items = player.items;
            for (Item item : items) {
                if (item.isActive()) {
                    if (item.getPictureBig() == null || item.getPictureBig().isEmpty()) {
                        picture = item.getPicture();
                        width = 55;
                        height = 55;
                    } else {
                        picture = item.getPictureBig();
                        width = 100;
                        height = 100;
                    }

                    objects.add(new Drawable(x, y, width, height, UIType.IMAGE, getImage(picture), item.getName() + "§" + item.getDescription(), item.getAction()));
                    x += 75;
                    switch (counter) {
                        default:
                            break;
                        case 11:
                        case 23:
                        case 35:
                        case 47:
                            y += 90;
                            x = 25;
                            break;
                    }
                    counter++;
                }
            }

            x = 210;

            // random drawables that get thrown on the screen
            Drawables drawables = places.getDrawables(to);
            objects.addAll(drawables);

            if (isShowOptions()) paintOptions(objects);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paintDialog(String sentences) {
        try {
            ArrayList<Spicebean> actions = dialogs.getChoices(sentences);
            String path = "/pics/navigation/characters/" + dialogs.getTitle(sentences).toLowerCase().replaceAll(" ", "_") + ".png";
            Image image = getImage(path);

            // draw the panel and the label
            Drawable drawable = new Drawable(200, 56, getWidth() / 2 - 160, 150, UIType.LABEL, dialogs.getText(sentences));
            if (drawable.plainText.size() > 6)
                panel.add(new Drawable(20, 18, getWidth() - 70, 60 + (drawable.plainText.size()) * 18, UIType.PANEL, image, dialogs.getTitle(sentences)));
            else if (actions.size() > 3)
                panel.add(new Drawable(20, 18, getWidth() - 70, 100 + (actions.size() + 1) * 20, UIType.PANEL, image, dialogs.getTitle(sentences)));
            else
                panel.add(new Drawable(20, 18, getWidth() - 70, 155, UIType.PANEL, image, dialogs.getTitle(sentences)));
            panel.add(drawable);

            // draw all the lines of dialog choices
            int _y = 56;
            for (Spicebean spicebean : actions) {
                drawable = new Drawable(getWidth() / 2 + 70, _y, 340, 30, UIType.LABEL, spicebean.name, spicebean.action);
                panel.add(drawable);
                _y += 16 * drawable.plainText.size() - 1;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paintDiary(int page) {
        try {
            int y;
            int x;
            panel.add(new Drawable(150, 100, 760, 500, UIType.PANEL, getImage("/pics/abu/face/dead2.png"), getImage(player.logo, true),
                    "Your diary", Color.black));

            Font statusFontBold = new Font(typeface, 1, 14);
            Font statusFont = new Font(typeface, 0, 14);
            Color color = Color.white;

            // ADD 10.04.2005 -- Want to see the book of life events in the order they happen in
            ArrayList<String> quests = player.getJournal().quests;
            String quest;

            if (page == 0) {
                Image image;

                x = 327;
                y = 360;
                switch (player.deathType) {
                    case 1:
                        image = getImage("/pics/abu/death/dead001.png");
                        x = 420;
                        y = 280;
                        break;
                    case 2:
                        image = getImage("/pics/abu/death/dead003a.png");
                        x = 528;
                        y = 177;
                        break;
                    case 3:
                        image = getImage("/pics/abu/death/dead004.png");
                        x = 420;
                        y = 280;
                        break;
                    case 4:
                        image = getImage("/pics/abu/death/dead005.png");
                        x = 400;
                        y = 280;
                        break;
                    default:
                        image = getImage("/pics/abu/death/dead002.png");
                        x = 400;
                        y = 280;
                        break;
                }

                panel.add(new Drawable(x, y, 0, 0, UIType.IMAGE, image));
                Drawable d1 = new Drawable((410 + 175), 495, 460, 20, UIType.LABEL, player.getLastWords(), statusFontBold, color);
                d1.x -= (d1.labelWidth / 2);
                panel.add(d1);
            } else {
                quest = quests.get(page - 1);
                int when = Integer.parseInt(player.getJournal().timestamp.get(quest));
                int year = (when / 360) + 17 - 500;
                String time = ", age of " + year;
                Image image = getImage(player.getJournal().getPicture(quest));
                String questText = player.getJournal().getString(quest, "Achievement");
                panel.add(new Drawable((507 + 75) - (image.getWidth(this) / 2), (350) - (image.getHeight(this) / 2), 0, 0, UIType.IMAGE, image));
                Drawable d1 = new Drawable((410 + 175), 515, 460, 20, UIType.LABEL, player.getJournal().getString(quest, "Display") + time, statusFontBold, color);
                d1.x -= (d1.labelWidth / 2);
                panel.add(d1);
                Drawable d2 = new Drawable((410 + 175), 540, 460, 380, UIType.LABEL, questText, statusFont, color);
                d2.x -= (d2.labelWidth / 2);
                panel.add(d2);
            }

            if (page > 0)
                panel.add(new Drawable(170, 570, 120, 20, UIType.LABEL, "<< PREVIOUS", "frame.gotoDiary(" + (page - 1) + ");", statusFontBold, color));
            // FIX 10.5.2005 Clicking END GAME caused music to be turned back on
            panel.add(new Drawable(540, 570, 120, 20, UIType.LABEL, "END GAME",
                    "boolean p = frame.jukebox.musicOn;frame.jukebox.musicOn=false;frame.player = new Player();frame.jukebox.musicOn=p;frame.jukebox.loopMusic(\"/music/16_world_horizon.ogg\");frame.gotoPlace(\"menu\");", statusFontBold, color));
            if (page < player.getJournal().questsClosed.size())
                panel.add(new Drawable(840, 570, 120, 20, UIType.LABEL, "NEXT >>", "frame.gotoDiary(" + (page + 1) + ");", statusFontBold, color));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paintCalendar(String back) {
        paintCalendar(back, -1, 0, 0);
    }

    public void paintCalendar(String back, int number, int action, int total) {
        // this is the Hijra <-> Gregorian calendar calculator :-)
        // it was pretty fun to make this, even though we were pressed for time
        int x = 165;
        int y = 280;
        int width = 220;
        int height = 290;
        int newTotal = total;
        Font statusFontBold = new Font(typeface, 1, 11);
        Font statusFont = new Font(typeface, 0, 10);
        if (number > -1)
            newTotal = Integer.parseInt(String.valueOf(total) + number);
        if (action == 1)
            newTotal = -newTotal;
        panel.add(new Drawable(x, y, width, height, UIType.PANEL, null, null, "Hijra converter", null, null, null, Color.white, 1));
        panel.add(new Drawable(x + width - 20, y + 10, 20, 20, UIType.LABEL, "X", "Close converter", "frame.converter=0;" + back, statusFontBold, Color.black));
        panel.add(new Drawable(x + 40, y + 18, 140, 20, UIType.LABEL, "CALENDAR CONVERTER", statusFontBold, Color.black));
        panel.add(new Drawable(x + 43, y + 45, 120, 108, UIType.IMAGE, getImage("/pics/objects/converter.png")));

        panel.add(new Drawable(x + width - 70, y + 190, 40, 20, UIType.LABEL, "CLEAR", "Clear converter",
            "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"") + "\", -1, 0, 0);", statusFont, Color.black));

        panel.add(new Drawable(x + 26, y + 183, 50, 20, UIType.LABEL, "+/-", "Change year to positive/negative",
            "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"") + "\", -1, 1, " + newTotal + ");", statusFontBold, Color.black));

        int _x = x + 60;
        int _y = y + 138;
        Color color = Color.black;
        int counter = 0;
        for (int i = 1; i < 11; i++) {
            color = (i == number) ? Color.red : Color.black;

            if (i < 10) {
                _x += 20;
                panel.add(new Drawable(_x, _y, 13, 15, UIType.LABEL, String.valueOf(i), String.valueOf(i), "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"") + "\", " + i + ", 0, " + newTotal + ");", statusFontBold, color));
            } else {
                _x += 40;
                panel.add(new Drawable(_x, _y, 13, 15, UIType.LABEL, "0", "0", "frame.paintCalendar(\"" + back.replaceAll("\"", "\\\\\"") + "\", 0, 0, " + newTotal + ");", statusFontBold, color));
            }

            if (counter > 1) {
                _x = x + 60;
                _y += 15;
                counter = 0;
            }
            counter++;
        }
        int ah = newTotal - Math.round((newTotal / 33.0F)) + 622;
        int gre = (newTotal - 622) + Math.round((newTotal - 622) / 32.0F);
        String greStr = "AC";
        String ahStr = "AH";
        String greStr2 = "AC";
        String ahStr2 = "AH";
        if (gre < 0)
            ahStr = "BH";
        if (ah < 0)
            greStr = "BC";
        if (newTotal < 0)
            ahStr2 = "BH";
        if (newTotal < 0)
            greStr2 = "BC";
        panel.add(new Drawable(x + 40, y + 220, 300, 100, UIType.LABEL, "Hijra years to gregorian years", statusFont, Color.black));
        panel.add(new Drawable(x + 70, y + 235, 300, 100, UIType.LABEL, newTotal + " " + ahStr2 + " = " + ah + " " + greStr, statusFont, Color.black));
        panel.add(new Drawable(x + 40, y + 255, 300, 100, UIType.LABEL, "Gregorian years to Hijra years", statusFont, Color.black));
        panel.add(new Drawable(x + 70, y + 270, 300, 100, UIType.LABEL, newTotal + " " + greStr2 + " = " + gre + " " + ahStr, statusFont, Color.black));

        doLayout();
        repaint();
    }

    public void paintOptions(Drawables drawables) {
        drawables.add(new Drawable(getWidth() - 40, 10, 0, 0, UIType.IMAGE, getImage("controls", true)));
        drawables.add(new Drawable(getWidth() - 40, 88, 100, 19, UIType.IMAGE, getImage("pixel", true), "Close", "frame.exit();"));
        drawables.add(new Drawable(getWidth() - 40, 65, 100, 19, UIType.IMAGE, getImage("pixel", true), "Save", "frame.saveGame();"));
        drawables.add(new Drawable(getWidth() - 40, 40, 100, 19, UIType.IMAGE, getImage("pixel", true), "Load", "frame.loadGame();"));
        drawables.add(new Drawable(getWidth() - 40, 15, 100, 19, UIType.IMAGE, getImage("pixel", true), "Minimize", "frame.setExtendedState(Frame.ICONIFIED);"));
        drawables.add(new Drawable(getWidth() - 40, 111, 24, 19, UIType.IMAGE, getImage("pixel", true), "Back to menu",
                "frame.setShowOptions(false);frame.player.setLastPlace(\"" + player.getPlace() + "\");frame.gotoPlace(\"menu\");"));
        String helpid = "9239";
        if (helpid != null && !helpid.isEmpty())
            drawables.add(new Drawable(getWidth() - 39, 136, 100, 19, UIType.IMAGE, getImage("questionMark", true), "Help about world map",
                "frame.gotoStatus(Tab.HELP, \"" + helpid + "\", 0, Tab.STATUS, 0);"));
    }

    public void paintStatus(Tab tab, String id, int page, Tab from, int more) {
        try {
            // STATUS: the main status screen shows an overview of the player status
            // INVENTORY: all the items that the player has (well, not _all_ of them, just the ones we want to show
            // DIARY: open and done quests
            // HELP: help screen, index or individual help
            // LOG: the log of "all" the things that have happened, dialogs, etc
            // CULTURE: statistics for the culture points of the player
            // DEATH: the death screen, when the game ends

            int x;
            int y;
            int _y;
            int _x;
            int width;
            int height;
            panel.add(new Drawable(150, 100, 760, 500, UIType.PANEL, getImage("statusFace", true), getImage(player.logo, true), tab.getName()));

            Font statusFontBold = new Font(typeface, 1, 12);
            Font statusFont = new Font(typeface, 0, 10);

            if (tab != Tab.HELP) {
                x = 410;
                y = 240;
                _x = 594;
                _y = 255;
                width = 300;
                height = 20;
                panel.add(new Drawable(x, y - 30, width, height, UIType.LABEL, player.get("Name"), statusFontBold, Color.black));
                panel.add(new Drawable(x, y, width, height, UIType.LABEL, "date and place of born", statusFont, Color.black));
                panel.add(new Drawable(_x, y, width, height, UIType.LABEL, player.get("dateAndPlace"), statusFont, Color.black));

                Drawable wd = new Drawable(_x, _y, width, height, UIType.LABEL, player.get("wives"), statusFont, Color.black);
                int wives = wd.plainText.size();
                String label = (wives > 1) ? "wives" : "wife";
                panel.add(new Drawable(x, _y, width, height, UIType.LABEL, label, statusFont, Color.black));
                if (!player.get("wives").isEmpty()) panel.add(wd);
                else panel.add(new Drawable(_x, _y, width, height, UIType.LABEL, "none", statusFont, Color.black));

                Drawable cd = new Drawable(_x, _y + 2 + (wives * 9), width, height, UIType.LABEL, player.get("children"), statusFont, Color.black);
                int children = cd.plainText.size();
                label = (children > 1) ? "children" : "child";
                panel.add(new Drawable(x, _y + 2 + (wives * 9), width, height, UIType.LABEL, label, statusFont, Color.black));
                if (!player.get("children").isEmpty()) panel.add(cd);
                else panel.add(new Drawable(_x, 257 + (wives * 9), width, height, UIType.LABEL, "none", statusFont, Color.black));

                _x = 177;
                _y = 280;
                width = 200;
                panel.add(new Drawable(_x, _y, width, height, UIType.LABEL, "MONEY", statusFontBold, Color.black));
                panel.add(new Drawable(_x, _y + 20, width, height, UIType.LABEL, player.getMoney() + " silver dirhams", statusFont, Color.black));
            }

            switch (tab) {
                default:
                    break;
                case STATUS:
                    int increment = 20;
                    y = 370;
                    _x = 177;
                    width = 300;
                    height = 20;
                    boolean found = false;
                    boolean foundsuper = false;
                    // Property
                    panel.add(new Drawable(_x, y - 30, width, height, UIType.LABEL, "PROPERTY", statusFontBold, Color.black));
                    panel.add(new Drawable(_x, y, width, height, UIType.LABEL, "- lands", statusFont, Color.black));
                    x = 253;
                    width = 20;
                    height = 25;
                    String[] lands = {"12050", "14000", "14005", "14010", "14015", "14020", "14025", "14030", "14035", "14040", "14045"};
                    found = createImageDrawables(panel, lands, x, y - 5, width, height, increment, tab, page, found);
                    found = false;

                    String[] spices = {"10110", "10100", "10120", "10130", "10210", "10200", "10220", "10230"};
                    found = createImageDrawables(panel, spices, x, y + 22, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 30, "- spices", statusFont, Color.black);
                    found = false;

                    String[] art1 = {"10800", "10801", "10802", "10803", "10804", "10805"};
                    String[] art2 = {"10810", "10811", "10812"};
                    String[] art3 = {"10820"};
                    String[] art4 = {"10830", "10831"};
                    found = createImageDrawables(panel, art1, x, y + 53, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, art2, x, y + 53, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, art3, x, y + 53, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, art4, x, y + 53, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 60, "- works of art", statusFont, Color.black);
                    found = false;

                    String[] treasures1 = {"11600", "11601", "11602", "11603", "11604"};
                    String[] treasures2 = {"11610", "11611"};
                    String[] treasures3 = {"11620"};
                    String[] treasures4 = {"11630"};
                    String[] treasures5 = {"11640"};
                    String[] treasures6 = {"11650", "11651", "11652", "11660", "11661"};
                    String[] treasures7 = {"11670", "11671", "11672", "11673"};
                    String[] treasures8 = {"11680", "11680"};
                    found = createImageDrawables(panel, treasures1, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures2, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures3, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures4, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures5, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures6, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures7, x, y + 82, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, treasures8, x, y + 82, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 90, "- treasures", statusFont, Color.black);
                    found = false;

                    String[] luxury1 = {"10720"};
                    String[] luxury2 = {"10750", "10751"};
                    String[] luxury3 = {"10730", "10731", "10732", "10733"};
                    String[] luxury4 = {"10760"};
                    String[] luxury5 = {"10770"};
                    String[] luxury6 = {"10710"};
                    String[] luxury7 = {"10740", "10741", "10742", "10743", "10744", "10745"};
                    found = createImageDrawables(panel, luxury1, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury2, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury3, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury4, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury5, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury6, x, y + 112, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, luxury7, x, y + 112, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 120, "- luxury", statusFont, Color.black);
                    found = false;

                    String[] buildings1 = {"12000", "12001", "12002", "12003", "12004"};
                    String[] buildings2 = {"12060", "12061", "12062", "12063", "12064", "12065", "12066", "12067", "12068", "12069", "12070"};
                    String[] buildings3 = {"12080", "12081", "12082", "12083", "12084", "12085", "12086", "12087", "12088", "12089", "12090"};
                    String[] buildings4 = {"12030"};
                    String[] buildings5 = {"12010"};
                    String[] buildings6 = {"12020"};
                    String[] buildings7 = {"12040"};
                    found = createImageDrawables(panel, buildings1, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings2, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings3, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings4, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings5, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings6, x, y + 143, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, buildings7, x, y + 143, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 150, "- buildings", statusFont, Color.black);
                    found = false;

                    // Licenses
                    String[] licenses1 = {"10600", "10610"};
                    String[] licenses2 = {"10620"};
                    String[] licenses3 = {"10630"};
                    String[] licenses4 = {"10640"};
                    String[] licenses5 = {"10650", "10651", "10652", "10653", "10654", "10655", "10656", "10657", "10658", "10659", "10660"};
                    increment = 40;
                    x = 450;
                    _x = 460;
                    width = 40;
                    height = 40;
                    found = createImageDrawables(panel, licenses1, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses2, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses3, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses4, x, y - 10, width, height, increment, tab, page, found);
                    found = createImageDrawables(panel, licenses5, x, y - 10, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y - 30, "LICENSES", statusFontBold, Color.black);
                    found = false;

                    // Maps
                    String[] maps = {"11500", "11510", "11520", "11530"};
                    found = createImageDrawables(panel, maps, x, y + 60, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 40, "MAPS", statusFontBold, Color.black);
                    found = false;

                    // Workers
                    String[] workers = {"16000", "16010", "16020", "16030"};
                    found = createImageDrawables(panel, workers, x, y + 130, width, height, increment, tab, page, found);
                    createLabelDrawable(found, foundsuper, _x, y + 110, "WORKERS", statusFontBold, Color.black);
                    found = false;

                    // Supernatural
                    String[] amulets = {"11000", "11001", "11002", "11003", "11004", "11005", "11010", "11011", "11012", "11013", "11014", "11015"};
                    x = 723;
                    _x = 652;
                    width = 10;
                    height = 20;
                    increment = 13;
                    found = createImageDrawables(panel, amulets, x, y - 6, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y, "- amulets", statusFont, Color.black);
                    found = false;

                    String[] drinks = {"11030", "11031", "11040"};
                    found = createImageDrawables(panel, drinks, x, y + 24, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 30, "- drinks", statusFont, Color.black);
                    found = false;

                    String[] buckles = {"11050", "11051", "11052", "11053", "11054", "11055", "11056"};
                    width = 13;
                    height = 22;
                    increment = 20;
                    found = createImageDrawables(panel, buckles, x, y + 54, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 60, "- buckles", statusFont, Color.black);        
                    found = false;

                    String[] rosary = {"11020", "11025"};
                    width = 60;
                    height = 50;
                    increment = 50;
                    found = createImageDrawables(panel, rosary, x, y + 87, width, height, increment, tab, page, found);
                    String label = (player.hasItem("11020")) ? "- rosary" : "- talisman";
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 90, label, statusFont, Color.black);
                    found = false;

                    String[] flying = {"10335", "10345", "10355"};
                    found = createImageDrawables(panel, flying, x, y + 105, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 120, "- flying", statusFont, Color.black);
                    found = false;

                    String[] others = {"11700", "13000"};
                    increment = 40;
                    found = createImageDrawables(panel, others, x, y + 150, width, height, increment, tab, page, found);
                    foundsuper = createLabelDrawable(found, foundsuper, _x, y + 150, "- others", statusFont, Color.black);
                    found = false;
                    createLabelDrawable(foundsuper, foundsuper, _x, y - 30, "SUPERNATURAL", statusFontBold, Color.black);
                    break;
                case Tab.INVENTORY:
                    _x = 150;
                    int per = 16;
                    Item item;
                    Inventory inventory = player.getInventory();
                    int i = page * per;
                    int j = Math.min((page + 1) * per, inventory.size() - 1);
                    if (inventory.size() > per) {
                        if (page > 0)
                            panel.add(new Drawable(190, 340, 80, 20, UIType.LABEL, "<< Previous", "Goto previous page",
                                "frame.gotoStatus(Tab.INVENTORY, \"\", " + (page - 1) + ", Tab." + tab + ", 0);"));
                        int k = 0;
                        _y = 312;
                        for (int l = (inventory.size() - 1) / per; k <= l; k++) {
                            String strPage = (k + 1) + (k == l ? "" : ",");
                            Color labelColor = (page == k) ? Color.red : null;
                            Font labelFont = (page == k) ? fontBold : null;
                            panel.add(new Drawable(290 + (k * 20), 340, 18, 20, UIType.LABEL, strPage, "Goto page " + (k + 1),
                                "frame.gotoStatus(Tab.INVENTORY, \"\", " + k + ", Tab." + tab + ", 0);", labelFont, labelColor));
                        }
                        if (page < ((inventory.size() - 1) / per))
                            panel.add(new Drawable(290 + (k * 20), 340, 80, 20, UIType.LABEL, "Next >>", "Goto next page", "frame.gotoStatus(Tab.INVENTORY, \"\", " + (page + 1) + ", Tab." + tab + ", 0);"));

                    }

                    _x = 150;
                    _y = 360;
                    if ((inventory.size() >= per && page != (inventory.size() / per)) || (page == 0 && inventory.size() != per)) j++;

                    for (int l = i; l < j; l++) {
                        if (_y > 500) {
                            _x += 94;
                            _y = 360;
                        }
                        item = inventory.get(l);
                        Image image = getImage(item.getPicture());
                        i++;
                        panel.add(new Drawable(_x + (47 - (image.getWidth(this) / 2)), _y + (50 - (image.getHeight(this) / 2)), 70, 70, UIType.IMAGE, getImage(item.getPicture()),
                            item.getName() + "§" + item.getDescription(), "frame.gotoStatus(Tab.HELP,\"" + item.getHelpId() + "\", " + page + ", " + tab + ", 0);"));
                        _y += 100;
                    }
                    break;
                case DIARY:
                    increment = 20;
                    x = 177;
                    y = 370;
                    width = 300;
                    height = 20;
                    panel.add(new Drawable(x, y - 30, width, height, UIType.LABEL, "OPEN QUESTS", statusFontBold, Color.black));

                    Iterator<String> objects = player.getJournal().questsOpen.keySet().iterator();
                    String quest;
                    String additional;
                    int counter = 0;
                    while (objects.hasNext()) {
                        quest = objects.next();
                        // FIX 29.4.2005 Do not display null values
                        if (quest == null) continue;
                        additional = player.getJournal().get(quest);
                        if (!additional.isEmpty() && additional.charAt(0) == '!')
                            if (!additional.contains("§")) additional = "";
                            else additional = additional.substring(additional.indexOf("§"));
                        // FIX 29.4.2005 Do not display null values
                        if (player.getJournal().getString(quest, "Display") != null && player.getJournal().getString(quest, "Text") != null)
                            panel.add(new Drawable(x, y, 180, height, UIType.LABEL, player.getJournal().getString(quest, "Display"), player.getJournal().getString(quest, "Text") + additional, "result=false;", statusFont, Color.black));
                        y += increment;
                        counter++;
                        if (counter == 9) {
                            y = 370;
                            x += 180;
                        }
                    }

                    increment = 20;
                    y = 370;
                    counter = 0;
                    x = 450;
                    panel.add(new Drawable(450, y - 30, width, height, UIType.LABEL, "QUESTS DONE", statusFontBold, Color.black));

                    if (player.getJournal().questsClosed.size() > (18 + more)) {
                        panel.add(new Drawable(750, y - 30, width, height, UIType.LABEL, "MORE >>", "frame.gotoStatus(Tab." + tab + ", \"\", " + page + ", Tab." + from + ", " + (more + 18) + ");", statusFontBold, Color.black));
                    }
                    if (more > 0) {
                        panel.add(new Drawable(650, y - 30, width, height, UIType.LABEL, "<< LESS", "frame.gotoStatus(Tab." + tab + ", \"\", " + page + ", Tab." + from + ", " + (more - 18) + ");", statusFontBold, Color.black));
                    }

                    Iterator<String> quests = player.getJournal().questsClosed.keySet().iterator();
                    quest = "";
                    int looper = 0;
                    while (quests.hasNext()) {

                        quest = quests.next();
                        if (looper >= more) {
                            panel.add(new Drawable(x, y, 180, height, UIType.LABEL, player.getJournal().getString(quest, "Display"), player.getJournal().getString(quest, "Text") + player.getJournal().get(quest), "result=false;", statusFont, Color.black));
                            y += increment;

                            counter++;
                            if (counter == 9) {
                                y = 370;
                                x += 180;
                            } else if (counter == 18)
                                break;
                        }
                        looper++;
                    }
                    break;
                case HELP:
                    if (id.isEmpty()) {
                        String[] alphabet = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
                        Inventory helps = market.getHelp(alphabet[more]);

                        for (i = 0; i < alphabet.length; i++) {
                            Color hcolor = (i == more) ? Color.red : Color.black;
                            panel.add(new Drawable(190 + (i * 27), 310, 15, 15, UIType.LABEL, alphabet[i], "Show help for letter " + alphabet[i], "frame.gotoStatus(Tab." + tab + ", \"\", " + page + ", Tab." + from + ", " + i + ");", statusFontBold, hcolor));
                        }

                        _y = 340;
                        _x = 177;
                        for (Item help : helps) {
                            panel.add(new Drawable(_x, _y, 230, 20, UIType.LABEL, help.name, "frame.gotoStatus(Tab." + tab + ", \"" + help.id + "\", " + page + ", Tab." + tab + ", 0);", statusFontBold, Color.black));
                            _y += 30;

                            if (_y > 530) {
                                _y = 340;
                                _x += 190;
                            }
                            if (_x > 750)
                                break;
                        }
                    } else {
                        item = market.getItem(id);
                        Image image = getImage(item.getPictureBig());
                        panel.add(new Drawable((200 + 75) - (image.getWidth(this) / 2), (300 + 119) - (image.getHeight(this) / 2), 0, 0, UIType.IMAGE, image));
                        panel.add(new Drawable(410, 210, 460, 20, UIType.LABEL, item.getName(), fontBold, Color.black));
                        Drawable drawable = new Drawable(410, 240, 460, 380, UIType.LABEL, item.getDescription());
                        panel.add(new Drawable(750, 529, 120, 20, UIType.LABEL, "Calendar converter",
                         "frame.converter=frame.panel.size();frame.paintCalendar(\"frame.gotoStatus(Tab." + tab + ", \\\"" + id + "\\\", " + page + ", " + "Tab." + from + ", " + more + ");\");"));
                        x = 570;
                        y = 529;
                        width = 40;
                        height = 20;
                        int pagination = 15;
                        int endIndex = more * pagination;

                        if (more > 0) drawable.plainText.subList(0, endIndex).clear(); 
                        boolean beyond = drawable.plainText.size() > pagination;
                        if (beyond) drawable.plainText.subList(pagination + 1, drawable.plainText.size()).clear();

                        int xLess = (beyond && more < 3) ? x - 20 : x;
                        int xMore = (beyond && more > 0) ? x + 20 : x;
                        Drawable drawableLess = new Drawable(xLess, y, width, height, UIType.LABEL, "Less", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (more - 1) + ");");
                        Drawable drawableMore = new Drawable(xMore, y, width, height, UIType.LABEL, "More", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (more + 1) + ");");

                        switch (more) {
                            case 0:
                                if(beyond) panel.add(drawableMore);
                                break;
                            case 1:
                            case 2:
                                panel.add(drawableLess);
                                if(beyond) panel.add(drawableMore);
                                break;
                            case 3:
                                panel.add(drawableLess);
                                break;
                            default:
                                break;
                        }
                        panel.add(drawable);
                        panel.add(new Drawable(780, 570, 65, height, UIType.LABEL, "<< Back", "frame.gotoStatus(" + "Tab." + from + ", \"\", " + page + ", Tab.STATUS, 0);"));
                    }
                    break;
                case LOG:
                    x = 550;
                    y = 545;
                    _y = 370;
                    _x = 177;
                    width = 40;
                    height = 20;
                    counter = player.log.size() - 1;

                    panel.add(new Drawable(_x, _y - 30, 100, 200, UIType.LABEL, "LOG", statusFontBold, Color.black));

                    Drawable drawable;
                    while (_y < 500 && counter > -1) {
                        if (counter <= more) {
                            LogItem logItem = player.log.get(counter);
                            drawable = new Drawable(_x, _y, 700, 200, UIType.LABEL, logItem.toString(), statusFont, Color.black);
                            panel.add(drawable);
                            _y += drawable.plainText.size() * 11 + 3;
                        }
                        counter--;
                    }
                    if (more > 0) {
                        int left = more - 2;
                        left = (left > 0) ? left : 0;
                        panel.add(new Drawable(x + 40, y, width, height, UIType.LABEL, "More", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (left) + ");"));
                    }
                    if (more < player.log.size() - 1)
                        panel.add(new Drawable(x, y, width, height, UIType.LABEL, "Less", "frame.gotoStatus(Tab." + tab + ", \"" + id + "\", " + page + ", " + "Tab." + from + ", " + (more + 2) + ");"));
                    break;
                case CULTURE:
                    int middleX = 510;
                    int startY = 315;
                    int labelWidth = 100;
                    int lineHeight = 10;
                    Color pink = new Color(220, 0, 255);

                    panel.add(new Drawable(middleX, startY, middleX, startY + 260, UIType.LINE, Color.blue, 1));

                    startY += 5;
                    Font cfont = null;
                    Color leftColor = Color.red;
                    Color rightColor = Color.blue;
                    for (String city : Player.cities) {
                        int culture = player.getCulture(city);
                        int cultureWidth = culture * 2;
                        cfont = (city.equals(player.getCity())) ? new Font(typeface, Font.BOLD, 10) : statusFont;
                        Drawable cityLabel = new Drawable(middleX, startY - 3, labelWidth, lineHeight, UIType.LABEL, city.toUpperCase(), cfont, Color.black);

                        if (culture < 0) {
                            leftColor = (culture > -16) ? pink : Color.red;
                            panel.add(new Drawable(middleX + cultureWidth, startY, -(cultureWidth), lineHeight, UIType.RECTANGLE, "", null, leftColor));
                            cityLabel.x = middleX + 5;
                        } else {
                            rightColor = (culture < 16) ? pink : Color.blue;
                            panel.add(new Drawable(middleX, startY, cultureWidth, lineHeight, UIType.RECTANGLE, "", null, rightColor));
                            cityLabel.x = middleX - 5 - cityLabel.labelWidth;
                        }
                        panel.add(cityLabel);
                        startY += 15;
                    }
                    // meaning of red
                    panel.add(new Drawable(760, 475, 10, lineHeight, UIType.RECTANGLE, "", null, Color.red));
                    panel.add(new Drawable(773, 472, 140, lineHeight, UIType.LABEL, "European influence strong", cfont, Color.black));
                    // ..blue
                    panel.add(new Drawable(760, 500, 10, lineHeight, UIType.RECTANGLE, "", null, Color.blue));
                    panel.add(new Drawable(773, 497, 140, lineHeight, UIType.LABEL, "My influence strong", cfont, Color.black));
                    // ..and the meaning of pink :-)
                    panel.add(new Drawable(760, 525, 10, lineHeight, UIType.RECTANGLE, "", null, pink));
                    panel.add(new Drawable(773, 522, 140, lineHeight, UIType.LABEL, "Close to neutral", cfont, Color.black));

                    break;
                case DEATH:
                    x = 420;
                    y = 350;
                    width = 270;
                    height = 200;

                    String action = "frame.setSowStatus(false);frame.player.logo=\"logoDeath2\";frame.gotoPlace(\"gameover" + (player.moral > 0 ? "paradise" : "hell") + "\");";

                    switch (player.deathType) {
                        case 1:
                            panel.add(new Drawable(x, y, width, height, UIType.IMAGE, getImage("/pics/abu/death/dead001.png"), "Game over", action));
                            break;
                        case 2:
                            panel.add(new Drawable(528, 177, width, 400, UIType.IMAGE, getImage("/pics/abu/death/dead003.png"), "Game over", action));
                            break;
                        case 3:
                            panel.add(new Drawable(x, y, width, height, UIType.IMAGE, getImage("/pics/abu/death/dead004.png"), "Game over", action));
                            break;
                        case 4:
                            panel.add(new Drawable(400, 280, width, height, UIType.IMAGE, getImage("/pics/abu/death/dead005.png"), "Game over", action));
                            break;
                        default:
                            panel.add(new Drawable(400, y, width, height, UIType.IMAGE, getImage("/pics/abu/death/dead002.png"), "Game over", action));
                            break;
                    }
                    break;
            }
            // Navigation bar
            String[] actions = {
                "frame.gotoStatus(Tab.STATUS, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.INVENTORY, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.DIARY, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.HELP, \"\", " + page + ", Tab.STATUS, 0);",
                "frame.player.chooseFace();frame.gotoStatus(Tab.LOG, \"\", " + page + ", Tab." + tab + ", (frame.player.log.size()-1));",
                "frame.player.chooseFace();frame.gotoStatus(Tab.CULTURE, \"\", " + page + ", Tab." + tab + ", 0);",
                "frame.setShowOptions(false);frame.player.logo=\"logoDeath2\";frame.gotoPlace(\"gameover" + (player.moral > 0 ? "paradise" : "hell") + "\");",
                "frame.player.chooseFace();frame.gotoPlace(frame.player.getPlace());"
            };
            x = 170;
            y = 570;
            _x = 850;
            width = 80;
            height = 20;
            for (Tab tabToDraw : Tab.values()) {
                int i = tabToDraw.ordinal();
                String label = tabToDraw.getName();
                Color color = (tab == tabToDraw) ? Color.red : Color.black;
                if (tabToDraw != Tab.DEATH) {
                    panel.add(new Drawable(x, y, width, height, UIType.LABEL, label, actions[i], fontBold, color));
                    x += 100;
                } else {
                    if (player.inGameOver) panel.add(new Drawable(_x, y, width - 20, height, UIType.LABEL, label, actions[i], fontBold, color));
                    else panel.add(new Drawable(_x, y, width - 20, height, UIType.LABEL, "Close", actions[7], fontBold, color));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean createImageDrawables(Drawables panel, String[] objects, int x, int y, int width, int height, int increment, Tab tab, int page, boolean found) {
        for (String object : objects) {
            if (player.hasItem(object, 1, false)) {
                Item item = market.getItem(object);
                Image image = getImage(item.getPictureStatus());
                panel.add(createImageDrawable(x, y, width, height, tab, page, image, item));
                x += increment;
                found = true;
                break;
            }
        }
        return found;
    }

    private Drawable createImageDrawable(int x, int y, int width, int height, Tab tab, int page, Image image, Item item) {
        String action = "frame.gotoStatus(Tab.HELP,\"" + item.getHelpId() + "\", " + page + ", Tab." + tab + ", 0);";
        String description = item.getName() + "§" + item.getDescription();
        return new Drawable(x, y, width, height, UIType.IMAGE, image, description, action);
    }

    private boolean createLabelDrawable(boolean found, boolean foundsuper, int x, int y, String label, Font statusFont, Color color) {
        if (found) {
            panel.add(new Drawable(x, y, 300, 20, UIType.LABEL, label, statusFont, color));
            foundsuper = true;
        }
        return foundsuper;
    }

    // BOOLEAN SETTERS AND GETTERS

    public boolean isIdle() {
        return idle;
    }

    public void setIdle(boolean idle) {
        this.idle = idle;
    }

    public boolean isModal() {
        return modal;
    }

    public void setModal(boolean modal) {
        this.modal = modal;
    }

    public boolean isShowMapGlobe() {
        return this.showMapGlobe;
    }

    public void setShowMapGlobe(boolean showMapGlobe) {
        this.showMapGlobe = showMapGlobe;
    }

    public boolean isShowOptions() {
        return this.showOptions;
    }

    public void setShowOptions(boolean showOptions) {
        this.showOptions = showOptions;
    }

    public void saveGame() {
        try {
            // XStream.. gotta love it! I mean, how much less code can you write?
            String save = xstream.toXML(player);
            FileDialog fd = new FileDialog(this, "Save game", FileDialog.SAVE);
            fd.setDirectory(".");
            fd.setFile("spicetrade.sav");
            fd.setVisible(true);
            String file = fd.getDirectory() + fd.getFile();
            tools.writeFile(file, save);
            jukebox.playSound("/music/fx_signal_bell_hitlink.ogg");
            tools.showMessage(this, "Game saved!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loadGame() {
        try {
            jukebox.stopAll();
            FileDialog fd = new FileDialog(this, "Load game", FileDialog.LOAD);
            fd.setDirectory(".");
            fd.setFile("spicetrade.sav");
            fd.setVisible(true);
            // FIX: 12.4.2005 Changed the loading and save routine so that the directory information is used also
            String file = fd.getDirectory() + fd.getFile();
            if (fd.getFile() == null) return;
            String load = tools.readFile(file, false);
            jukebox.playSound("/music/fx_signal_bell_hitlink.ogg");
            player = (Player) xstream.fromXML(load);
            setShowOptions(true);
            //tools.showMessage(this, "Game loaded.");
            // check if the player has a travel permit
            if (player.hasAnyItems("10620", "10630"))
                setShowMapGlobe(true);
            player.chooseFace();
            refresh();
            Thread.sleep(50);
            gotoPlace(player.getPlace());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveSettings() {
        try {
            Settings settings = new Settings();
            settings.setDifficulty(player.getDifficulty());
            settings.setMusicOn(jukebox.isMusicOn());
            settings.setFullScreen(player.isFullScreen());
            String save = xstream.toXML(settings);
            tools.writeFile("spicetrade.properties", save);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loadSettings() {
        try {
            String load = tools.readFile("spicetrade.properties", false);
            Settings settings = new Settings();
            if (load != null && !load.isEmpty())
                settings = (Settings) xstream.fromXML(load);
            player.setDifficulty(settings.getDifficulty());
            player.setFullScreen(settings.isFullScreen());
            jukebox.setMusicOn(settings.isMusicOn());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Image getImage(String picture) {
        return getImage("", picture, false);
    }

    public Image getImage(String name, boolean cache) {
        return getImage(name, "", cache);
    }

    public Image getImage(String name, String picture, boolean cache) {
        return getImage(name, picture, cache, false);
    }

    public Image getImage(String name, String picture, boolean cache, boolean removeCache) {
        // I'm not all together happy with this implementation, but will have to do for a while
        Image image;
        try {
            if (name == null || name.isEmpty())
                name = picture;
            if (pictures.containsKey(name) && !removeCache)
                return pictures.get(name);
            else {
                image = tools.loadImage(this, picture);
                if (image == null) {
                    image = tools.loadImage(this, "/pics/notavailable.gif");
                    System.out.println("Image " + picture + " not available.");
                }
                pictures.remove(name);

                if (cache)
                    pictures.put(name, image);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            image = pictures.get("notavailable");
        }
        return image;
    }

    public void exit() {
        System.exit(0);
    }

    public void this_windowClosed(WindowEvent e) {
        System.exit(0);
    }

    public void this_mouseClicked(MouseEvent e) {
        try {
            // to prevent the user from double clicking on icons that generate
            // an animated action
            if (shouldDelayClick())
                Thread.sleep(1000);

            Drawables drawables = isModal() ? panel : objects;

            for (Drawable drawable : drawables)
                if (processDrawableClick(drawable, e)) {
                    return; // Found a clickable object, no need to continue
                }
    
            if (!isModal()) {
                drawables = hud;
                for (Drawable drawable : drawables)
                    if (processDrawableClick(drawable, e)) {
                        return; // Found a clickable object, no need to continue
                    }
                }
    
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void this_mouseMoved(MouseEvent e) {
        try {
            // this is for the flyover
            if (idle) {
                player.chooseFace();
                paintGlass();
                idle = false;
            }
    
            Drawables drawables = isModal() ? panel : objects;
            boolean repaintp = false;
    
            flyover = null;
    
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

            for (Drawable drawable : drawables)
                if (processDrawableMove(drawable, e)) {
                    repaintp = true;
                    break;
                }
    
            if (!repaintp && !isModal()) {
                drawables = hud;
                for (Drawable drawable : drawables) {
                    if (processDrawableMove(drawable, e)) {
                        repaintp = true;
                        break;
                    }
                }
            }
    
            if (repaintp) {
                if (flyover != null) {
                    flyover.x = (flyover.labelWidth * 2 > getWidth() - e.getX()) ? flyover.x - (flyover.labelWidth + 30) : flyover.x;
                    flyover.y = (40 > getHeight() - e.getY()) ? flyover.y - 40 : flyover.y;
                }
                repaint();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean shouldDelayClick() {
        return ("fields".equals(player.getPlace()) || "witchmountain".equals(player.getPlace()) || "hermitcave".equals(player.getPlace()));
    }
    
    private boolean processDrawableClick(Drawable drawable, MouseEvent e) {
        if (isClickable(drawable, e)) {
            String label = getDrawableLabel(drawable);
            logIfNecessary(label, drawable.action);
            evalAction(drawable.action);
            return true; // Click processed
        }
        return false; // Click not processed
    }
    
    private boolean processDrawableMove(Drawable drawable, MouseEvent e) {
        if (isClickable(drawable, e)) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            String label = getDrawableLabel(drawable);
            flyover = new Drawable(e.getX() + 10, e.getY() + 10, 300, 20, UIType.FLYOVER, label);
            return true;
        }
        return false;
    }
    
    private boolean isClickable(Drawable drawable, MouseEvent e) {
        return drawable.contains(e.getPoint()) && drawable.action != null;
    }
    
    private void logIfNecessary(String label, String action) {
        if (!nonLoggables.contains(label)) {
            player.addLog(label, action, (player.getDay() + player.getMonth() * 30 + player.getYear() * 360));
        }
    }
    
    private void evalAction(String action) {
        try {
            bsh.eval(bshimport + action);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private String getDrawableLabel(Drawable drawable) {
        if (drawable.tooltip == null && drawable.label == null) return drawable.action;
        else if (drawable.tooltip != null) return drawable.tooltip;
        else return drawable.label;
    }

    public void refresh() {
        // FIX 10.04.2005 - the journal.refresh() is required when loading game 
        player.getJournal().refresh();
    }

    // This method is called for travelling and others routines
    public void createTask(String action, int duration, int delay) {
        createTask(action, duration, "", delay);
    }

    // This method is called for animation routines
    public void createTask(String action, int duration, String doAfter, int delay) {
        task = new Task(action, duration, doAfter, delay);
        task.start();
    }
}
