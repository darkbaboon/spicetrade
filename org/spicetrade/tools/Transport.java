package org.spicetrade.tools;

public enum Transport {
    WALK("walk"),
    HORSE("horse"),
    CARAVAN("caravan"),
    SULEIMAN("suleiman"),
    BOAT("boat"),
    BORAK("borak"),
    DOG("dog");

    private final String name;

    Transport(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}