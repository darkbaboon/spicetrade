/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import bsh.Interpreter;
import org.spicetrade.Mainframe;

public class Spicebean {

    public String condition;
    public String name;
    public String action;
    public String picture;
    public int x;
    public int y;
    public int width;
    public int height;

    public Spicebean() {
        condition = null;
        name = null;
        action = null;
        picture = null;
        x = 0;
        y = 0;
        width = 0;
        height = 0;
    }

    public void ingrain() {
        if (isDoable())
            try {
                Interpreter bsh = new Interpreter();
                bsh.set("frame", Mainframe.me);
                bsh.eval(Mainframe.me.bshimport + action);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    }

    public String getAction() {
        return action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addName(String name) {
        this.name += ", " + name;
    }

    public String getPicture() {
        return picture;
    }

    public boolean isActive() {
        switch (condition) {
            case "result=false;":
                return false;
            case "result=true;":
                return true;
            default:
                break;
        }
        try {
            Interpreter bsh = new Interpreter();
            bsh.set("frame", Mainframe.me);
            bsh.set("result", false);
            bsh.eval(Mainframe.me.bshimport + condition);
            return getBoolean((Boolean) bsh.get("result"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean isDoable() {
        return (action != null && !action.isEmpty());
    }

    private boolean getBoolean(Boolean b) {
        return b;
    }

//    public Image getImage() {
//        if (picture != null)
//            try {
//                Image i = Mainframe.me.tools.loadImage(Mainframe.me, picture);
//                return i;
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        return null;
//    }
}