/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import org.spicetrade.Mainframe;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;

public class Sound implements Runnable {

    private static final int EXTERNAL_BUFFER_SIZE = 128000;
    private Thread thread = null;
    public String filename = "";
    public boolean playing = false;
    AudioInputStream audioInputStream = null;
    AudioInputStream targetStream = null;
    AudioFormat sourceFormat = null;
    AudioFormat targetFormat = null;
    SourceDataLine line = null;
    byte[] audioData = new byte[EXTERNAL_BUFFER_SIZE];
    boolean loop = false;

    public void start(String afile) {
        start(afile, false);
    }

    public void start(String afile, boolean loop) {
        filename = afile;

        if (thread == null) {
            thread = new Thread(this, (filename + System.currentTimeMillis()));
            this.loop = loop;
            thread.start();
        }
    }
            
    public void run() {
        Thread myThread = Thread.currentThread();
        while (thread == myThread) {
            try {
                URL url = getClass().getResource(filename);
                audioInputStream = AudioSystem.getAudioInputStream(url.openStream());

                if (audioInputStream == null) {
                    System.out.println("Cannot play sound: " + filename);
                    return;
                }
                sourceFormat = audioInputStream.getFormat();
                targetFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, sourceFormat.getSampleRate(),
                16, sourceFormat.getChannels(), sourceFormat.getChannels() * 2, sourceFormat.getSampleRate(), false);
                // get AudioInputStream that will be decoded by underlying VorbisSPI
                targetStream = AudioSystem.getAudioInputStream(targetFormat, audioInputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // get a line from a mixer in the system with the wanted format
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, targetFormat);
            try (SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);) {
                if (line != null) {
                    line.open(targetFormat);
                    try {
                        line.start();
                        do {
                            playing = true;
                            int nBytesRead = 0;
                            while (nBytesRead != -1) {
                                nBytesRead = targetStream.read(audioData, 0, audioData.length);
                                if (nBytesRead != -1) line.write(audioData, 0, nBytesRead);
                            }
                        } while (this.loop);
                        // prevent certain short sounds to be abruptly cut
                        Thread.sleep(400);
                        stop();
                    } catch (IOException | InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            } catch (LineUnavailableException lue) {
                Mainframe frame = Mainframe.me;
                frame.jukebox.setMusicOn(false);
            }
        }
    }

    public void stop() {
        try {
            playing = false;
            loop = false;
            thread = null;

            if (line != null) {
                line.drain();
                line.close();
            }

            if (audioInputStream != null) {
                audioInputStream.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
