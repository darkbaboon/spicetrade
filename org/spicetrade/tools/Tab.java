package org.spicetrade.tools;

public enum Tab {
    STATUS("Status"),
    INVENTORY("Inventory"),
    DIARY("Diary"),
    HELP("Help"),
    LOG("Log"),
    CULTURE("Culture"),
    DEATH("DEATH!");

    private final String name;
 
    Tab(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}