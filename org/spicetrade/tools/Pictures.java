package org.spicetrade.tools;

import java.awt.Image;
import java.util.HashMap;

public class Pictures {

    public static Pictures instance;
    private HashMap<String, Image> scrapbook;

    public Pictures() {
        scrapbook = new HashMap<>();
    }

    public static Pictures getInstance() {
        if (instance == null) {
            instance = new Pictures();
        }
        return instance;
    }

    public boolean containsKey(String key) {
        return scrapbook.containsKey(key);
    }

    public Image get(String key) {
        return scrapbook.get(key);
    }

    public void put(String key, Image value) {
        scrapbook.put(key, value);
    }

    public void remove(String key) {
        scrapbook.remove(key);
    }

    public void clear() {
        scrapbook.clear();
    }
}

