package org.spicetrade.tools;

public enum UIType {
    STATUS, BUTTON, LABEL, IMAGE, PANEL, FLYOVER, TIMAGE, LINE, CIRCLE, RECTANGLE, MPANEL, BPANEL
}
