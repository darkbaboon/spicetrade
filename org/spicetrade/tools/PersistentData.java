/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import java.awt.FileDialog;

import org.spicetrade.Audio;
import org.spicetrade.Mainframe;
import org.spicetrade.Player;

import com.thoughtworks.xstream.XStream;

public class PersistentData {
    private XStream xstream;
    public Mainframe mainframe;
    public Audio jukebox;
    public Player player;
    private Tools tools;

    public PersistentData(Mainframe mainframe, Audio jukebox, Tools tools) {
        this.mainframe = mainframe;
        this.jukebox = jukebox;
        this.tools = tools;
        this.xstream = new XStream();
    }

    public void saveGame(Player player) {
        try {
            String save = xstream.toXML(player);
            FileDialog fd = new FileDialog(mainframe, "Save game", FileDialog.SAVE);
            fd.setDirectory(".");
            fd.setFile("spicetrade.sav");
            fd.setVisible(true);
            String file = fd.getDirectory() + fd.getFile();
            tools.writeFile(file, save);
            jukebox.playSound("/music/fx_signal_bell_hitlink.ogg");
            tools.showMessage(mainframe, "Game saved!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveSettings(Settings settings) {
        try {
            String save = xstream.toXML(settings);
            tools.writeFile("spicetrade.properties", save);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Settings loadSettings() {
        try {
            String load = tools.readFile("spicetrade.properties", false);
            Settings settings = new Settings();
            if (load != null && !load.isEmpty())
                settings = (Settings) xstream.fromXML(load);
            return settings;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new Settings();
        }
    }

    public Player loadGame() {
        try {
            jukebox.stopAll();
            FileDialog fd = new FileDialog(mainframe, "Load game", FileDialog.LOAD);
            fd.setDirectory(".");
            fd.setFile("spicetrade.sav");
            fd.setVisible(true);
            String file = fd.getDirectory() + fd.getFile();
            if (fd.getFile() == null) return null;
            String load = tools.readFile(file, false);
            jukebox.playSound("/music/fx_signal_bell_hitlink.ogg");
            Player player = (Player) xstream.fromXML(load);
            mainframe.setShowOptions(true);
            if (player.hasAnyItems("10620", "10630"))
                mainframe.setShowMapGlobe(true);
            player.chooseFace();
            mainframe.refresh();
            Thread.sleep(50);
            mainframe.gotoPlace(player.getPlace());
            return player;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
