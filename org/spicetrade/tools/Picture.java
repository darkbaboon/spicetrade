package org.spicetrade.tools;

import java.awt.Image;
import java.io.IOException;

public class Picture {

    private static final String NOT_AVAILABLE_IMAGE_PATH = "/pics/notavailable.gif";

    public Picture() {
    }

    public Image getPicture(String path) {
        return getPicture("", path, false);
    }

    public Image getPicture(String name, boolean cache) {
        return getPicture(name, "", cache);
    }

    public Image getPicture(String name, String path, boolean cache) {
        return getPicture(name, path, cache, false);
    }

    public Image getPicture(String name, String path, boolean cache, boolean removeCache) {
        Image image;
        try {
            if (name == null || name.isEmpty())
                name = path;
            if (Pictures.getInstance().containsKey(name) && !removeCache)
                return Pictures.getInstance().get(name);
            else {
                image = loadImage(path);
                if (image == null) {
                    image = loadImage(NOT_AVAILABLE_IMAGE_PATH);
                    System.out.println("Image " + name + " not available.");
                }
                Pictures.getInstance().remove(name);

                if (cache)
                    Pictures.getInstance().put(name, image);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            image = Pictures.getInstance().get("notavailable");
        }
        return image;
    }

    private Image loadImage(String path) {
        try {
            if (getClass().getResource(path) != null)
                return javax.imageio.ImageIO.read(getClass().getResource(path));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
