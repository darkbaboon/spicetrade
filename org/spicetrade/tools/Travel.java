/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import bsh.Interpreter;
import org.spicetrade.Mainframe;

public class Travel {

    public Transport transport;
    public String name = "";
    public String nice = "";
    public String description = "";
    public String condition = "";
    public boolean european;
    public int x;
    public int y;
    public int width = 20;
    public int height = 20;
    public int duration1; // walking
    public int duration2; // riding
    public int duration3; // caravanning
    public int duration4; // flying
    public int duration5; // sailing
    public String terrestrial = "";
    public String airborne = "";
    public String nautical = "";

    public String getDescription(Transport transport) {
        return "Travel to " + nice + "§" + "This will take you " + getTravelDuration(transport) + " days.";
    }

    public int getTravelDuration(Transport transport) {
        switch (transport) {
            case WALK:
                return duration1;
            case HORSE:
                return duration2;
            case CARAVAN:
                return duration3;
            case SULEIMAN:
            case BORAK:
            case DOG:
                return duration4;
            case BOAT:
                return duration5;
            default:
                return 0;
        }
    }

    public String getAction(Transport transport) {
        String action = "";

        int duration = getTravelDuration(transport);
        switch (transport) {
            case WALK:
            case HORSE:
            case CARAVAN:
                action = terrestrial;
                break;
            case SULEIMAN:
            case BORAK:
            case DOG:
                action = airborne;
                break;
            case BOAT:
                action = nautical;
                break;
            default:
                action = "";
        }

        action = action.replaceAll("!transport", transport.getName());
        action = action.replaceAll("!duration", String.valueOf(duration * 2));

        return action;
    }

    public boolean isActive(Interpreter bsh) {
        try {
            bsh.eval(Mainframe.me.bshimport + condition);
            return getBoolean((Boolean) bsh.get("result"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private boolean getBoolean(Boolean b) {
        return b;
    }

    public String toString() {
        return nice + " (" + x + ", " + y + ") - feet: " + duration1 + ", horse: " + duration2 + ", caravan: " + duration3 + ", flying: " + duration4 + ", boat: " + duration5;
    }
}