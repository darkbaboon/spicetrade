/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import org.spicetrade.Mainframe;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class Tools {

    public void showMessage(Frame frame, String message) {
        try {
            JOptionPane.showMessageDialog(frame, message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean askYesNo(Frame frame, String message) {
        try {
            int selected = JOptionPane.showConfirmDialog(Mainframe.me, message, "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            return (selected == JOptionPane.YES_OPTION) ? true : false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Image loadImage(Frame frame, String picture) {
        try {
            if (getClass().getResource(picture) != null) return ImageIO.read(getClass().getResource(picture));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String readFile(String file) {
        return readFile(file, true);
    }

    public String readFile(String file, boolean injar) {
        StringBuilder ret = new StringBuilder();
        try {
            String line;
            InputStreamReader r;
            if (injar)
                r = new InputStreamReader(getClass().getResourceAsStream(file), StandardCharsets.UTF_8);
            else
                r = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);

            BufferedReader br = new BufferedReader(r);

            while ((line = br.readLine()) != null)
                ret.append(line);

            br.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ret.toString();
    }

    public void writeFile(String file, String string) {
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
                writer.write(string);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}