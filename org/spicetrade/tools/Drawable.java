/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import org.spicetrade.Mainframe;

import java.awt.*;
import java.util.ArrayList;

public class Drawable extends Rectangle {

    public UIType type;
    public int x;
    public int y;
    public int x2;
    public int y2;
    public int width;
    public int height;
    public int xarc;
    public int yarc;
    public float alpha;
    public Font font;
    public Color color;
    public Image image;
    public Image image2;
    public String label;
    public ArrayList<Object> plainText;
    public int labelWidth;
    public String action;
    public String tooltip;

    public Drawable(int x, int y, int x2, int y2, UIType type, Color color, float alpha) {
        // Lines with color and alpha
        super(0, 0, 0, 0);
        this.type = type;
        this.x = x;
        this.y = y;
        this.x2 = x2;
        this.y2 = y2;
        this.color = color;
        this.alpha = alpha;
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image) {
        // Images
        this(x, y, width, height, type, image, null, null, null, null, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image, float alpha) {
        // Transparent images
        this(x, y, width, height, type, image, null, null, null, null, null, null, alpha);
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image, String label) {
        // Panels
        this(x, y, width, height, type, image, null, label, null, null, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image, String label, String action) {
        // MPanels and clickable images
        this(x, y, width, height, type, image, null, label, null, action, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image, Image image2, String label) {
        // Status dialog
        this(x, y, width, height, type, image, image2, label, null, null, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image, Image image2, String label, Color color) {
        // Status dialog
        this(x, y, width, height, type, image, image2, label, null, null, null, color, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image, Image image2, String label, String action) {
        // Status bar
        this(x, y, width, height, type, image, image2, label, null, action, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, String label) {
        // Label
        this(x, y, width, height, type, null, null, label, null, null, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, String label, Font font, Color color) {
        // Label with font and color
        this(x, y, width, height, type, null, null, label, null, null, font, color, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, String label, String action, Font font, Color color) {
        // Clickable label with font and color
        this(x, y, width, height, type, null, null, label, null, action, font, color, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, String label, String tooltip, String action, Font font, Color color) {
        // Clickable label with font, color and tooltip
        this(x, y, width, height, type, null, null, label, tooltip, action, font, color, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, String label, String action) {
        // Clickable label
        this(x, y, width, height, type, null, null, label, null, action, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, String label, String tooltip, String action) {
        // Clickable label
        this(x, y, width, height, type, null, null, label, tooltip, action, null, null, 0);
    }

    public Drawable(int x, int y, int width, int height, UIType type, Image image, Image image2, String label, String tooltip, String action, Font font, Color color, float alpha) {
        super(x, y, width, height);
        this.type = type;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.image2 = image2;
        this.label = label;
        this.tooltip = tooltip;
        this.action = action;
        this.alpha = alpha;
        this.font = font;
        this.color = color;
        if (label != null) {
            Graphics2D g2d = (Graphics2D) Mainframe.me.getGraphics();
            font = (font == null) ? Mainframe.me.font : font;
            g2d.setFont(font);
            plainText = formatText(label, width, g2d);
            labelWidth = (Integer) plainText.get(0);
        }
    }

    public void draw(Graphics g) {
        try {
            Mainframe frame = Mainframe.me;
            Graphics2D g2d = (Graphics2D) g;
            Stroke s = g2d.getStroke();
            Composite old = g2d.getComposite();
            FontMetrics fm = g2d.getFontMetrics();
            int i;
            int _y;
            int increment;
            g.setColor(Color.white);
            switch (type) {
            default:
                break;
                case STATUS:
                    int ty = y + 149;
                    int iy = ty - 12;
                    int maxbar = frame.getHeight() - y - 35;
                    int bar;
                    g.drawImage(image2, 100, y + 137, frame);
                    g.drawImage(image, 20, y + 2, frame);
                    g.setColor(Color.black);

                    // age
                    g.setColor(Color.magenta);
                    g.setFont(new Font(frame.typeface, 1, 12));
                    //g.drawString(frame.player.getDay() + "." + frame.player.getMonth() + "." + frame.player.getAge(), 390, ty);
                    g.setColor(Color.black);
                    g.drawString(frame.player.getNiceCity(), 1000 - fm.stringWidth(frame.player.getNiceCity()), ty);
                    g.setFont(frame.font);
                    g2d.setComposite(AlphaComposite.getInstance(3, 0.54F));
                    g.setColor(Color.white);
                    g2d.fillRect(375, iy - frame.player.getAge(), 30, frame.player.getAge());
                    g.setColor(Color.white);

                    // health
                    bar = Math.min(maxbar, (int) frame.player.getHealth());
                    if (bar > 0) g2d.fillRect(475, iy - bar, 30, bar);

                    // money
                    bar = Math.min(maxbar, frame.player.getMoney() / 100);
                    if (bar > 0) g2d.fillRect(585, iy - bar, 30, bar);

                    // culture
                    bar = Math.min(maxbar, frame.player.getCulture());
                    if (bar > 0) g2d.fillRect(705, iy - bar, 30, bar);

                    //                // wives
                    //                bar = Math.min(maxbar, frame.player.wives * 10);
                    //                if (bar > 0) g2d.fillRect(812, iy - bar, 30, bar);
                    //
                    //                // children
                    //                bar = Math.min(maxbar, frame.player.children * 5);
                    //                if (bar > 0) g2d.fillRect(933, iy - bar, 30, bar);

                    g2d.setComposite(old);
                    break;

                case BUTTON:
                    g2d.setComposite(AlphaComposite.getInstance(3, 0.74F));
                    g.setColor(Color.gray);
                    g2d.fillRoundRect(x + 4, y + 4, width, height, xarc, yarc);
                    g.setColor(Color.white);
                    if (!action.contains("gotoPlace"))
                        g.setColor(Color.red);
                    g2d.fillRoundRect(x, y, width, height, xarc, yarc);
                    g.setColor(Color.black);
                    g2d.drawRoundRect(x, y, width, height, 15, 15);
                    g.drawString(label, x + 10, y + 20);
                    g2d.setComposite(old);
                    break;

                case LABEL:
                    increment = (font != null && font.getSize() == 10) ? 11 : 18;

                    _y = y + increment;
                    g.setColor(Color.black);
                    if (action != null)
                        g.setFont(frame.fontBold);
                    if (font != null) {
                        g.setFont(font);
                        g.setColor(color);
                    }
                    i = 1;
                    for (int j = plainText.size(); i < j; i++) {
                        g.drawString((String) plainText.get(i), x, _y);
                        _y += increment;
                    }
                    g.setFont(frame.font);
                    break;

                case IMAGE:
                    g.drawImage(image, x, y, frame);
                    break;

                case PANEL:
                    color = (color == null) ? Color.white : color;
                    alpha = (alpha == 0) ? 0.84F : alpha;
                    g.setColor(color);
                    g2d.setComposite(AlphaComposite.getInstance(3, alpha));
                    g2d.fillRect(x, y, width, height);
                    g.setColor(Color.black);
                    g2d.setComposite(old);
                    g.setColor(Color.white);
                    if (image != null)
                        g.drawImage(image, x + 25, y + 25, frame);
                    if (image2 != null)
                        g.drawImage(image2, x + 260, y + 25, frame);
                    break;

                case FLYOVER:
                    _y = y + 12;
                    if (label != null) {
                        g.setColor(Color.white);
                        g2d.setComposite(AlphaComposite.getInstance(3, 0.64F));
                        g2d.fillRect(x, y, (Integer) plainText.get(0) + 5, height * (plainText.size() - 1));
                        g.setColor(Color.black);
                        g2d.setComposite(old);
                        i = 1;
                        for (int j = plainText.size(); i < j; i++) {
                            g.drawString((String) plainText.get(i), x + 2, _y + 2);
                            _y += 18;
                        }

                    }
                    g.setColor(Color.white);
                    break;

                case TIMAGE: // transparent image
                    g2d.setComposite(AlphaComposite.getInstance(3, alpha));
                    g.drawImage(image, x, y, frame);
                    g2d.setComposite(old);
                    break;

                case LINE:
                    color = (color == null) ? Color.red : color;
                    alpha = (alpha == 0) ? 7.0F : alpha;
                    g.setColor(color);
                    g2d.setStroke(new BasicStroke(alpha));
                    g2d.drawLine(x, y, x2, y2);
                    g2d.setStroke(s);
                    break;

                case CIRCLE:
                    color = (color == null) ? Color.red : color;
                    g.setColor(color);
                    g2d.setStroke(new BasicStroke(5.0F));
                    g2d.drawOval(x, y, width, height);
                    g2d.setStroke(s);
                    break;

                case RECTANGLE:
                    color = (color == null) ? Color.red : color;
                    g.setColor(color);
                    g.fillRect(x, y, width, height);
                    break;

                case MPANEL:
                    if (label != null) {
                        g.setColor(Color.white);
                        g2d.setComposite(AlphaComposite.getInstance(3, 0.84F));
                        g2d.fillRect(x, y, width, height);
                        g.setColor(Color.black);
                        g2d.setComposite(old);
                        i = 1;
                        _y = y;
                        String t;
                        for (int j = plainText.size(); i < j; i++) {
                            t = (String) plainText.get(i);
                            g.drawString(t, (x + width / 2) - (stringSize(t, g2d) / 2), (_y + height) - ((plainText.size()) * 18));
                            _y += 18;
                        }
                        g.setColor(Color.white);
                    }

                    if (image2 != null) {
                        g2d.setComposite(AlphaComposite.getInstance(3, ((float) 1 - alpha)));
                        g.drawImage(image, (x + width / 2) - 75, (y + height / 2) - image.getHeight(frame) / 2 - 20, frame);
                        g2d.setComposite(AlphaComposite.getInstance(3, alpha));
                        g.drawImage(image2, (x + width / 2) - 75, (y + height / 2) - image2.getHeight(frame) / 2 - 20, frame);
                        g2d.setComposite(old);
                    } else
                        g.drawImage(image, (x + width / 2) - 75, (y + height / 2) - image.getHeight(frame) / 2 - 20, frame);

                    break;
  
                case BPANEL: // battle and shop panel
                    g.setColor(Color.white);
                    g2d.setComposite(AlphaComposite.getInstance(3, 0.84F));
                    g2d.fillRect(x, y, width, height);
                    g.setColor(Color.black);
                    g2d.setComposite(old);
                    g.setColor(Color.black);
                    g.drawLine(x + (width / 2), y + 50, x + (width / 2), y + height - 80);
                    g.drawImage(image, x + 206, y + 25, frame);
                    g.drawString(label, x + 240, y + height - 42);
                    g.setColor(Color.white);

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private ArrayList<Object> formatText(String label, int labelWidth, Graphics2D g2d) {
        ArrayList<Object> result = new ArrayList<Object>();
        StringBuilder line = new StringBuilder();
        int lineWidth = 0;
        
        for (int index = 0; index < label.length(); index++) {
            char firstChar = label.charAt(0);
            char currentChar = label.charAt(index);
            
            if (stringSize(line.toString(), g2d) > labelWidth || currentChar == '§') {
                for (int i = line.length() - 1; i > 0; i--) {
                    if (currentChar == '§') break;
                    index--;
                    firstChar = line.charAt(0);
                    currentChar = line.charAt(i);
                    if (isControlChar(currentChar) || firstChar == '/')
                       break;
                    line.setLength(line.length() - 1);
                }
                lineWidth = Math.max(lineWidth, stringSize(line.toString(), g2d));
                result.add(line.toString());
                line.setLength(0);
            } else if (currentChar != '§') line.append(currentChar);
        }
        lineWidth = Math.max(lineWidth, stringSize(line.toString(), g2d));
        result.add(line.toString());
        result.add(0, lineWidth);
        
        return result;
    }

    private boolean isControlChar(char candidate) {
        char[] controlChars = new char[]{' ','\n',',','.','@'};
        for(char controlChar : controlChars) {
            if (candidate == controlChar)
                return true;
        }
        return false;   
    }

    private int stringSize(String line, Graphics2D g2d) {
        FontMetrics fontMetrics = g2d.getFontMetrics();
        return fontMetrics.stringWidth(line);
    }
}