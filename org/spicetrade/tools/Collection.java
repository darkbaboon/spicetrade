/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.*;

import org.spicetrade.Mainframe;

import java.util.*;

public class Collection {

    private transient HashMap<String, Object> collection;

    protected HashMap<String, Object> getCollection() {
        return collection;
    }

    public Collection(String filePath) {
        readXML(filePath);
    }

    public void refresh(String filePath) {
        readXML(filePath);
    }

    @SuppressWarnings("unchecked")
    public void readXML(String filePath) {
        collection = new HashMap<String, Object>();
        String xml = null;
        XStream xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY);
        xstream.addPermission(NullPermission.NULL);
        xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
        try {
            xml = Mainframe.me.tools.readFile(filePath);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        collection = (HashMap<String, Object>) xstream.fromXML(xml);
    }

    public ArrayList<Spicebean> getList(String key1, String key2) {
        ArrayList<Spicebean> list = new ArrayList<>();
        Iterable<Object> objects = getObjectList(key1, key2);
        for (Object object : objects) {
            if (object instanceof Spicebean) {
                list.add((Spicebean) object);
            }
        }
        return list;
    }

    public ArrayList<Object> getObjectList(String key) {
        ArrayList<Object> result = new ArrayList<>();
        try {
            Object object = collection.get(key);
            if (object instanceof ArrayList<?>) {
                ArrayList<?> list = (ArrayList<?>) object;
                for (Object item : list) {
                    if (item instanceof Travel) {
                        if (((Travel) item).isActive(Mainframe.me.bsh)) {
                            result.add(item);
                        }
                    } else {
                        result.add(item);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public ArrayList<Object> getObjectList(String key1, String key2) {
        ArrayList<Object> result = new ArrayList<>();
        try {
            HashMap<?, ?> subHashMap = (HashMap<?, ?>) collection.get(key1);
            ArrayList<?> list = (ArrayList<?>) subHashMap.get(key2);
            for (Object item : list) {
                if (item instanceof Spicebean) {
                    if (((Spicebean) item).isActive()) {
                        result.add(item);
                    }
                } else if (item instanceof String) {
                    result.add(item);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getString(String key1, String key2) {
        try {
            HashMap<?, ?> subHashMap = (HashMap<?, ?>) collection.get(key1);
            return (String) subHashMap.get(key2);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public int getInt(String key1, String key2) {
        int result = 0;
        try {
            HashMap<?, ?> subHashMap = (HashMap<?, ?>) collection.get(key1);
            String value = (String) subHashMap.get(key2);
            result = Integer.parseInt(value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public double getDouble(String key1, String key2) {
        double result = 0;
        try {
            HashMap<?, ?> subHashMap = (HashMap<?, ?>) collection.get(key1);
            String value = (String) subHashMap.get(key2);
            result = Double.parseDouble(value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public Object getObject(String key) {
        try {
            return collection.get(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
