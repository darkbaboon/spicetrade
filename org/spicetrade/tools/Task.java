/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.tools;

import bsh.Interpreter;
import org.spicetrade.Mainframe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.Timer;

public class Task implements ActionListener {

    private String action;
    private int duration;
    private int counter = 1;
    private Interpreter bshint = new Interpreter();
    private Timer timer;
    private String doAfter;
    private boolean paused;
    private HashMap<String, String> attr = new HashMap<>();
    final Mainframe frame;

    public Task(String action, int duration, String doAfter, int delay) {
        frame = Mainframe.me;
        this.action = action;
        this.duration = duration;
        this.doAfter = doAfter;

        // Create a Timer with this ActionListener
        this.timer = new Timer(delay, this);
    }

    public void add(String n) {
        add(n, "");
    }

    public void add(String n, String v) {
        attr.put(n, v);
    }

    public int getInt(String n) {
        try {
            return Integer.parseInt(attr.getOrDefault(n, "0"));
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public void start() {
        paused = false;
        timer.start();
    }

    public void actionPerformed(ActionEvent e) {
        if (!paused) {
            String a = action.replaceAll("!counter", String.valueOf(counter));
            counter++;
            duration--;

            try {
                if (duration < 1) {
                    frame.player.chooseFace();
                    a = a.replaceAll("!donep", "true");
                    cancel();
                    bshint.set("frame", frame);
                    bshint.eval(frame.bshimport + a);

                    if (doAfter != null && !doAfter.isEmpty()) {
                        bshint.set("frame", frame);
                        bshint.eval(frame.bshimport + doAfter);
                    }
                } else {
                    a = a.replaceAll("!donep", "false");
                    bshint.set("frame", frame);
                    bshint.eval(frame.bshimport + a);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void pause() {
        paused = true;
    }

    public void cancel() {
        timer.stop();
    }
}
