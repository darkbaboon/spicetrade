package org.spicetrade.tools;

import java.awt.Color;

public enum Terrain {
    LAND(Color.red),
    SEA(Color.blue),
    SKY(Color.white);

    private final Color color;

    Terrain(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}