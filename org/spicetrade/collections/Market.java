/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.collections;

import org.spicetrade.tools.Collection;
import org.spicetrade.tools.Inventory;
import org.spicetrade.tools.Item;

import java.util.Iterator;

public class Market extends Collection {

    public static final String xml = "/data/items.xml";

    public Market() {
        super(xml);
    }

    public void refresh() {
        refresh(xml);
    }

    public Item getItem(String id) {
        Item item = null;
        try {
            item = (Item) getObject(id);
            return item;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public int getPrice(String id) {
        try {
            Item i = getItem(id);
            return i.price;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public Inventory getHelp(String firstLetter) {
        Inventory help = new Inventory();
        try {
            Iterator<String> iterator = getCollection().keySet().iterator();
            while (iterator.hasNext()) {
                Item item = getItem(iterator.next());
                if (item.getId().length() == 4)
                    if (item.getName().substring(0, 1).equalsIgnoreCase(firstLetter))
                        help.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return help;
    }
}
