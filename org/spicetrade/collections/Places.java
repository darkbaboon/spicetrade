/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.collections;

import bsh.Interpreter;

import org.spicetrade.Mainframe;
import org.spicetrade.tools.Collection;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Drawables;
import org.spicetrade.tools.Spicebean;

import java.util.ArrayList;

public class Places extends Collection {

    public static final String xml = "/data/places.xml";

    public Places() {
        super(xml);
    }

    public void refresh() {
        refresh(xml);
    }

    public String getText(String place) {
        return getString(place, "Text");
    }

    public String getTitle(String place) {
        return getString(place, "Name");
    }

    public ArrayList<Spicebean> getActions(String place) {
        return getList(place, "Actions");
    }

    public ArrayList<Spicebean> getPictures(String place) {
        return getList(place, "Pictures");
    }

    public ArrayList<Spicebean> getEvents(String place) {
        return getList(place, "Events");
    }

    public String getBackground(String place) {
        try {
            ArrayList<Spicebean> spicebeans = getList(place, "Backgrounds");
            if (spicebeans != null && !spicebeans.isEmpty()) {
                for (Spicebean spicebean : spicebeans) {
                    if (spicebean.isActive()) {
                        return spicebean.picture;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }
    

    public Drawables getDrawables(String place) {
        ArrayList<Spicebean> spicebeans = getList(place, "Drawables");
        Drawables drawables = new Drawables();
        if (spicebeans.isEmpty()) return drawables;
        try {
            Interpreter bsh = new Interpreter();
            bsh.set("frame", Mainframe.me);

            for (Spicebean spicebean : spicebeans) {
                bsh.eval(Mainframe.me.bshimport + spicebean.action);
                Drawable drawable = (Drawable) bsh.get("drawable");
                if (drawable != null)
                    drawables.add(drawable);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return drawables;
    }
}