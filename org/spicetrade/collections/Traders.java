/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.collections;

import java.util.Arrays;

import org.spicetrade.Mainframe;
import org.spicetrade.tools.Collection;
import org.spicetrade.tools.Inventory;
import org.spicetrade.tools.Item;

public class Traders extends Collection {

    public static final String xml = "/data/traders.xml";
    final Mainframe frame;

    public Traders() {
        super(xml);
        frame = Mainframe.me;
    }

    public void refresh() {
        refresh(xml);
    }

    public String getName(String name) {
        return getString(name, "Name");
    }

    public String getDisplay(String name) {
        return getString(name, "Display");
    }

    public int getDescription(String description) {
        return getInt(description, "Description");
    }

    public double getBuyFactor(String factor) {
        return getDouble(factor, "Buy factor");
    }

    public double getSellFactor(String factor) {
        return getDouble(factor, "Sell factor");
    }

    public int getSellingPrice(String who, String id) {
        Item item = frame.market.getItem(id);
        double sellFactor = getSellFactor(who);
        return (frame.player.getDifficulty() == 0) ? (int) (item.getPrice() * sellFactor * 1.15) : (int) (item.getPrice() * sellFactor);
    }

    public int getPurchasePrice(String who, String id) {
        Item item = frame.market.getItem(id);
        double buyFactor = getBuyFactor(who);
        return (frame.player.getDifficulty() == 0) ? (int) (item.getPrice() * buyFactor * 0.85) : (int) (item.getPrice() * buyFactor);
    }


    public Inventory getItemsToBuy(String who) {
        return getItems("Buy items", who, true);
    }

    public Inventory getItemsForSale(String who) {
        return getItems("Sell items", who, false);
    }

    public Inventory getItems(String what, String who, boolean trader) {
        Inventory inventory = new Inventory();
        Inventory items = getItems(who, what);
        String[] spices = {"10100", "10110", "10120", "10130", "10200", "10210", "10220", "10230"};
        try {
            for (Item item : items) {
                if (trader && (Arrays.asList(spices).contains(item.getId())))
                    inventory.add(item);
                else if (!trader && frame.player.hasItem(item.getId()) && !frame.player.hasSold(item.getId()))
                    inventory.add(item);
                else if (trader && !frame.player.hasItem(item.getId()) && !frame.player.hasSold(item.getId(), who))
                    inventory.add(item);
            }
            if (trader) {
                Inventory soldItems = frame.player.getSoldItems(who);
                for (Item soldItem : soldItems) {
                    inventory.add(soldItem);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return inventory;
    }

    public Inventory getItems(String who, String what) {
        Inventory inventory = new Inventory();
        try {
            Iterable<Object> objects = getObjectList(who, what);
            Item item;
            for (Object object : objects) {
                if (object instanceof String) {
                    String id = (String) object;
                    item = frame.market.getItem(id);
                    inventory.add(item);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return inventory;
    }
}