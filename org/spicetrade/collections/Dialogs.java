/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.collections;

import org.spicetrade.tools.Collection;
import org.spicetrade.tools.Spicebean;

import java.util.ArrayList;

public class Dialogs extends Collection {

    public static final String xml = "/data/dialogs.xml";

    public Dialogs() {
        super(xml);
    }

    public void refresh() {
        refresh(xml);
    }

    public String getTitle(String dialog) {
        return getString(dialog, "Name");
    }

    public String getText(String dialog) {
        return getString(dialog, "Text");
    }

    public ArrayList<Spicebean> getChoices(String dialog) {
        ArrayList<Spicebean> choices = new ArrayList<>();
        Iterable<Object> objects = getObjectList(dialog, "Choices");
        for (Object object : objects) {
            if (object instanceof Spicebean) {
                choices.add((Spicebean) object);
                }
        }
        return choices;
    }
}
