/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.collections;

import org.spicetrade.Mainframe;
import org.spicetrade.tools.Collection;

import java.util.HashMap;
import java.util.ArrayList;

public class Journal extends Collection {

    public static final String xml = "/data/journal.xml";
    final Mainframe frame;
    public HashMap<String, String> questsOpen;
    public HashMap<String, String> questsClosed;
    public HashMap<String, String> timestamp;
    // ADD 10.04.2005 -- Want to see the book of life events in the order they happen in
    public ArrayList<String> quests = new ArrayList<String>();

    public Journal() {
        super(xml);
        frame = Mainframe.me;
        questsOpen = new HashMap<>();
        questsClosed = new HashMap<>();
        timestamp = new HashMap<>();
    }

    public void refresh() {
        refresh(xml);
    }

    public void open(String quest) {
        open(quest, "");
    }

    public void open(String quest, String value) {
        if (!isDone(quest) && !has(quest)) {
            frame.jukebox.playSound("/music/fx_hit.ogg");
            questsOpen.put(quest, value);
            stamp(quest);
        }
    }

    public boolean contains(String quest, String value) {
        String compare = get(quest);
        return compare.contains(value);
    }

    public String get(String quest) {
        if (questsOpen.containsKey(quest))
            return questsOpen.get(quest);
        else if (questsClosed.containsKey(quest))
            return questsClosed.get(quest);
        else
            return "";
    }

    public boolean has(String quest) {
        return questsOpen.containsKey(quest);
    }

    public boolean hasAnyQuests(String quest1, String quest2, String quest3, String quest4) {
        String[] quests = new String[]{quest1, quest2, quest3, quest4};
        for (String quest : quests) {
            if (questsOpen.containsKey(quest))
                    return true;
        }
        return false;
    }

    public void done(String quest) {
        done(quest, "");
    }

    public void done(String quest, String value) {
        done(quest, value, false);
    }

    public void done(String quest, String value, boolean replace) {
        if (!isDone(quest)) {
            String newValue = value;
            if (!replace)
                newValue = get(quest) + value;
            frame.jukebox.playSound("/music/fx_famous_ending.ogg");
            // ADD 10.04.2005 -- Want the book of life happenings should be in the order they happen in
            quests.add(quest);
            questsClosed.put(quest, newValue);
            questsOpen.remove(quest);
            stamp(quest);
        }
    }

    public boolean isDone(String quest) {
        return questsClosed.containsKey(quest);
    }

    public void remove(String quest) {
        if (has(quest)) questsOpen.remove(quest);
    }

    public void add(String quest, String value) {
        put(quest, get(quest) + value);
    }

    public void put(String quest, String value) {
        // FIX 10.04.2005 -- redundant check for exact value in quest
        if (questsOpen.containsKey(quest)) {
            questsOpen.put(quest, value);
            stamp(quest);
        } else if (questsClosed.containsKey(quest)) {
            questsClosed.put(quest, value);
            stamp(quest);
        }
    }

    public String getPicture(String quest) {
        return getString(quest, "Picture");
    }

    public void stamp(String quest) {
        timestamp.put(quest, String.valueOf(frame.player.getDay() + (frame.player.getMonth() * 30) + (frame.player.getYear() * 360)));
    }
}