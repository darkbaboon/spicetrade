/* Spice Trade
 * Copyright (C) 2005-2024 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 * Author: Ronan Rabouin, darkbaboon@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade.collections;

import org.spicetrade.Mainframe;
import org.spicetrade.tools.Collection;
import org.spicetrade.tools.Drawable;
import org.spicetrade.tools.Spicebean;
import org.spicetrade.tools.Transport;
import org.spicetrade.tools.UIType;

import java.awt.*;
import java.util.ArrayList;

public class Actions extends Collection {

    public static final String xml = "/data/actions.xml";
    final Mainframe frame;

    public Actions() {
        super(xml);
        frame = Mainframe.me;
    }

    public void refresh() {
        refresh(xml);
    }

    public void process(String action, ArrayList<int[]> coords, int duration, String to, String nicecity, int i, int x, int y, boolean done) {
        try {
            // calculates the line across the map
            int size = coords.size() - 1;
            double pace = size > 1 ? (double) duration / size : duration;
            int where = 1;

            if (size > 1) {
                while (where <= size && i > Math.round(where * pace)) where++;
                where = Math.min(where, size);
            }

            int[] start = coords.get(where - 1);
            int[] end = size > 1 ? coords.get(where) : coords.get(size);
            
            // calculating the length of one step in this particular line
            double lineX = (start[0] + (((end[0] - start[0]) / pace) * (i - (where - 1) * pace)));
            double lineY = (start[1] + (((end[1] - start[1]) / pace) * (i - (where - 1) * pace)));

            // calculating the position of the Abu-icon in front of the line
            int[] offset = new int[]{0,0};
            double angle = Math.atan2(end[1] - start[1], end[0] - start[0]) + Math.PI;

            if (isBetween(angle, 0.25, 1.02)) offset = new int[]{-16, -14};
            else if (isBetween(angle, 1.02, 2.03)) offset = new int[]{-15, -15};
            else if (isBetween(angle, 2.03, 2.84)) offset = new int[]{-10, -20};
            else if (isBetween(angle, 2.84, 3.5)) offset = new int[]{-10, -12};
            else if (isBetween(angle, 3.5, 4.4)) offset = new int[]{-16, -14};
            else if (isBetween(angle, 4.4, 5.1)) offset = new int[]{-15, -15};
            else if (isBetween(angle, 5.1, 5.8)) offset = new int[]{-15, -13};
            else offset = new int[]{-20, -10};

            int iconX = (int) lineX + offset[0];
            int iconY = (int) lineY + offset[1];

            int startX = start[0];
            int startY = start[1];

            // just drawing the panel, no need to draw the glass or the map
            frame.setModal(true);
            frame.panel.clear();

            Color color = getColorForAction(action);

            if (where > 1) {
                for (int a = 0; a < where - 1; a++) {
                    int[] startPoint = coords.get(a);
                    int[] endPoint = coords.get(a + 1);
                    frame.objects.add(new Drawable(startPoint[0], startPoint[1], endPoint[0], endPoint[1], UIType.LINE, color, 0));
                }
            }

            // after all the previous lines (if there were any) were drawn, put in the current line
            frame.panel.add(new Drawable(startX, startY, (int) lineX, (int) lineY, UIType.LINE, color, 0));
            // and the icon
            frame.panel.add(new Drawable(iconX, iconY, 0, 0, UIType.IMAGE, frame.getImage("playerIcon")));

            // and of course the movement panel
            process(action, getString(action, "Text") + " to " + nicecity, i, x, y, 200, 300);
            if (i % 2 == 0) frame.player.nextDay(true);

            frame.doLayout();
            frame.repaint();

            if (done)
                // once we are done, move to the appropriate city etc..
                handleDoneAction(to, nicecity, end);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void process(String action, int i, int x, int y, boolean done) {
        process(action, i, x, y, done, 0);
    }

    public void process(String action, int i, int x, int y, boolean done, int after) {
        process(action, i, x, y, done, after, false);
    }

    public void process(String action, int i, int x, int y, boolean done, int after, boolean fade) {
        try {
            // Regular actions (like drowing something etc..
            frame.setModal(true);
            frame.panel.clear();
            process(action, getString(action, "Text"), i, x, y, fade);
            frame.doLayout();
            frame.repaint();
            if (done) {
                Thread.sleep(1000 + after);
                frame.setModal(false);
                frame.gotoPlace(frame.player.getPlace());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void process(String action, String label, int i, int x, int y) {
        process(action, label, i, x, y, false);
    }

    public void process(String action, String label, int i, int x, int y, boolean fade) {
        process(action, label, i, x, y, 200, 400, fade);
    }

    public void process(String action, String label, int i, int x, int y, int width, int height) {
        process(action, label, i, x, y, width, height, false);
    }

    public void process(String action, String label, int i, int x, int y, int width, int height, boolean fade) {
        // This is the animation panel logic
        frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        ArrayList<Image> animation = new ArrayList<>();
        ArrayList<String> pictures = new ArrayList<>();
        Iterable<Object> objects = getObjectList(action, "Animation");
        for(Object object : objects) {
            if (object instanceof String) {
                pictures.add((String) object);
            }
        }
        // loop through the pictures, one by one
        for (String picture : pictures) {
            animation.add(frame.tools.loadImage(frame, picture));
        }
        // fade from the first picture to the second picture, used when a church turn into mosquee, etc..
        if (fade) {
            frame.panel.add(new Drawable(x, y, width, height, UIType.MPANEL, animation.get(0), animation.get(1), label, "", null, null, null, ((float) (i * 5) / 100)));
        } else {
            // other kind of action, like panel or growing animations
            int act = (i % animation.size() <= (animation.size() - 1)) ? i % animation.size() : 0;
            UIType type = (label != null && !label.isEmpty()) ? UIType.MPANEL : UIType.IMAGE;
            frame.panel.add(new Drawable(x, y, width, height, type, animation.get(act), label, ""));
        }
        // get the events for the action and see if one of them would like to be triggered
        // again, this "event" mechanism is too simple.. will see if I have the energy to refactor these to a more logical
        // structure
        Iterable<Spicebean> events = getList(action, "Events");
        for (Spicebean event : events) {
            event.ingrain();
            break;
        }
    }

    private static boolean isBetween(double angle, double lower, double upper) {
        return lower <= angle && angle < upper;
    }

    private Color getColorForAction(String action) {
        // red is for land travel, cyan for ocean and white for flying
        Color color = Color.red;
        if (action.equals(Transport.BOAT.getName()))
            color = Color.cyan;
        else if (action.equals(Transport.SULEIMAN.getName()) || action.equals(Transport.BORAK.getName())
                || action.equals(Transport.DOG.getName()))
            color = Color.white;
        return color;
    }

    private void handleDoneAction(String to, String nicecity, int[] end) throws InterruptedException {
        if (frame.player.getTransport() == Transport.CARAVAN) {
            // Caravan is a one way ticket
            // these kinds of checks should really be refactored straight into the items, and not used here  
            frame.player.removeItem("10320");
            frame.player.removeItem("10325");
            frame.player.setTransport(Transport.WALK);
        }
        Thread.sleep(500);
        frame.player.toHomeCountry(end[0], end[1], to, nicecity);
        frame.setModal(false);
        frame.gotoPlace(to);
    }
}