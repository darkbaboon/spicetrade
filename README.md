# Spice Trade
Spice Trade is an artistic simulation made with a twinkle in the eye,
  with an emphasis on placing yourself in someone else’s shoes and 
  questioning European values systems as universal, rather than historic
  accuracy.

You can download the Spice Trade computer game for free from the [Spice Trade](https://gitlab.com/darkbaboon/spicetrade)


### 0. Contents

   1. General
   2. Description
   3. Contact information
   4. License information
   5. Tools and technology
   6. Documentation
   7. The end


### 1. General

  Spice Trade computer game was done by:
  Kalle Hamm, Juha Holopainen, Dzamil Kamanger, Sade Kahra, 
  Goran Mrdja and Michiko Isogai.


### 2. Description

  Colonize Europe!

  Start from nothing and dominate another continent.
  Spread the influence of your culture while creating your own empire!

  Since the 14th century, Europe has been a global super power when it 
  started colonizing other continents. But what if history had gone a 
  different way? What would Europe be like if Asia had prevailed?

  Abu Al-Qazzaz is the main character in Spice Trade. He is a poor young 
  man from Baghdad, who has inherited a house and some land, and he wants 
  to get married too.

  Abu decides to try his luck in the spice game. He has to prevent the 
  European countries from gaining a trade monopoly. Will Abu be able to 
  beat out the Europeans in the spice trade?
  Could Europe have been different?
  How? You decide!

  Specification: Java(TM)2, JRE 1.4+
  Get Java at [www.java.com](http://www.java.com)


### 3. Contact information

  Homepage:
  [www.spicetrade.org](http://www.spicetrade.org/)

  Questions and comments can be sent to abu@spicetrade.org

    * Kalle Hamm, kalle@spicetrade.org
    * Juha Holopainen, juhah@spicetrade.org
    * Sade Kahra, sade@spicetrade.org
    * Michiko Isogai, michiko@spicetrade.org
    * Goran Mrdja, goran@spicetrade.org
    * Dzamil Kamanger, dzamil@spicetrade.org


### 4. License information

    The game and source code can be downloaded at 
    http://www.spicetrade.org/

    The source code is released under the LGPL license and can be 
    distributed under the terms of that license. Please see the license 
    for more details, you can find it at
    http://www.gnu.org/copyleft/lesser.html.

    The third-party libraries used in the game:
    - XStream
    - MXParser
    - BeanShell
    - Java-Vorbis-Support
    Please see the license/ directory for the licenses of these libraries.

    Java and all Java-based trademarks are trademarks of Sun Microsystems, Inc.
    Lotus and Notes are trademarks of IBM Corporation


### 5. Tools and technology

  The following third party technologies (libraries) were used in building 
  the game engine (many thanks to all parties):

  [Java and AWT](http://java.sun.com)
    - The game is done as a Java application, using the AWT drawing routines.

  [XStream](http://x-stream.github.io/)
    - The game data is stored as XML, which is loaded by using XStream, done 
      by Joe Walnes.

  [BeanShell](http://www.beanshell.org/)
    - Another technology used in the game is BeanShell, done by Pat Niemeyer, 
      that allows for arbitrary Java code to be executed (eval) within context.

  [java-vorbis-support](https://github.com/Trilarion/java-vorbis-support)
    - Combination of JOrbis, JavaSPI and Tritonus-Share.

  [Tritonus](http://www.tritonus.org/)
    - Tritonus is an implementation of the Java Sound API and is used in the 
      game together with JOrbis to provide the Ogg Vorbis support.

  [JOrbis](http://www.jcraft.com/jorbis/)
    - JOrbis is a pure Java Ogg Vorbis decoder from JCraft and together with 
  Tritonus, enables the game to use Ogg Vorbis format sound, which is good 
  because the origianl WAV format files took 500Mb.

    And these are the tools:

  [Eclipse](http://www.eclipse.org/)
    - The game has been developed using the Eclipse IDE.

  [Visual Studio Code](https://code.visualstudio.com/)
    - The game is now developped using the Visual Studio Code IDE.
  
  [Ant](http://ant.apache.org/)
    - Packaging the game into a JAR file has been done using an Ant build script.

  [Lotus Notes](http://www.ibm.com/)
    - All the XML in the game has been edited with a Lotus Notes application 
      created specifically for the task.


### 6. Documentation

  Please see the tutorial for the game, it can be found on the Spice Trade CD,
  in the /tutorial folder. You can also find [the tutorial in the Spice Trade website](https://web.archive.org/web/20200128111237/http://www.spicetrade.org/tutorial.html).

  For more information about the game, please see the following web pages: https://web.archive.org/web/20200128111233/http://www.spicetrade.org/index.html


### 7. The end

  Thanks to everyone and hope you enjoy the game :-)

Originally developed at ![[SourceForge](https://sourceforge.net/projects/spicetrade/)](https://sourceforge.net/sflogo.php?group_id=138597&type=5) by [juhah101](https://sourceforge.net/u/juhah101/) and published under [LGPL 2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html).

Copyright (C) 2023 spicetrade.org
Originally Made in Finland and continued in France